<?php 
use Cake\Routing\Router;
?>

<section class="inner_menu">
  <div class="container">
    <div class="center_menu">
        <ul>
          <li class="active"><a href="http://webinar.aistechnolabs.in/wireframe/meeting-room-data.html" class="btn btn-primary">MEETINGS ROOMS</a></li>
          <li><a href="http://webinar.aistechnolabs.in/wireframe/new-meeting-room.html" class="btn btn-primary">New Room</a></li>
          <li><a href="http://webinar.aistechnolabs.in/wireframe/content-library.html" class="btn btn-primary">content library</a></li>
          <li><a href="http://webinar.aistechnolabs.in/wireframe/recording.html" class="btn btn-primary">Recordings</a></li>
          <li><a href="http://webinar.aistechnolabs.in/wireframe/default-settings-invitations.html" class="btn btn-primary">Default settings</a></li>
        </ul>
    </div>
  </div>
</section>
<section>
<div class="container">
    <div class="page_title">
      <div class="text-center">
        <h1><span>Meeting Rooms</span></h1>
      </div>
    </div>
  </div>
</section>
<section class="main_content">
  <div class="container">
    <div class="recording_box">
      <h3 class="text-center">An Olampic Idea</h3>
      <div class="link_title">
        <h4>Meeting URL: <a href="#">http://myWebinar.now/mondaywebinar</a></h4>
      </div>
      <div class="box">
        <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
          <div class="btn btn-primary btn-block start_meeting meeting">
            <a href="http://webinar.aistechnolabs.in/wireframe/create-registration-form.html"><h1><i class="fa fa-play-circle-o"></i> Enter Meeting</h1>
            <h4>Status: <span>Active</span></h4></a>
          </div>
        </div>
        <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
          <div class="list_btn">
            <div class="delete_btn disabled"> <a href="#" class="disabled"><i class="fa fa-pencil-square-o"></i> <span>Registration</span></a> </div>
            <div class="viewers_btn disabled"> <a href="#" class="disabled"><i class="fa fa-wrench"></i> <span>Edit Meeting Room</span></a> </div>
            <div class="delete_btn disabled"> <a href="#" class="disabled"><i class="fa fa-calendar"></i> <span>Scheduling</span></a> </div>
          </div>
        </div>
        <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
          <div class="list_btn">
            <div class="delete_btn disabled"> <a href="#" class="disabled"><i class="fa fa-upload"></i> <span>Notes Settings</span></a> </div>
            <div class="viewers_btn disabled"> <a href="#" class="disabled"><i class="fa fa-bar-chart"></i> <span>Edit Polls</span></a> </div>
            <div class="delete_btn disabled"> <a href="#" class="disabled"><i class="fa fa-search"></i> <span>Reports</span></a> </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
   
    <div class="pagination_box">
      <div class="row">
        <div class="col-sm-6">
          <div class="numeric_paginaion">
            <ul>
               <?PHP echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
            </ul>
          </div>
        </div>
        <div class="col-sm-6 text-right">
          <div class="next_previous">
            <ul>
           <?php   echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
    ['escape' => false]); ?>
    
     <?php   echo $this->Paginator->next('<i class="fa fa-fast-backward"></i>',
    ['escape' => false]); ?>
             
             
             
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  </section>

        





<!--

<section class="main_content">
    <div class="container">
        <?php foreach ($meetings as $row): ?>
        <div class="recording_box">
            <h1 class="text-center"><?= $row->name ?></h1>
            <div class="link_title">
                <h4>Public Reply: <a target="_blank" href="<?php echo $this->Url->build('/meetings/joinMeeting/'.$row->meeting_id, true); ?>">
                    <?php echo $this->Url->build('/meetings/joinMeeting/'.$row->meeting_id, true); ?>
                </a></h4>
            </div>
            <div class="box">
                <div class="col-sm-5">
                    <div class="btn btn-primary btn-block">
                        <h1><i class="fa fa-play-circle-o"></i> Play &amp; Edit</h1>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="list_btn">
                        <div class="delete_btn">
                          <?php actionDelete(['url'=>"meetings/delete",'id'=>$row->id,'class'=>'delete-btn','icon'=>'fa fa-remove']); ?>
                        </div>
                        <div class="viewers_btn">
                            <a href="#"><i class="fa fa-eye"></i> <span>Viewers</span></a>
                        </div>
                        <div class="delete_btn">
                            <a href="#"><i class="fa fa-code"></i> <span>Embeded</span></a>
                        </div>
                        <div class="delete_btn">
                            <a href="#"><i class="fa fa-ellipsis-h"></i> <span>Details</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="detail_center">
                        <label>Date</label><br>
                        <label><?= date('F d Y , h:i A',strtotime($row->meeting_time)) ?></label><br>
                        <label>Unique Viewers:</label>4<br>
                        <label>Viewing Duration:</label><?= $row->meeting_duration ?> minutes
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php endforeach; ?>

-->
