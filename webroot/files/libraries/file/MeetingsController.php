<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use BigBlueButton\BigBlueButton as BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters as CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters as JoinMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters as IsMeetingRunningParameters;
use DateTimeZone;
use DateTime;
use GuzzleHttp\Client as Client;
use Cake\Routing\Router;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */
class MeetingsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $meetings = $this->paginate($this->Meetings);

        $this->set(compact('meetings'));
        $this->set('_serialize', ['meetings']);
    }

    /**
     * View method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);

        $this->set('meeting', $meeting);
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $meeting = $this->Meetings->newEntity();
        
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $data['created_at'] = new Time(date('Y-m-d H:i:s'));
            $data['meeting_id'] = md5(uniqid(rand(), true));
            $meeting = $this->Meetings->patchEntity($meeting, $data);
            if ($this->Meetings->save($meeting)) {
                $id = $meeting->id;
                $this->Flash->success(__('The meeting has been saved.'));
                return $this->redirect(['action' => 'addImageUpload',$id]);
            } else {
                $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meeting'));
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Add Meeting Image Upload
     */

    public function addImageUpload($id=null){
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $data['updated_at'] = new Time(date('Y-m-d H:i:s'));

            $meeting = $this->Meetings->patchEntity($meeting, $data);
            if ($this->Meetings->save($meeting)) {
                $id = $meeting->id;
                $this->Flash->success(__('The meeting has been saved.'));
                return $this->redirect(['action' => 'addSuccess',$id]);
            } else {
                $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meeting'));
        $this->set('_serialize', ['meeting']);
    }

    /*
     * Add Success
     */

    public function addSuccess($id=null){

        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);

        $this->set(compact('meeting'));
        $this->set('_serialize', ['meeting']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
            
            $meeting = $this->Meetings->patchEntity($meeting, $data);
            if ($this->Meetings->save($meeting)) {
                $this->Flash->success(__('The meeting has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('meeting'));
        $this->set('_serialize', ['meeting']);
    }

    /*
     * Email Invitaion
     */

    public function emailInvitation($id = null){
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);

        $timezoneList = $this->timezoneList();
        $this->set(compact('meeting','timezoneList'));
        $this->set('_serialize', ['email_invitation']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Meeting id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $meeting = $this->Meetings->get($id);
        if ($this->Meetings->delete($meeting)) {
            $this->Flash->success(__('The meeting has been deleted.'));
        } else {
            $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller'=>'Dashboard','action' => 'index']);
    }
    // ============================================= 
    /**
    * class   : 
    * menthod : joinMeeting
    * @param  : 
    * @output : 
    * @Description : 
    **/
    // ==============================================    
    public function joinMeeting($meeting_id='')
    {
        $meeting = $this->Meetings->find()
            ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
            ->first();
        
        if(!empty($meeting)){
            $meeting = json_decode(json_encode($meeting));

            $bbb = new BigBlueButton;
            
            //CREATE MEETING
            $create_meeting = new CreateMeetingParameters($meeting_id,$meeting->name);
            $create_meeting->setAttendeePassword('123456')->setRecord(true)->setModeratorPassword('123456');
        
            //$is_meeting_running = $bbb->isMeetingRunning(new IsMeetingRunningParameters($meeting_id));
            $xml = '';
            if(!empty($meeting->presentation_file)){
                $file_url = Router::url('/', true).'webroot/files/Meetings/presentation_file/'.$meeting->presentation_file; 
                
            }
            
            $result = $bbb->createMeeting($create_meeting,$xml);
            
            //JOIN MEETING
            if(!empty($result)){
                $meeting_url = $bbb->getJoinMeetingURL($join_meeting);
                $this->redirect($meeting_url);
            }

        }
        $this->Flash->error(__('The meeting could not in our record.'));
        return $this->redirect(['controller'=>'Dashboard','action' => 'index']);

        //CREATE MEETING
        /*$bbb = new BigBlueButton;
        echo "<pre>";print_r($bbb->getApiVersion()); exit;
        $url = "http://192.168.1.97/bigbluebutton/api/create?name=Test+Meeting&meetingID=abc23&attendeePW=111222&moderatorPW=333444&checksum=8c54c0fc46c52a7d8f7141f6694e56ca708ca67a";
        $join ="http://192.168.1.97/bigbluebutton/api/join?fullName=manoj&meetingID=abc23&password=111222&checksum=9ff4b7e705933002b73dfe4f72e0e54d69198b61";*/
        //$query_string = "createname=ancb&meetingID=ad1234567&attendeePW=123456&moderatorPW=123456f62fc859ba6205651aed7e34b58e0405";
        //echo "<pre>";print_r(sha1('createname=Test+Meeting&meetingID=abc23&attendeePW=111222&moderatorPW=333444f62fc859ba6205651aed7e34b58e0405')); exit;
        
        /*$join_query_string = "joinfullName=manoj&meetingID=abc23&password=111222f62fc859ba6205651aed7e34b58e0405";
        echo "<pre>";print_r(sha1($join_query_string)); exit;*/
    }


    /*
     * For Used Timezone List
     */

    public function timezoneList()
    {
        $timezoneIdentifiers = DateTimeZone::listIdentifiers();
        $utcTime = new DateTime('now', new DateTimeZone('UTC'));

        $tempTimezones = array();
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = array(
                'offset' => (int)$currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            );
        }

        // Sort the array by offset,identifier ascending
        usort($tempTimezones, function($a, $b) {
            return ($a['offset'] == $b['offset'])
                ? strcmp($a['identifier'], $b['identifier'])
                : $a['offset'] - $b['offset'];
        });

        $timezoneList = array();
        foreach ($tempTimezones as $tz) {
            $sign = ($tz['offset'] > 0) ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
                $tz['identifier'];
        }

        return $timezoneList;
    }
}
