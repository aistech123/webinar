<?php
use Migrations\AbstractMigration;

class AddUserIdInMeeting extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('meetings');
        $table->addColumn('created_by', 'integer', [
            'default' => 0,
            'limit' => 11
        ]);
        $table->update();
    }
}
