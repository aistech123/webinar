<?php
use Migrations\AbstractMigration;

class AddMeetingTimeToMeetings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('meetings');
        $table->removeColumn('meeting_time');
        $table->addColumn('meeting_date', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('meeting_time', 'time', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('is_recurring', 'integer', [
            'default' => null,
            'limit' => 1,
            'null' => false,
        ]);
        $table->addColumn('attendees', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('presenters', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('email_subject', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('email_message', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('conference_information', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->update();
    }
}
