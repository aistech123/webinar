<?php
use Migrations\AbstractMigration;

class AddMeetingPasswordToMeetings extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('meetings');
        $table->addColumn('is_meeting_password_protected', 'integer', [
            'default' => null,
            'limit' => 1,
            'null' => false,
        ]);
        $table->addColumn('is_meeting_room_active', 'integer', [
            'default' => null,
            'limit' => 1,
            'null' => false,
        ]);
        $table->update();
    }
}
