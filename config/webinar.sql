-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 21, 2016 at 11:30 AM
-- Server version: 5.5.53-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webinar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(6) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`) VALUES
(1111, 'admin', 'admin@1234');

-- --------------------------------------------------------

--
-- Table structure for table `attendees`
--

CREATE TABLE `attendees` (
  `id` int(11) NOT NULL,
  `meeting_id` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `username` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `exit_time` datetime NOT NULL,
  `role` varchar(5) NOT NULL,
  `meeting_date` date NOT NULL,
  `durationOfMeeting` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendees`
--

INSERT INTO `attendees` (`id`, `meeting_id`, `email`, `password`, `username`, `created_at`, `updated_at`, `exit_time`, `role`, `meeting_date`, `durationOfMeeting`) VALUES
(128, 'newmeeting', '', '', '', '2016-09-21 23:29:38', '2016-10-18 21:22:53', '2016-09-21 23:59:05', 'admin', '2016-09-21', 30),
(129, 'MonthlyMeeting', '', '', '', '2016-09-22 17:04:02', '2016-10-25 11:47:06', '2016-09-23 14:12:46', 'admin', '2016-09-22', 8),
(130, 'MonthlyMeeting', '', '', '', '2016-09-23 14:13:46', '2016-10-25 11:47:06', '2016-10-17 14:44:38', 'admin', '2016-09-23', 31),
(131, 'newroom', '', '', '', '2016-10-17 05:48:34', '2016-10-24 07:28:10', '2016-10-17 06:26:35', 'admin', '2016-10-17', 38),
(132, 'newroom', '', '', '', '2016-10-18 03:35:13', '2016-10-24 07:28:10', '2016-10-18 09:05:36', 'admin', '2016-10-18', 330),
(133, '???', '???', '', '', '2016-10-19 01:52:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'admin', '2016-10-19', 0),
(134, 'newroom', 'xzczxczxc@gmail.com', '', 'નામ', '2016-10-19 01:54:38', '2016-10-24 07:28:10', '2016-10-19 02:00:47', '', '2016-10-19', 0),
(135, 'newmeeting', '', '', '', '2016-10-18 19:36:07', '2016-10-18 21:22:53', '2016-10-18 21:44:06', 'admin', '2016-10-18', 128),
(136, 'newmeeting', 'admin1111@gmail.com', '', 'xcxcx???111222', '2016-10-18 21:28:15', '0000-00-00 00:00:00', '2016-10-18 21:44:06', '', '2016-10-18', 0),
(137, 'newmeeting', 'Nome@utente.com', '', 'Nome utente', '2016-10-18 21:30:10', '0000-00-00 00:00:00', '2016-10-18 21:44:06', '', '2016-10-18', 0),
(138, 'newroom', '', '', '', '2016-10-20 07:20:57', '2016-10-24 07:28:10', '0000-00-00 00:00:00', 'admin', '2016-10-20', 0),
(139, 'czxvvcx', '', '', '', '2016-10-21 05:50:53', '0000-00-00 00:00:00', '2016-10-21 05:54:58', 'admin', '2016-10-21', 4),
(140, 'newroom', '', '', '', '2016-10-21 03:44:41', '2016-10-24 07:28:10', '0000-00-00 00:00:00', 'admin', '2016-10-21', 0),
(141, 'fdgfdgg', '', '', '', '2016-10-21 18:29:33', '0000-00-00 00:00:00', '2016-10-24 16:32:34', 'admin', '2016-10-21', 1323),
(142, 'fdgfdgg', '', '', '', '2016-10-24 16:20:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'admin', '2016-10-24', 0),
(143, 'fdgfdgg', 'fdgfd@dsfdsf.com', '', 'ffffffffffffff', '2016-10-24 16:21:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2016-10-24', 0),
(144, 'fdgfdgg', 'hina@gmail.com', '', 'hina', '2016-10-24 16:24:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2016-10-24', 0),
(145, 'fdgfdgg', 'ritika@gmail.com', '', 'ritika', '2016-10-24 16:25:17', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2016-10-24', 0),
(146, 'newroom', '', '', '', '2016-10-24 07:03:13', '2016-10-24 07:28:10', '2016-10-24 07:12:39', 'admin', '2016-10-24', 0),
(147, 'newroom', 'nikki@gmail.com', '', 'nikita', '2016-10-24 07:05:42', '2016-10-24 07:28:10', '2016-10-24 07:10:00', '', '2016-10-24', 0),
(148, 'newroom', 'roshani@gmail.com', '', 'roshani', '2016-10-24 07:06:49', '2016-10-24 07:28:10', '2016-10-24 07:12:39', '', '2016-10-24', 0),
(149, 'newroom', 'riddhi@gmail.com', '', 'riddhi', '2016-10-24 07:12:39', '2016-10-24 07:28:10', '2016-10-24 07:12:39', '', '2016-10-24', 0),
(150, 'newroom', 'xcvcx@cxvxc.com', '', 'cvxcv', '2016-10-24 07:14:09', '2016-10-24 07:28:10', '2016-10-24 07:12:39', '', '2016-10-24', 0),
(151, 'newmeeting', '', '', '', '2016-10-24 00:15:08', '0000-00-00 00:00:00', '2016-10-24 00:22:33', 'admin', '2016-10-24', 7),
(152, 'newmeeting', 'sdsdsd@gmail.com', '', 'sadsa', '2016-10-24 00:20:17', '0000-00-00 00:00:00', '2016-10-24 00:22:33', '', '2016-10-24', 0),
(153, 'newmeeting', 'hina@gmail.com', '', 'hinal', '2016-10-24 00:20:42', '0000-00-00 00:00:00', '2016-10-24 00:22:33', '', '2016-10-24', 0),
(154, 'MonthlyMeeting', '', '', '', '2016-10-24 16:53:05', '2016-10-25 11:47:06', '2016-10-25 11:50:09', 'admin', '2016-10-24', 1137),
(155, 'MonthlyMeeting', 'ogo@gmail.com', '', 'ogisdfds', '2016-10-24 16:54:45', '2016-10-25 11:47:06', '2016-10-24 16:57:53', '', '2016-10-24', 0),
(156, 'MonthlyMeeting', 'czxzx@sddfsdf.esresr', '', 'xcxcxcxc', '2016-10-24 16:55:20', '2016-10-25 11:47:06', '2016-10-24 16:57:53', '', '2016-10-24', 0),
(157, 'newroom', 'fff@gfs.retrete', '', 'fffff', '2016-10-24 07:28:55', '0000-00-00 00:00:00', '2016-10-24 07:12:39', '', '2016-10-24', 0),
(158, 'newroom', 'devid@gmail.com', '', 'dreamcrafter', '2016-10-24 07:30:00', '0000-00-00 00:00:00', '2016-10-24 07:12:39', '', '2016-10-24', 0),
(159, 'MonthlyMeeting', 'sdsdsd@gmail.com', '', 'sdsdsds', '2016-10-24 17:11:12', '2016-10-25 11:47:06', '2016-10-25 11:50:09', '', '2016-10-24', 0),
(160, 'MonthlyMeeting', '', '', '', '2016-10-25 11:45:09', '2016-10-25 11:47:06', '0000-00-00 00:00:00', 'admin', '2016-10-25', 0),
(161, 'MonthlyMeeting', 'ashol@gmail.com', '', 'sdsddsad', '2016-10-25 11:48:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2016-10-25', 0),
(162, 'MonthlyMeeting', 'asfasfasfa@asfsf.fhgdhyd', '', 'assafasfasfasfd', '2016-10-25 11:49:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '2016-10-25', 0),
(163, 'adsfsafasda', '', '', '', '2016-11-04 06:01:50', '2016-11-16 05:08:18', '2016-11-11 06:26:27', 'admin', '2016-11-04', 85),
(164, 'adsfsafasda', '', '', '', '2016-11-14 00:28:34', '2016-11-16 05:08:18', '2016-11-16 01:04:42', 'admin', '2016-11-14', 36),
(165, 'adsfsafasda', '', '', '', '2016-11-15 07:39:01', '2016-11-16 05:08:18', '0000-00-00 00:00:00', 'admin', '2016-11-15', 0),
(166, 'adsfsafasda', '', '', '', '2016-11-16 02:36:33', '2016-11-16 05:08:18', '2016-11-16 05:12:19', 'admin', '2016-11-16', 156),
(167, '1161119095214', '', '', '', '2016-11-19 04:52:22', '0000-00-00 00:00:00', '2016-11-19 04:57:06', 'admin', '2016-11-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `copy_attendees`
--

CREATE TABLE `copy_attendees` (
  `id` int(11) NOT NULL,
  `meeting_id` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `role` varchar(5) NOT NULL,
  `timezone` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `copy_attendees`
--

INSERT INTO `copy_attendees` (`id`, `meeting_id`, `created_at`, `role`, `timezone`, `created_by`) VALUES
(1, 'newroom', '2016-10-19 01:52:54', 'admin', 'America/New_York', 1),
(2, 'adsfsafasda', '2016-11-19 04:52:22', 'admin', 'America/New_York', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'india', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'US', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Pak', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Ban', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Sri', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Aus', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Auf', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Eng', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'SAF', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `libraries`
--

CREATE TABLE `libraries` (
  `id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` int(25) NOT NULL,
  `created_at` datetime NOT NULL,
  `meeting_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `libraries`
--

INSERT INTO `libraries` (`id`, `file`, `size`, `created_at`, `meeting_id`) VALUES
(79, 'Webinar_ Meetings.pdf', 1114173, '2016-09-20 11:49:54', 1),
(80, '2.pdf', 5343581, '2016-09-20 11:50:22', 1),
(83, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 12:29:33', 1),
(84, 'New ERA of latest smartphones   Docoss X1  India    Thank you.png', 47547, '2016-09-20 12:30:50', 1),
(85, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:04:42', 1),
(86, 'OGI.mp4', 3091440, '2016-09-20 13:05:53', 1),
(87, 'OGI.mp4', 3091440, '2016-09-20 13:07:33', 1),
(88, 'OGI.mp4', 3091440, '2016-09-20 13:08:01', 1),
(89, 'OGI.mp4', 3091440, '2016-09-20 13:09:32', 1),
(90, 'OGI.mp4', 3091440, '2016-09-20 13:10:09', 1),
(91, 'googlelogo_color_272x92dp.png', 287810, '2016-09-20 13:13:25', 1),
(92, 'BigBlueButton   test123 meeting.png', 288567, '2016-09-20 13:14:07', 1),
(93, 'googlelogo_color_272x92dp.png', 287810, '2016-09-20 13:37:55', 1),
(94, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:38:25', 1),
(95, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:42:12', 1),
(96, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:49:50', 1),
(97, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:50:32', 1),
(98, 'CakePHPCookbook(1).pdf', 2404576, '2016-09-20 13:51:10', 1),
(99, 'OGI.mp4', 3091440, '2016-09-20 13:51:34', 1),
(100, 'OGI.mp4', 3091440, '2016-09-20 13:55:40', 1),
(101, 'OGI.mp4', 3091440, '2016-09-20 14:00:01', 1),
(102, 'OGI.mp4', 3091440, '2016-09-20 14:00:29', 1),
(103, 'OGI.mp4', 3091440, '2016-09-20 14:01:21', 1),
(104, 'OGI.mp4', 3091440, '2016-09-20 14:01:46', 1),
(105, 'OGI.mp4', 3091440, '2016-09-20 14:02:39', 1),
(106, 'editor.png', 82537, '2016-10-17 09:37:29', 1),
(107, 'bbb.png', 172312, '2016-10-17 09:38:48', 6),
(108, 'Presentation1.pptx', 20089425, '2016-10-21 06:12:04', 6),
(109, 'bbbdefault.png', 176505, '2016-11-04 09:02:25', 9),
(110, 'Exotica-Exz-80-dual-white-SDL852079537-1-b6af7.jpg', 128018, '2016-11-04 09:02:42', 9),
(111, 'Fastrack-Sports-3099SM04-Men-s-SDL088358073-1-090ad.jpg', 56129, '2016-11-04 09:03:19', 9),
(112, 'Fastrack-Sports-3099SM05-Men-s-SDL646689380-1-e9447.jpg', 55402, '2016-11-04 09:03:33', 9),
(113, 'default.ppt', 1436672, '2016-11-04 09:03:37', 9);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timezone` text NOT NULL,
  `last_login_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_id`, `timezone`, `last_login_time`) VALUES
(2, 1, 'America/New_York', '2016-10-24 00:00:00'),
(3, 1, 'America/New_York', '2016-10-24 09:26:19'),
(4, 1, 'America/New_York', '2016-10-25 02:14:16'),
(5, 1, 'America/New_York', '2016-10-25 03:42:39'),
(6, 1, 'America/New_York', '2016-10-27 01:34:30'),
(7, 1, 'America/New_York', '2016-10-27 02:28:28'),
(8, 1, 'America/New_York', '2016-10-27 03:26:54'),
(9, 1, 'America/New_York', '2016-10-27 06:25:12'),
(10, 1, 'America/New_York', '2016-10-27 07:55:49'),
(11, 1, 'America/New_York', '2016-10-27 08:38:02'),
(12, 1, 'America/New_York', '2016-10-28 01:23:37'),
(13, 1, 'America/New_York', '2016-10-28 03:26:54'),
(14, 1, 'America/New_York', '2016-10-28 03:27:14'),
(15, 1, 'America/New_York', '2016-10-28 04:14:44'),
(16, 1, 'America/New_York', '2016-10-28 05:17:08'),
(17, 1, 'America/New_York', '2016-10-28 05:19:39'),
(18, 1, 'America/New_York', '2016-10-28 05:59:49'),
(19, 1, 'America/New_York', '2016-10-28 06:00:56'),
(20, 1, 'America/New_York', '2016-10-28 06:07:54'),
(21, 1, 'America/New_York', '2016-10-28 07:21:37'),
(22, 1, 'America/New_York', '2016-10-28 07:24:57'),
(23, 1, 'America/New_York', '2016-10-28 07:29:16'),
(24, 1, 'America/New_York', '2016-10-28 07:53:21'),
(25, 1, 'America/New_York', '2016-11-02 03:56:32'),
(26, 1, 'America/New_York', '2016-11-02 04:24:08'),
(27, 1, 'America/New_York', '2016-11-02 04:37:10'),
(28, 1, 'America/New_York', '2016-11-04 01:52:18'),
(29, 1, 'America/New_York', '2016-11-04 05:00:48'),
(30, 1, 'America/New_York', '2016-11-04 05:38:48'),
(31, 1, 'America/New_York', '2016-11-04 06:19:26'),
(32, 1, 'America/New_York', '2016-11-04 06:47:37'),
(33, 1, 'America/New_York', '2016-11-05 04:46:34'),
(34, 1, 'America/New_York', '2016-11-08 03:48:26'),
(35, 1, 'America/New_York', '2016-11-09 06:03:47'),
(36, 1, 'America/New_York', '2016-11-11 06:26:10'),
(37, 1, 'America/New_York', '2016-11-14 00:27:55'),
(38, 1, 'America/New_York', '2016-11-15 07:37:46'),
(39, 1, 'America/New_York', '2016-11-16 01:03:40'),
(40, 1, 'America/New_York', '2016-11-16 02:36:09'),
(41, 1, 'America/New_York', '2016-11-16 03:01:58'),
(42, 1, 'America/New_York', '2016-11-16 04:40:22'),
(43, 1, 'America/New_York', '2016-11-19 04:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `topic` text,
  `password` varchar(255) NOT NULL,
  `presentation_file` varchar(255) NOT NULL,
  `meeting_duration` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `meeting_id` varchar(255) DEFAULT NULL,
  `is_meeting_password_protected` int(1) NOT NULL,
  `is_meeting_room_active` int(1) NOT NULL,
  `meeting_exit_url` varchar(255) NOT NULL,
  `meeting_date` date NOT NULL,
  `meeting_time` text NOT NULL,
  `is_recurring` int(1) NOT NULL,
  `attendees` varchar(255) NOT NULL,
  `presenters` varchar(255) NOT NULL,
  `email_subject` varchar(255) NOT NULL,
  `email_message` text NOT NULL,
  `conference_information` varchar(255) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `notes_subject` varchar(200) NOT NULL,
  `notes_message` text NOT NULL,
  `repeatDay` varchar(255) NOT NULL,
  `monthrepeat` varchar(255) NOT NULL,
  `repeatwed` varchar(255) NOT NULL,
  `repeatTime` varchar(255) NOT NULL,
  `sections` varchar(255) NOT NULL,
  `lockfeature` varchar(255) NOT NULL,
  `background` varchar(255) NOT NULL,
  `color` varchar(20) NOT NULL,
  `skin` text NOT NULL,
  `poll1` text NOT NULL,
  `attendeeName` text NOT NULL,
  `attendeeEmail` text NOT NULL,
  `attendeePassword` text NOT NULL,
  `headerText` varchar(200) NOT NULL,
  `joinText` varchar(200) NOT NULL,
  `joinButtonText` varchar(200) NOT NULL,
  `max_attendees` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `name`, `topic`, `password`, `presentation_file`, `meeting_duration`, `deleted`, `created_at`, `updated_at`, `created_by`, `meeting_id`, `is_meeting_password_protected`, `is_meeting_room_active`, `meeting_exit_url`, `meeting_date`, `meeting_time`, `is_recurring`, `attendees`, `presenters`, `email_subject`, `email_message`, `conference_information`, `timezone`, `notes_subject`, `notes_message`, `repeatDay`, `monthrepeat`, `repeatwed`, `repeatTime`, `sections`, `lockfeature`, `background`, `color`, `skin`, `poll1`, `attendeeName`, `attendeeEmail`, `attendeePassword`, `headerText`, `joinText`, `joinButtonText`, `max_attendees`) VALUES
(1, 'MonthlyMeeting', NULL, '', '', 10, 0, '2016-08-03 09:17:57', '2016-09-14 09:44:57', 1, 'MonthlyMeeting', 0, 0, 'http://192.168.1.153/bigbluebutton/api/join?meetingID=MonthlyMeeting&fullName=assafasfasfasfd&password=1234&userID=162&checksum=6a5804f0fb72cadf929a30d640e0be1d60803e7b', '2017-03-31', '09:17', 0, '', '', '', '', '', 'Asia/Kolkata', '', '', '', 'fifth fri', '', 'monthly', '', '', '', '', '', '', '', '', '', 'monthly meeting', '', '', 0),
(2, 'fdgfdgg', NULL, '', '', 10, 0, '2016-08-03 09:21:16', '2016-08-05 12:15:56', 1, 'fdgfdgg', 0, 0, 'http://192.168.1.153/bigbluebutton/api/join?meetingID=fdgfdgg&fullName=ritika&password=1234&userID=145&checksum=000c81021beb9fbfcefc6c98a86514dc70539b34', '2016-08-03', '09:21', 0, '', '', '', '', '', 'Asia/Colombo', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(4, 'czxvvcx', NULL, '', 'rsz_bbbgrey.png', 0, 0, '2016-08-17 08:06:23', '2016-09-05 12:33:02', 1, 'czxvvcx', 0, 0, 'http://192.168.1.153/bigbluebutton/api/join?meetingID=czxvvcx&fullName=admin&password=swM6dfAKVF%40%24&checksum=5b7a75a1578d3269258c2af72499ea92095dfc10', '2016-08-17', '08:06', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Name', 'Email', '', 'csadsad', 'Login by entering your information below', 'Join Meeting', 0);

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

CREATE TABLE `phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `start_time`, `end_time`) VALUES
(20160406123349, '2016-04-06 07:09:48', '2016-04-06 07:09:48'),
(20160407085104, '2016-04-07 06:28:10', '2016-04-07 06:28:10'),
(20160407093725, '2016-04-07 06:28:10', '2016-04-07 06:28:12'),
(20160408062808, '2016-04-08 03:23:35', '2016-04-08 03:23:37'),
(20160408093546, '2016-04-08 04:06:31', '2016-04-08 04:06:31'),
(20160409105458, '2016-04-09 05:40:42', '2016-04-09 05:40:42'),
(20160409111926, '2016-04-09 05:54:19', '2016-04-09 05:54:20'),
(20160413070016, '2016-04-13 01:31:19', '2016-04-13 01:31:20'),
(20160413092815, '2016-04-14 05:22:55', '2016-04-14 05:22:56'),
(20160413100101, '2016-04-14 05:22:56', '2016-04-14 05:22:56'),
(20160415062353, '2016-04-24 23:24:36', '2016-04-24 23:24:39'),
(20160415071036, '2016-04-24 23:24:39', '2016-04-24 23:24:39');

-- --------------------------------------------------------

--
-- Table structure for table `polls`
--

CREATE TABLE `polls` (
  `id` int(11) NOT NULL,
  `poll_id` varchar(255) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `description` text,
  `answer` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `polls`
--

INSERT INTO `polls` (`id`, `poll_id`, `meeting_id`, `description`, `answer`) VALUES
(1, '0', 39, 'poll1', 'sdasdasd'),
(7, '6', 39, 'zxczxczc', 'zxczcxxz'),
(8, '6', 39, NULL, 'zxczxczxc'),
(9, '8', 39, 'sdsdsd', 'sdsdsd'),
(11, '8', 39, NULL, 'sdsdsds'),
(12, '0', 37, 'xzxzx', 'zxzxzx');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `notes_subject` varchar(255) DEFAULT NULL,
  `notes_message` text,
  `email_subject` varchar(255) NOT NULL,
  `attendees` text NOT NULL,
  `email_message` text NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(25) NOT NULL,
  `sections` varchar(255) NOT NULL,
  `lockfeature` varchar(255) NOT NULL,
  `background` text NOT NULL,
  `color` text NOT NULL,
  `skin` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `notes_subject`, `notes_message`, `email_subject`, `attendees`, `email_message`, `name`, `image`, `password`, `sections`, `lockfeature`, `background`, `color`, `skin`) VALUES
(1, 'zXzXzX', '<p>ZXZXzXZXz</p>\r\n', 'asdsadas', 'priyal@aistechnolabs.us,priyal.limbani@gmail.com', '<p>sadasdasdas</p>\r\n', '<p>xzczxc sdfsdfsdfsdfsdfasdsadas</p>\r\n', 'feature.png', '', 'bbb.layout.name.presentfocus', '0,1,2,3,', 'cake111.icon.png', '', 'BBBBlue.css.swf');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` tinytext NOT NULL,
  `max_attendees` int(10) NOT NULL,
  `email` tinytext NOT NULL,
  `role` tinytext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `country` int(11) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `zip` int(11) NOT NULL,
  `fax` varchar(60) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `address` tinytext NOT NULL,
  `reset_password_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `max_attendees`, `email`, `role`, `created_at`, `updated_at`, `firstname`, `lastname`, `country`, `phone`, `state`, `city`, `zip`, `fax`, `timezone`, `address`, `reset_password_token`) VALUES
(1, 'admin', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 13, 'admin@gmail.com', 'admin', '2016-04-07 00:00:00', '0000-00-00 00:00:00', '', '', 1, '', '', '', 0, '', 'America/New_York', '', ''),
(2, 'manoj', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 0, 'manoj30808@gmail.com', 'admin', '2016-04-07 10:02:00', '2016-04-07 10:02:00', '', '', 1, '', '', '', 0, '', '', '', 'f8d7d77bc460ffdf2b3388e148b7a48c3ee2065fc5b3ec53e96685bfa8e6a28e'),
(3, 'manoj30808', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 0, 'ait26manojsonagra@yahoo.com', 'member', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'manoj', 'sonagra', 1, '9998366087', '', '', 0, '', '', '', ''),
(4, 'qqq@gmail.com', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 0, 'qqq@gmail.com', 'asad', '2016-04-28 05:17:00', '2016-04-28 05:17:00', '', '', 0, '', '', '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users1`
--

CREATE TABLE `users1` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `role` tinytext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `firstname` varchar(60) NOT NULL,
  `lastname` varchar(60) NOT NULL,
  `country` int(11) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `state` varchar(60) NOT NULL,
  `city` varchar(60) NOT NULL,
  `zip` int(11) NOT NULL,
  `fax` varchar(60) NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `address` tinytext NOT NULL,
  `reset_password_token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users1`
--

INSERT INTO `users1` (`id`, `username`, `password`, `email`, `role`, `created_at`, `updated_at`, `firstname`, `lastname`, `country`, `phone`, `state`, `city`, `zip`, `fax`, `timezone`, `address`, `reset_password_token`) VALUES
(1, 'admin', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 'admin@gmail.com', 'admin', '2016-04-07 00:00:00', '0000-00-00 00:00:00', '', '', 1, '', '', '', 0, '', '', '', ''),
(2, 'manoj', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 'manoj30808@gmail.com', 'admin', '2016-04-07 10:02:00', '2016-04-07 10:02:00', '', '', 1, '', '', '', 0, '', '', '', 'f8d7d77bc460ffdf2b3388e148b7a48c3ee2065fc5b3ec53e96685bfa8e6a28e'),
(3, 'manoj30808', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 'ait26manojsonagra@yahoo.com', 'member', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'manoj', 'sonagra', 1, '9998366087', '', '', 0, '', '', '', ''),
(4, 'qqq@gmail.com', '$2y$10$uOSqcxDIySjmv4yaPkWIiOSFmvXeBIGGFvFhkjWeBJEksD.tzOimO', 'qqq@gmail.com', 'asad', '2016-04-28 05:17:00', '2016-04-28 05:17:00', '', '', 0, '', '', '', 0, '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendees`
--
ALTER TABLE `attendees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `copy_attendees`
--
ALTER TABLE `copy_attendees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `libraries`
--
ALTER TABLE `libraries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `polls`
--
ALTER TABLE `polls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1112;
--
-- AUTO_INCREMENT for table `attendees`
--
ALTER TABLE `attendees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `copy_attendees`
--
ALTER TABLE `copy_attendees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `libraries`
--
ALTER TABLE `libraries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `polls`
--
ALTER TABLE `polls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
