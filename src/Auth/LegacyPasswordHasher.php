<?php 
namespace App\Auth;

use Cake\Auth\AbstractPasswordHasher;

class LegacyPasswordHasher extends AbstractPasswordHasher
{

    public function hash($password)
    {
    	return sha1($password);
    }

    public function check($password, $hashedPassword)
    {
    	return sha1('DYhG93b1qyJfIxfs2guVoUubWwvniR2G0FgaC9mi'.$password) === $hashedPassword;
    	

    	//CLIENT SALT
    	//return sha1('DYhG93b0qyJfIxffs2guVoUubWwvniR2G0FgaC9mi'.$password) === $hashedPassword;
    }
}