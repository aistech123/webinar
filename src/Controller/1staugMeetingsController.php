<?php
namespace App\Controller;


use App\Controller\AppController;
use Cake\I18n\Time;
use BigBlueButton\BigBlueButton as BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters as CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters as JoinMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters as IsMeetingRunningParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters as GetMeetingInfoParameters;
use BigBlueButton\Core\ApiMethod as ApiMethod;
use BigBlueButton\Responses\GetDefaultConfigXMLResponse;
use BigBlueButton\Parameters\RecordingsParameters as RecordingsParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Responses\EndMeetingResponse;
use BigBlueButton\Responses\GetMeetingsResponse;
use Cake\ORM\TableRegistry;
use DateTimeZone;
use DateTime;
use GuzzleHttp\Client as Client;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Mailer\Email;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */
  error_reporting(0);
  class MeetingsController extends AppController
  {

       var $paginate = array(
          'limit' => '5',
          
      );
      /**
       * Index method
       *
       * @return \Cake\Network\Response|null
       */
      public function index()
      {
          $meetings = $this->paginate($this->Meetings);

          $this->set(compact('meetings'));
          $this->set('_serialize', ['meetings']);
      }

      /**
       * View method
       *
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|null
       * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
       */
      public function view($id = null)
      {
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          $this->set('meeting', $meeting);
          $this->set('_serialize', ['meeting']);
      }

      /**
       * Add method
       *
       * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
       */
      public function add()
      {
          $meeting = $this->Meetings->newEntity();
         
          if ($this->request->is('post')) {
              
              $data = $this->request->data;
              $data['created_at'] = new Time(date('Y-m-d H:i:s'));
              //$data['meeting_id'] = md5(uniqid(rand(), true));
              $data['meeting_id'] = $data['name'];
              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));
                 // return $this->redirect(['action' => 'newmeetingroomschedule',$id]);
                   if (isset($this->request->data['start_meeting'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting', $data['meeting_id']]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

     public function editBasic($id = null)
       {
         $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
            
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
                $data['meeting_id'] = $data['name'];
               $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $this->Flash->success(__('The meeting has been saved.'));
                 if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }

            }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['edit_basic']);
       }  
 

  //check unique Meeting Name

       public function checkMeetingName($meetingName)
       {
         $meetingName=$_POST['meetingName'];
          $meeting = $this->Meetings->find()
                ->where(['name =' => $meetingName])
                ->count();

                if($meeting > 0)
                {
                  echo $res="not OK";
                }
                else
                {
                  echo $res="ok";
                }

                exit;
       }

  //Edit mode check unique Meeting Name

       public function EditcheckMeetingName($meetingName,$id)
       {
         $meetingName=$_POST['meetingName'];
         $id=$_POST['id'];
         $meeting = $this->Meetings->find()
                ->where(['name =' => $meetingName,'id !=' => $id])
                ->count();

                if($meeting > 0)
                {
                  echo $res="not OK";
                }
                else
                {
                  echo $res="ok";
                }

                exit;
       }
  public function newmeetingroomschedule($id=null)
  {
         
       $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
               if($meeting->meeting_date != '')
               {
                  $meetingDate = date("m/d/Y", strtotime($meeting->meeting_date)); 
                }
                else
                {
                    $meetingDate = date('m/d/Y');
                }
          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
            

                $time=$data['meeting_time'] ;
                 $data['meeting_time']= str_replace(' ', '', $time);
   
                 if($data['repeat']=='on')
                 {
                      $monthrepeat=implode(',',$data['monthrepeat']);
                      $data['monthrepeat']=$monthrepeat;

                      $repeateDay=implode(',',$data['repeatDay']);
                      $data['repeatDay']=$repeateDay;

                }
                else
                {
                  $data['repeatTime']='';
                }

              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;

                  $this->Flash->success(__('The meeting has been saved.'));
                  //return $this->redirect(['action' => 'addSuccess',$id]);
                  if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
                 // return $this->redirect(['action' => 'emailInvitation',$id]);
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }


          $timezoneList = $this->timezoneList(); 

          $query = TableRegistry::get('Countries');
          $countries = $query->find('list');
          $this->set(compact('user','countries','timezoneList','meeting','meetingDate'));
          $this->set('_serialize', ['newmeetingroomschedule','meeting']);



  }

   /**
       * Add Meeting Image Upload
       */

      public function addImageUpload($id=null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));

              $meeting = $this->Meetings->patchEntity($meeting, $data);

              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));

                   if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /*
       * Add Success
       */

      public function addSuccess($id=null){

          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /**
       * Edit method
       *
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
       * @throws \Cake\Network\Exception\NotFoundException When record not found.
       */
      public function edit($id = null)
      {
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              
              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $this->Flash->success(__('The meeting has been saved.'));
                  return $this->redirect(['action' => 'index']);
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /*
       * Email Invitaion
       */

      public function emailInvitation($id = null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;

              //Convert time to H:i:s format, Need to remove space from get meeting_time and then convert.
              $meeting_time_array = explode(":",$this->request->data('meeting_time'));
              @$meeting_time_string = trim($meeting_time_array[0]).':'.trim($meeting_time_array[1]);
              $meeting_time = new Time(date('H:i:s', strtotime($meeting_time_string)));

              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              $data['meeting_time'] = $meeting_time;
              $meeting_id=$meeting->meeting_id;
              $meeting_name=$meeting->name;
               $password=$meeting->password;
              $meeting_date=$meeting->meeting_date;
              $to=explode(',',$data['attendees']);
              
              if (isset($this->request->data['sendmail'])) {
                  $sub=$data['email_subject'];
                      $email_message=$data['email_message'];
                      $this->sendMail($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name,$password);

                      $this->Flash->success(__('The meeting Invitaion mail send successfully.'));
                    
                  }
                  else
                  {
                  $meeting = $this->Meetings->patchEntity($meeting, $data);
                  if ($this->Meetings->save($meeting)) {

                      $this->Flash->success(__('The meeting has been saved.'));

                 if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
          } 
          else 
          {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
           }
       }
   }

          $timezoneList = $this->timezoneList();
          $this->set(compact('meeting','timezoneList'));
          $this->set('_serialize', ['email_invitation']);
      }


   /*
       * Email Meeting Notes
       */

      public function emailMeetingNote($id = null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;

              //Convert time to H:i:s format, Need to remove space from get meeting_time and then convert.
              $meeting_time_array = explode(":",$this->request->data('meeting_time'));
              @$meeting_time_string = trim($meeting_time_array[0]).':'.trim($meeting_time_array[1]);
              $meeting_time = new Time(date('H:i:s', strtotime($meeting_time_string)));

              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              $data['meeting_time'] = $meeting_time;
              $meeting_id=$meeting->meeting_id;
              $meeting_name=$meeting->name;
              $meeting_date=$meeting->meeting_date;
              $to=explode(',',$meeting->attendees);
              
              if (isset($this->request->data['sendmail'])) {
                  $sub=$data['notes_subject'];
                      $email_message=$data['notes_message'];
                      $this->sendMailNote($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name);

                      $this->Flash->success(__('The meeting Note mail send successfully.'));
                    
                  }
                  else
                  {
                  $meeting = $this->Meetings->patchEntity($meeting, $data);
                  if ($this->Meetings->save($meeting)) {

                      $this->Flash->success(__('The meeting has been saved.'));

                 if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
          } 
          else 
          {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
           }
       }
   }

          $timezoneList = $this->timezoneList();
          $this->set(compact('meeting','timezoneList'));
          $this->set('_serialize', ['email_invitation']);
      }

      /**
       * Delete method
       *
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|null Redirects to index.
       * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
       */
      public function delete($id = null)
      {
          //$this->request->allowMethod(['post', 'delete']);
          $meeting = $this->Meetings->get($id);
          if ($this->Meetings->delete($meeting)) {
              $this->Flash->success(__('The meeting has been deleted.'));
          } else {
              $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
          }
          return $this->redirect(['controller'=>'Meetings','action' => 'schedule_meeting',"upcoming"]);
      }
      // ============================================= 
      /**
      * class   : 
      * menthod : joinMeeting
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================    
      public function joinMeeting($meeting_id='',$AEmail,$username)
      {   
          $meeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->first();

          $meetings = $this->Meetings->find()->
          where(['deleted' => '0','created_by'=>$this->Auth->user('id'),'is_meeting_room_active'=>'1','meeting_id !='=>$meeting_id])->count();

          $InActive_meeting=$meeting_id;
          $bbb = new BigBlueButton;

          if($meetings > '0')
            {
             $this->Flash->error(__('You Can Not Join More Than one Meeting at Same Time.'));
              return $this->redirect(Router::fullbaseUrl());;
                       
             }

                if($meeting->password != '')
                {
                   $password=$meeting->password;
                }
                else
                {
                   $password='1234';
                }

                $mpassword='swM6dfAKVF@$';
             $logoutUrl='http://'.$_SERVER['HTTP_HOST'].'/meetings/endMeeting/'.$meeting->meeting_id.'/'.$mpassword;
            
              //$logoutUrl="http://webinar.mj:40000";
           $meeting->meeting_time;
           $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));

          $finaltime=$meeting_date.' '.$meeting->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          

	         $meeting->meeting_time;
           $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));

        if($meeting->repeatTime == '')
        {  
            $finaltime=$meeting_date.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $enddate= date('Y-m-d H:i',strtotime ( '180 minute' , strtotime ( $finaldate ) )) ;


        }

         if($meeting->repeatTime == 'weekly')
        {  
            
            $day=date('D');
            if($day == $meeting->repeatDay)
            {
             
              $meeting_date= date('Y-m-d');
              $finaltime=$meeting_date.' '.$meeting->meeting_time;

              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $enddate= date('Y-m-d H:i',strtotime ( '180 minute' , strtotime ( $finaldate ) )) ;
             }
             else
             {

              $finaldate="''";
              $enddate="''";
             }

        }

         if($meeting->repeatTime == 'daily')
        {  
           $meeting_date= date('Y-m-d');
           $finaltime=$meeting_date.' '.$meeting->meeting_time;

            
             $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
             $enddate= date('Y-m-d H:i',strtotime ( '180 minute' , strtotime ( $finaldate ) )) ;


        }

          if($meeting->repeatTime == 'monthly')
        {  
             $meeting->monthrepeat;
             $day=date('F Y');

            $monthrepeat=explode(' ',$meeting->monthrepeat);
           
            if($monthrepeat['1'] != '')
            {
               $meeting->monthrepeat. ' of '. $day;
               $meeting_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of '. $day));
                        
              $finaltime=$meeting_date.' '.$meeting->meeting_time;

               $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
                $enddate= date('Y-m-d H:i',strtotime ( '180 minute' , strtotime ( $finaldate ) )) ;
            }
            else
            {
               $monthrepeat=explode('th',$meeting->monthrepeat);
               $day=date('Y-m-'.$monthrepeat[0]);
              
              $finaltime=$day.' '.$meeting->meeting_time;

               $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
                $enddate= date('Y-m-d H:i',strtotime ( '180 minute' , strtotime ( $finaldate ) )) ;

            }
        }

           
       
		
         date_default_timezone_set($meeting->timezone);
          $today = date('Y-m-d H:i');
	    if($today >= $finaldate && $today <= $enddate)
      {
                 
                  if(!empty($meeting)){
                      

                      $bbb = new BigBlueButton;
                      $meeting = json_decode(json_encode($meeting));
                  
                    //CREATE MEETING
                      $create_meeting = new CreateMeetingParameters($meeting_id,$meeting->name);
                      $duaration=$meeting->meeting_duration+30;
     $create_meeting->setModeratorPassword($mpassword)->setrecord('true')->setautoStartRecording('false')->setattendeePassword($password)->setduration($duaration)->setWelcomeMessage('Welcome to meeting')->setlogoutUrl($logoutUrl);
            
                     
                          $xml = '';
                          
                             $librariesTable = TableRegistry::get('libraries');
                              $librariedata = $librariesTable->find()
                                  ->where(['created_by'=>$this->Auth->user('id')])->all();
                              
                            $xml = "<?xml version='1.0' encoding='UTF-8'?>
                            <modules>
                            <module name='presentation'>";
                      foreach ($librariedata as $key => $value) {
                        $file_url = Router::url('/', true).'webroot/files/libraries/file/'.$value->file; 
                            $xml.="<document name ='default Document' url='".$file_url."' ></document>";
                           }
                            $xml.="</module>
                            </modules>";
                         $result = $bbb->createMeeting($create_meeting,$xml);
                 

                        // call default config xml file 
                        $configXMLuRL = $bbb->getDefaultConfigXMLUrl(ApiMethod::GET_DEFAULT_CONFIG_XML);
                        $doc=$bbb->getDefaultConfigXML($configXMLuRL);

           // change config parameter like default layout, lock feature            
            if($meeting->sections != '')
            {
                (string)$doc->layout->attributes()->defaultLayout='bbb.layout.name.webcamsfocus';
            }
              
     
            if($meeting->skin != '')
            {
              (string)$doc->skinning->attributes()->url='http://192.99.62.100/client/branding/css/'.$meeting->skin;
            }
              //make new config.xml file after changed layout and lock features 

                $newXML = $doc;

                str_replace("\n", "", $newXML); //These 4 commands are based on the jsp BBB API.
                str_replace("\t", "", $newXML); // I also tried not to include these, but nothing changed
                str_replace(">  <", "><", $newXML);
                str_replace(">    <", "><", $newXML);
                $setConfigXMLParams = array(
                'meetingId' => $meeting_id,
                'configXML' => $doc
                
                );
                       
               // set new config.xml and get config token for join Meeting
                
            $newConfigXMLResponse = $bbb->setConfigXMLWithXmlResponseArray($setConfigXMLParams,$newXML);
            $configToken = (string)$newConfigXMLResponse->configToken; 
         
             if($AEmail != '')
             {
                 $name = (!empty($this->Auth->user('username')))?$this->Auth->user('firstname').' '.$this->Auth->user('username'):$username;


                       //JOIN MEETING
                      if(!empty($result)){

                          $join_meeting = new JoinMeetingParameters($meeting_id,$name,$password,$configToken);
                          $meeting_url = $bbb->getJoinMeetingURL($join_meeting);

                              $id=$meeting->id;
                              $meetingTable = TableRegistry::get('Meetings');
                              $meetingdata = $meetingTable->get($id); 
                              
                              $meetingdata->meeting_exit_url = $meeting_url;
					
				 $copy_attendees = TableRegistry::get('copy_attendees');
                    
          $roledata = $copy_attendees->find()
                                ->where(['role =' => 'admin','meeting_id =' => $id,'created_by'=>$meeting->created_by])
                                ->count();


              if($roledata == '0')
              {
                              $attendees = $copy_attendees->newEntity();
                              $attendees->role='user';
                              $attendees->created_at = new Time(date('Y-m-d H:i:s'));
                              $attendees->meeting_id=$meeting_id;
                              $attendees->timezone = $meeting->timezone;
                              $attendees->created_by=$meeting->created_by;
                              
                              if($copy_attendees->save($attendees))
                              {

                              }
                   }           

                            
                              if($meetingTable->save($meetingdata))
                              {
                                 $this->redirect($meeting_url);  
                              
                              }

                          //meeting_info = new getMeetingInfoParameters($meeting_id,123456);
                          //meeting_infodata = $bbb->getMeetingInfo($meeting_info);
                       }     
                     
                  }
                  else
                  {
                     
                       $name = (!empty($this->Auth->user('firstname')))?$this->Auth->user('firstname').' '.$this->Auth->user('lastname'):Guest;

                      
                      //JOIN MEETING
                      if(!empty($result)){

                          $join_meeting = new JoinMeetingParameters($meeting_id,$name,$mpassword,$configToken);
                          $meeting_url = $bbb->getJoinMeetingURL($join_meeting);

                              $id=$meeting->id;
                              $meetingTable = TableRegistry::get('Meetings');
                              $meetingdata = $meetingTable->get($id); 
                              
                              $meetingdata->meeting_exit_url = $meeting_url;
                              $meetingdata->is_meeting_room_active = '1';

			                        $copy_attendees = TableRegistry::get('copy_attendees');

                           $roledata = $copy_attendees->find()
                                ->where(['role' => 'admin','meeting_id' => $meeting_id,'created_by'=>$meeting->created_by])
                                ->count();
                               
                        
                             if($roledata == '0')
                             {   
                              $attendees = $copy_attendees->newEntity();
                              $attendees->role='admin';
                              $attendees->created_at = new Time(date('Y-m-d H:i:s'));
                              $attendees->meeting_id=$meeting_id;
                              $attendees->created_by=$meeting->created_by;
                              $attendees->timezone = $meeting->timezone;
                              
                              if($copy_attendees->save($attendees))
                              {

                             }
                            }  
                              if($meetingTable->save($meetingdata))
                              {
                                 $this->redirect($meeting_url);  
                              
                              }

                          //meeting_info = new getMeetingInfoParameters($meeting_id,123456);
                          //meeting_infodata = $bbb->getMeetingInfo($meeting_info);
                       }     
                         
                      }

          }
      }
      else
      {
       
          $this->Flash->error(__('This Meeting Room is currently Closed.'));
          return $this->redirect(['controller'=>'Dashboard','action' => 'index']);
      }

        
     }


      /*
           join as attendee   
           http://test-install.blindsidenetworks.com/bigbluebutton/api/join?fullName=User+3466007&meetingID=random-8567159&password=ap&redirect=true&checksum=dde1d96e2e09ecb5bafbb8978a28dfa7802acbf5
      */

           public function JoinAttendee($meeting_id)
           { 

           $this->viewBuilder()->layout('join_attendee');
             
             $meeting = $this->Meetings->find()
              ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
              ->first();

            $meeting->meeting_time;
            $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));

    
                    
               $countmeeting = $this->Meetings->find()
              ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
              ->count(); 

            if($countmeeting > 0)
            {
              $meeting = $this->Meetings->find()
              ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
              ->first();
              
               $password=$meeting->password;
               $this->set(compact('meeting'));
               $this->set('_serialize', ['JoinAttendee']);        

               if ($this->request->is(['patch', 'post', 'put'])) {
                
                  $data = $this->request->data;


                    $attendeesTable = TableRegistry::get('attendees');
                  /*   $finaldate;
                     $enddate;

                      //echo 'created_at BETWEEN ? AND ?'. array($finaldate,$enddate);
                          echo  $attData= $attendeesTable->find()
                      ->where([
                          'meeting_id =' => $meeting_id,
                          'email =' => $data['email']
                      ])
                      ->where(function ($exp, $q) use($finaldate, $enddate) {
                          $exp->between('created_at ', $finaldate, $enddate);
                          return $exp;
                      })
                      ->count();*/

           
                    $email= $data['email'];
                    $password= $data['password'];
                    $username= $data['username'];
                    

               if($attData > '0') 
                 {   
                    $updated_at= new Time(date('Y-m-d H:i:s'));
                    $query = $attendeesTable->query();
                    $query->update()
                    ->set(['updated_at' =>$updated_at])
                    ->where(['meeting_id =' => $meeting_id,'email =' => $data[email]])
                    ->execute();

               }
              else
              {     
                    

                    $data['created_at'] = new Time(date('Y-m-d H:i:s'));
                    $data['meeting_id']=$meeting_id;
                    $email= $data['email'];
                    $password= $data['password'];
                    $attendees = $attendeesTable->newEntity();
                    $attendees = $attendeesTable->patchEntity($attendees, $data);
                       if($attendeesTable->save($attendees))
                       {
                           $id = $attendees->id;  
                          
                       }
              }  
              return $this->redirect('http://'.$_SERVER['HTTP_HOST'].'/meetings/joinMeeting/'.$meeting_id.'/'.$email.'/'.$username); 
              //return $this->redirect('http://webinar.mj:40000/meetings/joinMeeting/'.$meeting_id.'/'.$email.'/'.$username); 
              //return $this->redirect(['action' => 'joinMeeting'],[$meeting_id,$email,$password]);     
             } 
          }
      
          else
          {
                $this->Flash->error(__('This Meeting Room is currently Closed.'));
                echo "<script type='text/javascript'>
                                        alert('This Meeting Room is currently Closed.');
                                        </script>"; 
                //return $this->redirect(['controller'=>'Dashboard','action' => 'index']);
          } 

      }

            public function endMeeting($meetingId,$password)
            {

                echo "<script type='text/javascript'>
                alert('Meetings was ended.');
                </script>";
                //return $this->redirect(['controller'=>'dashboard','action' => 'index']);
                 return $this->redirect(['controller'=>'dashboard','action' => 'index']);
                exit;
            }

          public function endMeetings($meetingId,$password)
            {
               if($this->Auth->user('role') == 'admin')
                {
                   $password='swM6dfAKVF@$';
                         $bbb = new BigBlueButton;
                               
                            //END MEETING
                              $end_meeting = new endMeetingParameters($meetingId,$password);
                               $EndMeetingResponse = $bbb-> getEndMeetingURL($end_meeting);
                               $response=$bbb->endMeeting($end_meeting);
                               
                                $arr=xml2array($response);
                                $vals= array_values($arr);

                              $message='Meetings was ended successfully.';
                               $copy_attendees = TableRegistry::get('copy_attendees');
                                 $copy_attendees->deleteAll(array('meeting_id =' => $meetingId));

                              $meetingTable = TableRegistry::get('Meetings');
                              $meeting =  $meetingTable->find()
                                              ->where(['meeting_id =' => $meetingId])  
                                              ->first();
                                $id=$meeting->id;
                                $meetingdata = $meetingTable->get($id); 
                              
                                $meetingdata->is_meeting_room_active = '0';
                                $meetingTable->save($meetingdata);
                            
                                echo "<script type='text/javascript'>
                                alert('$message');
                               </script>";   
                                    return $this->redirect(['controller'=>'dashboard','action' => 'index']);
                                exit;
                            }    
                     else
                     {                            
                                    echo "<script type='text/javascript'>
                                        alert('Meetings was ended.');
                                      </script>";  
                                   return $this->redirect(['controller'=>'dashboard','action' => 'index']);                                     
                                         exit;
                              
                              // }
                    
                     }
            }

   /**
       * Add Meeting layout 
       */

      public function meetingLayout($id=null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
        // $selectedsections =$meeting->sections;
          $selectedlockfeature = $meeting->lockfeature;

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              // $sections=implode(',',$data['sections']);
                      //$data['sections']=$sections;
                      $lockfeature=implode(',',$data['lockfeature']);
                      $data['lockfeature']=$lockfeature;

                        if($data['bgselect'] == 'color')
                      {
                        $data['color']  =$data['colorval'];
                        $data['background']='';
                      }
                       if($data['bgselect'] == 'image')
                      {
                         // $data['background']= $data['background']['name'];
                           $data['color']='';
                      }
              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));
                 // return $this->redirect(['action' => 'addSuccess',$id]);
                  if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting','selectedlockfeature'));
          $this->set('_serialize', ['meeting']);
      }

       /**
       * Add Meeting polls 
       */
       public function meetingPolls($id=null){
                  $meeting = $this->Meetings->get($id, [
                      'contain' => []
                  ]);
                 
                 $data1=$this->request->data();

                  $poll = TableRegistry::get('Polls');
                  $poll->recursive=-1;
                  $polldata = $poll->find('all', array('conditions'=>array('meeting_id'=>$id),
                      'group' => ['poll_id']));
               foreach($polldata as $data){ 
                   $pollans = $poll->find('all', array('conditions'=>array('poll_id'=>$data->poll_id,'meeting_id' =>$data->meeting_id)));
       
                   foreach ($pollans as $key => $value) {
                     //echo "<pre>";
        //  print_r($value);
          // echo "</pre>";
                  }
               } 
              $meetingpoll = $poll->newEntity();
                
         
          if ($this->request->is(['patch', 'post', 'put'])) {
              
              $data = $this->request->data;

            
                  $meetingpoll = $poll->patchEntity($meeting, $data);
                  $meetingpoll1=array();
                  
                     foreach ($meetingpoll['polls'] as $entity) {
                          
                         array_push($meetingpoll1,array('poll_id'=>$entity['pollid'],'meeting_id'=>$entity['meeting_id'],'description'=>$entity['description'],'answer'=>$entity['answer']));
                   
                      }
                   
                   $meetingDATA = $poll->newEntities($meetingpoll1);
                   foreach ($meetingDATA as $value) {
                          $poll->save($value);
                          }
          
      
          
             /* if ($poll->save($meetingpoll)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));
                  return $this->redirect(['action' => 'newmeetingroomschedule',$id]);
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }*/
          }
          $this->set(compact('meeting','polldata','pollkey',' poll'));
          $this->set('_serialize', ['meeting']);
      }

  /***
      find answer by poll_id
  ***/
  public function findans()
  {   
      $this->viewBuilder()->layout(false); 
      $poll_id=$_POST['poll_id'];
      $meeting_id=$_POST['meeting_id'];
    
      $poll = TableRegistry::get('Polls');
                  $poll->recursive=-1;
      $pollans = $poll->find('all', array('conditions'=>array('poll_id'=>$poll_id,'meeting_id' =>$meeting_id)));
      $anskey=1;

      foreach ($pollans as $key => $value) {
         
              $pollans1='<div id="ansdiv'.$value->poll_id.$anskey.'" class="form-group" style="">
                      <label>Answer </label>
                      <div class="row">
                        <div class="col-sm-10">
                              <div class="input text"><input type="text" id="polls-anskey-answer" data-bvalidator="required" value="'.$value->answer.'" class="form-control" name="polls['.$value->poll_id.'][answer]"></div><input type="hidden" value="'.$value->poll_id.'" name="polls['.$value->poll_id.'][meeting_id]">
                             <input type="hidden" value="'.$value->poll_id.'" name="polls['.$value->poll_id.'][pollid]">
                          </div>
                          <div class="col-sm-2">
                            <label class="bottom_text"></label>
                             <input type="button" value="Remove Answer" name="'.$value->poll_id.$anskey.'" id="removeans" class="btn btn-danger">
                          </div>
                          
                      </div>                    
                  </div>';
        $anskey++;          
       }
      echo $pollans1;
      $this->set(compact('pollans'));
      $this->set('_serialize', ['meetingPolls']);

      //print_r($pollans);
  }

    


      /*
       * For Used Timezone List
       */

     function timezoneList()
      {
          $timezoneIdentifiers = DateTimeZone::listIdentifiers();
          $utcTime = new DateTime('now', new DateTimeZone('UTC'));

          $tempTimezones = array();
          foreach ($timezoneIdentifiers as $timezoneIdentifier) {
              $currentTimezone = new DateTimeZone($timezoneIdentifier);

              $tempTimezones[] = array(
                  'offset' => (int)$currentTimezone->getOffset($utcTime),
                  'identifier' => $timezoneIdentifier
              );
          }

          // Sort the array by offset,identifier ascending
          usort($tempTimezones, function($a, $b) {
              return ($a['offset'] == $b['offset'])
                  ? strcmp($a['identifier'], $b['identifier'])
                  : $a['offset'] - $b['offset'];
          });

          $timezoneList = array();
          foreach ($tempTimezones as $tz) {
              $sign = ($tz['offset'] > 0) ? '+' : '-';
              $offset = gmdate('H:i', abs($tz['offset']));
              $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
                  $tz['identifier'];
          }

          return $timezoneList;
      }

       public function scheduleMeeting($type)
      {
         
          $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
          $today=$date->format('Y-m-d');

          $Pastmeeting = $this->Meetings->find()
          ->where(['meeting_date <' => $today,'deleted =' => '0']);

       $upComingmeeting = $this->Meetings->find()
          ->where(['deleted =' => '0','repeatTime !='=>' ']);
          
            
           if($type == 'upcoming')
               {
                   $this->set(compact('upComingmeeting','type', $this->               paginate($upComingmeeting)));
               }
                if($type == 'past')
               {
                $this->set(compact('Pastmeeting','type', $this->paginate($Pastmeeting)));
                 }

              $bbb = new BigBlueButton;

              $data=array();

             /* foreach ($Pastmeeting as $row) {
               
                  $meeting=$row->meeting_id;
                  $password=$row->password;
                  $meeting_id=json_decode(json_encode($meeting)); 

                 // print_r($meeting_id);
                 // $meeting_info = new GetMeetingInfoParameters($meeting_id,$password);
                  //$Pastmeetingdata = $bbb->getMeetingInfo($meeting_info);

                  //$meeting_info = new getMeetingsParameters($meeting_id,$password);

                 /* echo "<pre>";
                  print_r($Pastmeetingdata);
                  echo "</pre>";*/

               
             //  }*/
              
                 
               $this->set('_serialize', ['scheduleMeeting']);

      }
   function sendMail($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name,$password)
      {
          try {
              //SEND MAIL TO USER
              $email = new Email('default');
              $email->from(['webinar@gmail.com' => 'Webinar'])
                    ->to($to)
                    ->template('email_invitation')
                    ->emailFormat('html')
                    ->subject($sub)
                    ->viewVars(['meeting_id'=>$meeting_id,'email_message'=>$email_message,'meeting_time'=>$meeting_time,'meeting_date'=>$meeting_date,'meeting_name'=>$meeting_name,'password'=>$password])
                    ->send(); 
          } catch (Exception $e) {
              echo 'Exception : ',  $e->getMessage(), "\n";
          }
      }

      function sendMailNote($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name)
      {
          try {
              //SEND MAIL TO USER
              $email = new Email('default');
              $email->from(['webinar@gmail.com' => 'Webinar'])
                    ->to($to)
                    ->template('email_note')
                    ->emailFormat('html')
                    ->subject($sub)
                    ->viewVars(['meeting_id'=>$meeting_id,'email_message'=>$email_message,'meeting_time'=>$meeting_time,'meeting_date'=>$meeting_date,'meeting_name'=>$meeting_name])
                    ->send(); 
          } catch (Exception $e) {
              echo 'Exception : ',  $e->getMessage(), "\n";
          }
      }
     
  function attendencelist()
      {   
         $id=$_POST['id'];
        
          $query = TableRegistry::get('Meetings');
                          $meetings = $query->find();
                          $meetings->select('attendees');
                          $meetings->where(['id' => $id]);
                         // print_r($meetings);
                        $meetings->count();

                          foreach ($meetings as $row){

                           $meeting=$row->attendees;
                          }
                         echo $meeting;
                          exit;

      }       

   public function contentLibrary()
      {

          $librariesTable = TableRegistry::get('libraries');
          $librarie = $librariesTable->newEntity();
          $librariesTable->addBehavior('Josegonzalez/Upload.Upload', [    
             'file'=>[],
                  ]);
               $librarieData = $librariesTable->find()->where(['created_by'=>$this->Auth->user('id')])->order(['id' => 'DESC']);

             if ($this->request->is('post')) {
              
              $data = $this->request->data;
              $data['created_at'] = new Time(date('Y-m-d H:i:s'));

              $data['size']= $data['file']['name'];
                
              $librarie = $librariesTable->patchEntity($librarie, $data);
            

              if ($librariesTable->save($librarie)) {
                  $id = $librarie->id;
                 // $this->Flash->success(__('The librarie file has been saved.'));
                 // return $this->redirect(['action' => 'contentLibrary']);
              } else {
                  //$this->Flash->error(__('The contentLibrary file could not be saved. Please, try again.'));
              }
          }


          $this->set(compact('librarieData', $this->paginate($librarieData)));
          $this->set('_serialize', ['contentLibrary']);
      }

     
	   public function DeleteLibrary($id)
      {

           //$this->request->allowMethod(['post', 'delete']);
          $librariesTable = TableRegistry::get('libraries');
          $libraries = $librariesTable->get($id);
          if ($librariesTable->delete($libraries)) {
              $this->Flash->success(__('The libraries file has been deleted.'));
          } else {
              $this->Flash->error(__('The libraries file could not be deleted. Please, try again.'));
          } 
          return $this->redirect(['controller'=>'meetings','action' => 'contentLibrary']);

      }

      
      public function cronjob()
      {
          $this->viewBuilder()->layout(false);
            $copy_attendees = TableRegistry::get('copy_attendees');
           $roledata = $copy_attendees->find()
              ->where(['role =' => 'admin'])
              ->count();
              
            if($roledata == '0')
            {
             
                  $userdata = $copy_attendees->find()
                  ->where(['role =' => 'user']);
               foreach ($userdata as $key => $roledata) {
              

                  $finaltime=$roledata->created_at;
                  $created_by=$roledata->created_by;
                  $meetingId=$roledata->meeting_id;
                  $enterTime= date('Y-m-d H:i:s',strtotime ( '+30 minute' ,strtotime ( $finaltime )) );
                   date_default_timezone_set($roledata->timezone);
                   $currentTime=date('Y-m-d H:i:s');
            
               if($currentTime > $enterTime)
               {

                    $bbb = new BigBlueButton;
                     
                     $password='swM6dfAKVF@$';

                    //END MEETING
                      $end_meeting = new endMeetingParameters($meetingId,$password);
                       $EndMeetingResponse = $bbb-> getEndMeetingURL($end_meeting);
                       $response=$bbb->endMeeting($end_meeting);
                       
                        $arr=xml2array($response);
                        $vals= array_values($arr);
                         $copy_attendees->deleteAll(array('meeting_id =' => $meetingId));
    
                     echo $message='Meetings was ended successfully.';
               }
             }  
                exit; 
        }
            
               else
               {
                
                    $userdata = $copy_attendees->find()
                  ->where(['role =' => 'admin']);
              
               foreach ($userdata as $key => $roledata) {
              
                  $created_by=$roledata->created_by;
                  $finaltime=$roledata->created_at;
                  $meetingId=$roledata->meeting_id;
                  $MeetingsTable = TableRegistry::get('meetings');
                    $meeting = $MeetingsTable->find()
                      ->where(['meeting_id =' => $meetingId])
                      ->first();
                      
                    $meeting_time=$meeting->meeting_time;
                     $meeting_duration=($meeting->meeting_duration+30);
               $enterTime= date('Y-m-d H:i:s',strtotime ( '+'.$meeting_duration.' minute' ,strtotime ( $meeting_time )) );
                   date_default_timezone_set($roledata->timezone);
                  $currentTime=date('Y-m-d H:i:s');
               
                if($currentTime > $enterTime)
                   {

                        $bbb = new BigBlueButton;
                         
                        $password='swM6dfAKVF@$';

                        //END MEETING
                          $end_meeting = new endMeetingParameters($meetingId,$password);
                           $EndMeetingResponse = $bbb-> getEndMeetingURL($end_meeting);
                           $response=$bbb->endMeeting($end_meeting);
                           
                            $arr=xml2array($response);
                            $vals= array_values($arr);
                             //$copy_attendees->deleteAll(array('meeting_id =' => $meetingId));
                            $id=$meeting->id;
                            $meetingdata = $MeetingsTable->get($id); 
                              
                                $meetingdata->is_meeting_room_active = '0';
                                $MeetingsTable->save($meetingdata);

                         echo $message='Meetings was ended successfully.';
                   }
                 }  
                   exit;
             
      }
    }

    // ============================================= 
    /** menthod : downloadRecording
    * @param  : 
    * @Description : method for download recording
    */// ==============================================
    public function downloadRecording()
    {
        $presentation_folder = "/var/bigbluebutton/published/presentation";
        $recorded_files = "/home/olympic/www/webroot/files/recordings";


        $meetings = glob($presentation_folder . '/*' , GLOB_ONLYDIR);

        foreach ($meetings as $key => $value) {
            $meeting_id_ary = explode('/', $value);
            $meeting_id = end($meeting_id_ary);

            if(!file_exists($recorded_files."/".$meeting_id."/"."download.mp4")){
                shell_exec("mkdir $recorded_files/$meeting_id");
                $presentation_images_video = $this->generateVideoFromImages($value,$recorded_files."/".$meeting_id);   
            }
        }
        echo "<pre>";print_r(); echo "</pre>";exit;
    } 
    // ============================================= 
   /** menthod : generateVideoFromImages
   * @param  : 
   * @Description : GET ALL iMAGES FROM FOLDER AND GENERATE VIDEO FROM IT REGARDING IT'S DURATION
   */// ==============================================           
   public function generateVideoFromImages($meeting_path='',$destination_path='')
   {
       if (!empty($meeting_path)) {
            $image_xml = simplexml_load_file($meeting_path."/"."shapes.svg");
            $image_ary = json_decode(json_encode((array)$image_xml), TRUE);
            if(!isset($image_ary['image'])) return FALSE;
            
            shell_exec("mkdir $destination_path/raw-presentation");
            
            $cnt =1;
            $time_ary = array();
            foreach ($image_ary['image'] as $key => $value) {
                 if (isset($value['@attributes']['text'])) {
                    $in_time = $value['@attributes']['in'];
                    $out_time = $value['@attributes']['out'];
                    $text_file = $value['@attributes']['text'];

                    if(strpos($in_time, ' ')!==FALSE){
                        $intime = explode(' ', $in_time);
                        $outtime = explode(' ', $out_time);
                        
                        foreach ($intime as $key => $value) {
                            $value = round($value);
                            $time_ary[$value][$cnt]['in_time'] =  round($value);
                            $time_ary[$value][$cnt]['out_time'] =  round($outtime[$key]);
                            $time_ary[$value][$cnt]['text_file'] =  $text_file;
                            $time_ary[$value][$cnt]['destination_path'] =  $destination_path;
                            
                            $cnt++;
                        }
                    }else{
                        $in_time = round($in_time);
                        $time_ary[$in_time][$cnt]['in_time'] = round($in_time);
                        $time_ary[$in_time][$cnt]['out_time'] = round($out_time);
                        $time_ary[$in_time][$cnt]['text_file'] = $text_file;
                        $time_ary[$in_time][$cnt]['destination_path'] =  $destination_path;
                        
                        $cnt++;
                    }
                 }
            }
            ksort($time_ary);
            // TIME ARRAY 
            $img_cnt=1;
            foreach ($time_ary as $key => $value) {
                foreach ($value as $k => $val) {
                    $text_file = $val['text_file'];
                    $text_file_array = explode('/',$text_file);
                    $image_name = explode('.',end($text_file_array));

                    $time = $val['out_time']-$val['in_time'];

                    // CONVERT IMAGE TO VIDEO WITH IT'S RESPECTIVE IN OUT TIME
                    if (!empty($time)) {
                        if ( (!file_exists("$val[destination_path]/raw-presentation/$image_name[0]-$img_cnt.mp4")) ) {
                            $image_path = $meeting_path."/".$text_file_array[0]."/".$text_file_array[1]."/".$image_name[0].".png";
                            exec("/usr/local/bin/ffmpeg -loop 1 -i $image_path -c:v libx264 -t $time -pix_fmt yuv420p -vf scale=1000:700 $destination_path/raw-presentation/$image_name[0]-$img_cnt.mp4");
                        }

                        // CONVERT VIDEO TO TXT FILE FOR CONCAT
                        if ( (!file_exists("$val[destination_path]/raw-presentation/$image_name[0]-$img_cnt.ts")) ) {
                            exec("/usr/local/bin/ffmpeg -i $val[destination_path]/raw-presentation/$image_name[0]-$img_cnt.mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts $destination_path/raw-presentation/$image_name[0]-$img_cnt.ts");
                        }
                    
                        $img_cnt++;
                    }
                }
            }


            $raw_image_txt_files = "$destination_path/raw-presentation";
            $raw_images = glob($raw_image_txt_files . '/*');
            
            /*COMBINE ALL IMAGE VIDEO AS VIDEO*/
            $img_video_ts_files = array();
            foreach ($raw_images as $key => $value) {
                if(strpos($value, '.ts')!==FALSE){
                    $img_video_ts_files[]=$value;
                }
            }

            if (!empty($img_video_ts_files)) {
                $all_files = implode('|',$img_video_ts_files) ;
                exec('/usr/local/bin/ffmpeg -i "concat:'.$all_files.'" -c copy -bsf:a aac_adtstoasc '.$destination_path.'/presentation_image.mp4');
            }
            return $destination_path.'/presentation_image.mp4';
       }
   }
}  

