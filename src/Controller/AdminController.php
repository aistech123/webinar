<?php
namespace App\Controller;
use Cake\I18n\Time;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use DateTimeZone;
use DateTime;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use Cake\View\Helper\UrlHelper;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
/**
 * AdminController 
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */ 
	  class AdminController extends AppController
	  {
	  	 public function adminindex()
	      {
	      	  if(empty($this->Auth->user('username'))){
           		 return $this->redirect(array('controller' => 'Admin', 'action' => 'adminlogin'));
       			 }
       			 else
       			 {   
			      	 $userid=$this->Auth->user('username');
			      	 $this->viewBuilder()->layout('adminLayout');
		      	 
			      	 $usersTable = TableRegistry::get('users');
			      	 $user = $usersTable->find();
		         	 $user->select([
		                'DisplayName' => 'users.username',
		                'UserID'=>'users.id',
		                'peekAttendees' =>$user->func()->count('attendees.meeting_date'),
		                'MaxAttendees' =>'users.max_attendees',
		              //  'MeetingRooms'=>$user->func()->count('attendees.meeting_id'),
		                'LastLogin'=>$user->func()->max('login.last_login_time'),
		               ])
	            //->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_id'=> $id,'attendees.meeting_date'=>$meeting_date])
	           	->group(['users.id'])
	             
	              ->join([
	                'meetings' => [
	                    'table' => 'meetings',
	                    'type' => 'left',
	                    'conditions' => 'meetings.created_by =users.id'
	                ],		             
	            	 'attendees' => [
	                    'table' => 'attendees',
	                    'type' => 'left',
	                    'conditions' => 'attendees.meeting_id =meetings.name'
	                ],  
	                 'login' => [
	                    'table' => 'login',
	                    'type' => 'left',
	                    'conditions' => 'login.user_id =users.id'
	                ],       
	            ]); 

	              
	              $countuser=$user->count();
	             // echo debug($user);
	                foreach ($user as $key => $value) {
	                	//echo "<pre>";print_r($value);
	                }
	               $this->set(compact('user','userid','rows'));
	            }   
	      }
	       public function adminlogin()
	      {
	      	  $this->viewBuilder()->layout('login');

	      	   if($this->Auth->user('username')){
           		 return $this->redirect(array('controller' => 'Admin', 'action' => 'index'));
       			 }

		        if ($this->request->is('post')) {

		        	 $adminsTable = TableRegistry::get('admins');
		        	 $admin =$adminsTable->find()
                   	->select()
                    ->where(['admins.username'=>$this->request->data['username'],'admins.password'=>$this->request->data['password']])
              		  ->first();

                	if(!empty($admin))
                	{
                		$this->Auth->setuser($this->request->data);
                		return $this->redirect(['controller'=>'Admin','action'=>'adminlogin']);
						 $this->Flash->success(__('Login Successfully...'))	;
                	}
                	else
                	{
                	  $this->Flash->error(__('Invalid username or password, try again'));
                	}

		       }
		        $this->set('_serialize', ['admin']);
		 }
		 public function ajaxedituser()
		 {
		 	$this->viewBuilder()->layout(false);
		 	
		 	$userTable = TableRegistry::get('users');
		 	$query = $userTable->query();
                      $query->update()
                      ->set(['max_attendees' => $_POST['max_attendees']])
                      ->where(['id' => $_POST['userid']])
                      ->execute();

		 	exit;
		 }

		 public function logout()
	      {
	      		$this->Auth->logout();
	      		 return $this->redirect(array('controller' => 'Admin', 'action' => 'adminlogin'));
	  	 }

	  }	
  ?>