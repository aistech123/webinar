<?php
namespace App\Controller;
use Cake\I18n\Time;
use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use DateTimeZone;
use DateTime;
use Cake\Utility\Security;
use Cake\Mailer\Email;
use Cake\View\Helper\UrlHelper;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
   
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    //LOGIN METHOD
    public function login()
    {
        $this->viewBuilder()->layout('login');

        if($this->Auth->user('id')){
            return $this->redirect(array('controller' => 'Dashboard', 'action' => 'index'));
        }

        if ($this->request->is('post')) {
            
            $user = $this->Auth->identify();
          
            if ($user) {
                $this->Auth->setUser($user);
                $this->Flash->success(__('Login Successfully...'));
                date_default_timezone_set($user['timezone']);
                $loginTable=TableRegistry::get('login');
                $query = $loginTable->query();
                $query->insert(['user_id', 'last_login_time','timezone'])
                    ->values([
                        'user_id' => $user['id'],
                        'last_login_time' =>new Time(date('Y-m-d H:i:s')),
                        'timezone' => $user['timezone'],
                    ])
                    ->execute();
              
                return $this->redirect($this->Auth->redirectUrl());

            }
            $this->Flash->error(__('Invalid email or password, try again'));
        }
        $this->set('_serialize', ['user']);
    }
      //LOGIN METHOD
    public function loginpage()
    {
       $this->viewBuilder()->layout('loginpage');
       
    }


    //User Register
    public function register(){
        $this->viewBuilder()->layout('login');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'login']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }

        $query = TableRegistry::get('Countries');
        $countries = $query->find('list');
        $this->set(compact('user','countries'));
        $this->set('_serialize', ['register']);
    }

    //User Manage Profile
    public function manageProfile($id=null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            unset($this->request->data['max_attendees']);
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'manageProfile',$id]);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $timezoneList = $this->timezoneList();
        $query = TableRegistry::get('Countries');
        $countries = $query->find('list');
        $this->set(compact('user','countries','timezoneList'));
        $this->set('_serialize', ['manage_profile']);
    }

    //User view Profile
    public function profile()
    {
        $id = $this->Auth->user('id');
        $user =$this->Users
            ->find('all')
            ->select(['countries.name'])
            ->select($this->Users)
            ->join([
                'table' => 'countries',
                'conditions' => ['Users.country = countries.id'],
            ])
            ->where(['Users.id'=>$id])
        ->first();

        $this->set(compact('user'));
        $this->set('_serialize', ['dashboard']);
    }

    //LOGOUT METHOD
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    //Forget Password
    public function forgetPassword()
    {
        if($this->request->is('post')){

            //VALIDATION
            $validator = new Validator();
            $validator->notEmpty('email');
            $error = $validator->errors($this->request->data);

            if(!empty($error)){
                $this->Flash->success(__('Email field is required.'));
                return $this->redirect(['action' => 'forgetPassword']);
            }

            $user = $this->Users->find()
            ->where(['email =' => $this->request->data['email']])
            ->toArray();

            if(!empty($user)){

                //GENERATE UNIQUE TOKEN
                $token = $this->generatePasswordToken();

                //INSERT TOKEN IN USER TABLE
                $query = $this->Users->query();
                $query->update()
                    ->set(['reset_password_token' => $token])
                    ->where(['email' => $this->request->data['email']])
                    ->execute();

                //$msg = "For Reset Password please click on <a href=http://".$_SERVER['SERVER_NAME'].":".$_SERVER['SERVER_PORT']."/users/resetPassword?reset_token=".$token.">here</a>";
                $sub = 'Reset Password';

                //SEND MAIL
                $this->sendMail($this->request->data['email'],$token,$sub);

                $this->Flash->success(__('Please Check Mail.'));
                return $this->redirect(['action' => 'forgetPassword']);
            }
            $this->Flash->error(__('Sorry, the email entered was not found in our record..'));
            return $this->redirect(['action' => 'forgetPassword']);

        }

        $this->viewBuilder()->layout('login');
        $this->set('_serialize', ['forget_password']);
    }

    /**
     * Generate a unique hash / token.
     * @param Object User
     * @return Object User
     */
    function generatePasswordToken() {

        // Generate a random string 100 chars in length.
        $token = "";
        for ($i = 0; $i < 100; $i++) {
            $d = rand(1, 100000) % 2;
            $d ? $token .= chr(rand(33,79)) : $token .= chr(rand(80,126));
        }
        (rand(1, 100000) % 2) ? $token = strrev($token) : $token = $token;
        // Generate hash of random string
        $hash = Security::hash($token, 'sha256', true);;
        for ($i = 0; $i < 20; $i++) {
            $hash = Security::hash($hash, 'sha256', true);
        }

        return $hash;
    }

    function timezoneList()
    {
        $timezoneIdentifiers = DateTimeZone::listIdentifiers();
        $utcTime = new DateTime('now', new DateTimeZone('UTC'));

        $tempTimezones = array();
        foreach ($timezoneIdentifiers as $timezoneIdentifier) {
            $currentTimezone = new DateTimeZone($timezoneIdentifier);

            $tempTimezones[] = array(
                'offset' => (int)$currentTimezone->getOffset($utcTime),
                'identifier' => $timezoneIdentifier
            );
        }

        // Sort the array by offset,identifier ascending
        usort($tempTimezones, function($a, $b) {
            return ($a['offset'] == $b['offset'])
                ? strcmp($a['identifier'], $b['identifier'])
                : $a['offset'] - $b['offset'];
        });

        $timezoneList = array();
        foreach ($tempTimezones as $tz) {
            $sign = ($tz['offset'] > 0) ? '+' : '-';
            $offset = gmdate('H:i', abs($tz['offset']));
            $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
                $tz['identifier'];
        }

        return $timezoneList;
    }
    // =============================================
    /**
    * class   :
    * menthod : sendMail
    * @param  : email address
    * @output :
    * @Description : method send mail to user
    **/
    // ==============================================
    function sendMail($to,$token,$sub)
    {
        try {
            //SEND MAIL TO USER
            $email = new Email('default');
            $email->from(['webinar@gmail.com' => 'Webinar'])
                  ->to($to)
                  ->template('reset_password')
                  ->emailFormat('html')
                  ->subject($sub)
                  ->viewVars(['token'=>$token])
                  ->send();
        } catch (Exception $e) {
            echo 'Exception : ',  $e->getMessage(), "\n";
        }
    }
    // =============================================
    /**
    * class   :
    * menthod :
    * @param  :
    * @output :
    * @Description :
    **/
    // ==============================================
    public function resetPassword()
    {
        if(isset($this->request->query['reset_token']) && !empty($this->request->query['reset_token'])){
            $token = $this->request->query['reset_token'];
            $email = 'manoj30808@gmail.com';//$this->request->query['email'];

            $user = $this->Users->find()
            ->where(['email =' => $email])
            ->where(['reset_password_token =' => $token])
            ->toArray();

            if(empty($user)){
                $this->Flash->success(__('Your Token Is Invalid or Expire.'));
                return $this->redirect(['action' => 'login']);
            }

            if($this->request->is('post')){
                $validator = new Validator();
                $validator->add('password', [
                    'compare' => [
                        'rule' => ['compareWith', 'confirm_password']
                    ]
                ])
                ->notEmpty('username')
                ->add('confirm_password', [
                    'compare' => [
                        'rule' => ['compareWith', 'password']
                    ]
                ])
                ->notEmpty('compareWith');
                $error = $validator->errors($this->request->data);

                if(empty($error)){

                    $hasher = new DefaultPasswordHasher();
                    $password = $hasher->hash($this->request->data['password']);

                    //Update USER Password
                    $query = $this->Users->query();
                    $query->update()
                        ->set(['password' => $password])
                        ->set(['reset_password_token' =>''])
                        ->where(['email =' => $email])
                        ->where(['reset_password_token =' => $token])
                        ->execute();

                    $this->Flash->error(__('Your Password Change Successfully Please Login.'));
                    return $this->redirect(['action' => 'login']);
                }
                $this->Flash->error(__('Your Password Does Not Match.'));

            }

        }
        $this->viewBuilder()->layout('login');
        $this->set('_serialize', ['reset_password']);
    }
}
