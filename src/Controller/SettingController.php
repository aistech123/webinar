<?php 
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */
class SettingController extends AppController
{


		  /** setting **/
         public function index($id='1')
            {
              $setting = $this->Setting->get($id, [
            'contain' => []
             ]);
        
               $query = TableRegistry::get('Meetings');
                $meetings = $query->find();
                $meetings->where(['deleted' => '0'])->order(['meeting_date' => 'DESC']);

              if ($this->request->is(['patch', 'post', 'put'])) {
                    
                    $data = $this->request->data;
                    
                    $setting = $this->Setting->patchEntity($setting, $data);
                    if ($this->Setting->save($setting)) {
                     
                        $this->Flash->success(__('The setting has been saved.'));
                        return $this->redirect(['action' => 'settingRegister',$id]);
                    } else {
                        $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                    }
                }
                $this->set(compact('setting','meetings'));
                $this->set('_serialize', ['setting']);
    }

             /** setting register**/
         public function settingRegister($id='1')
            {

            $setting = $this->Setting->get($id, [
            'contain' => []
             ]);
        
               $query = TableRegistry::get('Meetings');
                $meetings = $query->find();
                $meetings->where(['deleted' => '0'])->order(['meeting_date' => 'DESC']);

              if ($this->request->is(['patch', 'post', 'put'])) {
                    
                    $data = $this->request->data;
                    
                    $setting = $this->Setting->patchEntity($setting, $data);
                    if ($this->Setting->save($setting)) {
                     
                        $this->Flash->success(__('The setting has been saved.'));
                        return $this->redirect(['action' => 'setting_notes',$id]);
                    } 
                    else {
                        $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                    }
                }
                $this->set(compact('setting','meetings'));
                $this->set('_serialize', ['setting']);
            }

             /** setting Notes**/
         public function settingNotes($id='1')
            {

            $setting = $this->Setting->get($id, [
            'contain' => []
             ]);
        
               $query = TableRegistry::get('Meetings');
                $meetings = $query->find();
                $meetings->where(['deleted' => '0'])->order(['meeting_date' => 'DESC']);

              if ($this->request->is(['patch', 'post', 'put'])) {
                    
                    $data = $this->request->data;
                    
                    $setting = $this->Setting->patchEntity($setting, $data);
                    if ($this->Setting->save($setting)) {
                     
                        $this->Flash->success(__('The setting has been saved.'));
                        return $this->redirect(['action' => 'setting_layout',$id]);
                    } else {
                        $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                    }
                }
                $this->set(compact('setting','meetings'));
                $this->set('_serialize', ['setting']);
            }
              /** setting Layout**/
         public function settingLayout($id='1')
            {

                 $setting = $this->Setting->get($id, [
                    'contain' => []
                     ]);
                  $selectedsections =explode(',',$setting->sections);
                  $selectedlockfeature = explode(',',$setting->lockfeature);

                $query = TableRegistry::get('Meetings');
                $meetings = $query->find();
                $meetings->where(['deleted' => '0'])->order(['meeting_date' => 'DESC']);
 
              if ($this->request->is(['patch', 'post', 'put'])) {
                    
                    $data = $this->request->data;
                   // $sections=implode(',',$data['sections']);
                   // $data['sections']=$sections;
                    $lockfeature=implode(',',$data['lockfeature']);
                    $data['lockfeature']=$lockfeature;
                   /* if($data['bgselect'] == 'color')
                    {
                      $data['color']  =$data['colorval'];
                      $data['background']='';
                    }
                     if($data['bgselect'] == 'image')
                    {
                       // $data['background']= $data['background']['name'];
                         $data['color']='';
                    }*/
                  
                    $setting = $this->Setting->patchEntity($setting, $data);
                    if ($this->Setting->save($setting)) {
                     
                        $this->Flash->success(__('The setting has been saved.'));
                         return $this->redirect(['action' => 'addSuccess',$id]);;
                    } else {
                        $this->Flash->error(__('The setting could not be saved. Please, try again.'));
                    }
                }
                $this->set(compact('setting','meetings','selectedsections','selectedlockfeature'));
                $this->set('_serialize', ['setting']);
            }
             public function addSuccess($id=null){

                  $setting = $this->Setting->get($id, [
                    'contain' => []
                     ]);

                   $this->set(compact('setting'));
                $this->set('_serialize', ['setting']);
              }
}



?>