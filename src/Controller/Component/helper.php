<?php 

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

	/* ALL REPEATED METHOD SHOULD BE HERE */
	function actionDelete($param=array())
	{
		if(is_array($param)){
			echo "<form action=$param[url]/$param[id] method='post' id=delete-form-$param[id] >";
				echo "<a href='#'  class=$param[class] id=$param[id]><i class='$param[icon]' ></i><span>Delete</span></a> ";
			echo "</form>";
		}
		echo "";
	}

	
	function xml2array ( $xmlObject, $out = array () )
	{
	    foreach ( (array) $xmlObject as $index => $node )
	        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

	    return $out;
	}

	function meetingName($meeting_id)
	{

		$query = TableRegistry::get('Meetings');
	    $meetings = $query->find();
	    $meetings->select('name');
	    $meetings->where(['meeting_id' => $meeting_id]);
	  /// $meetings->count();
	    foreach ($meetings as $row){

	     $meeting=$row->name;
	    }
	   return $meeting;
	}
  function peekAttendee($user_id)
  {
      
      $query = TableRegistry::get('Meetings');
              $meetings = $query->find();
              $meetings->select('meeting_id');
              $meetings->where(['created_by' => $user_id]);
              $meetingarray=array();
              $meetingdatearray=array();
              foreach ($meetings as $row){
            
                array_push($meetingarray,$row->meeting_id);
               }
             
               $countmeetingarray=sizeof($meetingarray);
               $attendeesTable = TableRegistry::get('attendees');
              for($i=0;$i<$countmeetingarray;$i++)
             {
               $attendees = $attendeesTable->find();
                      $attendees->select([
                         'meeting_id'=>'attendees.meeting_id',
                          'meeting_date'=>'attendees.meeting_date',
                         // 'TotalAttendee'=>$attendees->func()->count('attendees.id'),
                         ])
                      ->where(['attendees.meeting_id'=> $meetingarray[$i]])
                      ->group(['attendees.meeting_date']);
                    foreach ($attendees as $row){
                      array_push($meetingdatearray,array('meeting_date'=>$row->meeting_date,'meeting_id'=>$row->meeting_id));
                 }  

            }
            
            for($j=0;$j<sizeof($meetingdatearray);$j++)
            {   
                      $attendees = $attendeesTable->find();
                      $attendees->select([
                          'created_at'=>'attendees.created_at',
                          'TotalAttendee'=>$attendees->func()->count('attendees.id'),
                         ])
                      ->where(['Meetings.created_by'=>$user_id,'attendees.meeting_id'=> $meetingdatearray[$j]['meeting_id'],'attendees.meeting_date'=> $meetingdatearray[$j]['meeting_date']])
                      ->group(['unix_timestamp(attendees.created_at) DIV 200'])
                      ->join([
                          'Meetings' => [
                              'table' => 'meetings',
                              'type' => 'inner',
                              'conditions' => 'Meetings.name = attendees.meeting_id'
                          ]
                         
                      ]);

                      $attendeeExit = $attendeesTable->find();
                      $attendeeExit->select([
                          'exit_time'=>'attendees.exit_time',
                          'TotalexitAttendee'=>$attendeeExit->func()->count('attendees.id'),
                         ])
                     ->where(['Meetings.created_by'=>$user_id,'attendees.meeting_id'=> $meetingdatearray[$j]['meeting_id'],'attendees.meeting_date'=> $meetingdatearray[$j]['meeting_date']])
                      ->group(['unix_timestamp(attendees.exit_time) DIV 200'])
                      ->join([
                          'Meetings' => [
                              'table' => 'meetings',
                              'type' => 'inner',
                              'conditions' => 'Meetings.name = attendees.meeting_id'
                          ]
                         
                      ]);
                      $exitArray=array(); 
                      $ecount=$attendeeExit->count(); 
                      $countexit='0';
                     foreach ($attendeeExit as $key => $value) {

                      $exitArray[$key]['exit_time'] =date('h:m:i',strtotime($value['exit_time']));
                      $exitArray[$key]['TotalexitAttendee']=$value['TotalexitAttendee'];
                      $countexit++;
                     }
                      $exitArrayTime=array(); 
                    foreach ($attendeeExit as $key => $value) {

                      $exitArrayTime[$key] =date('h:m:i',strtotime($value['exit_time']));
                    
                     }
          
                         //set data points for Attendees chart data 
                    $data_points[] = array();
                    $countArray=count($exitArray);
                    //$count=0;
                    $k=0;
                
                    foreach ($attendees as $key => $value) {
                       error_reporting(0);
                      // return print_r($value);
                  /*  if(! isset($value['created_at']))
                    {*/
                       $enter_time=date('h:m:i',strtotime(@$value['created_at']));
                       $exit_time=date('h:m:i',strtotime($exitArray[$key]['exit_time']));
                      
                        $findExitTime = in_array($enter_time,$exitArrayTime);
                        $timenew=date('h:m:i',strtotime(@$value['created_at']));

                        if((strtotime($exit_time)) < (strtotime($enter_time)))
                        {
                           $k=$k-($exitArray[$key]['TotalexitAttendee']);
                        
                        }else{
                           $k=$k+$value['TotalAttendee'];
                        
                        }
                        $point = array(abs($k));
                       // return print_r($point);
                        array_push($data_points, $point);
                      // return print_r($peekattendee= max($data_points));
                        }  
                   // } 
            }
            error_reporting(0);
             $peekattendee= max(@$data_points); 
            return $peekattendee['0'];
             
            
   
  }
  function MeetingsRoom($user_id)
  {
      $query = TableRegistry::get('Meetings');
      $meetings = $query->find();
      $meetings->select('meeting_id');
      $meetings->where(['created_by' => $user_id]);
      $count=$meetings->count();
     
     return $count;
  }
  function meetingTimezone($meeting_id)
  {
      $query = TableRegistry::get('Meetings');
      $meetings = $query->find();
      $meetings->select('timezone');
      $meetings->where(['meeting_id' => $meeting_id]);
    /// $meetings->count();
      foreach ($meetings as $row){

       $meeting=$row->timezone;
      }
     return $meeting;
  }
	function byte_convert($size) {
	  # size smaller then 1kb
	  	if ($size < 1024) return $size . ' Byte';
	  # size smaller then 1mb
	 	 if ($size < 1048576) return sprintf("%4.2f KB", $size/1024);
	  # size smaller then 1gb
	  	if ($size < 1073741824) return sprintf("%4.2f MB", $size/1048576);
	  # size smaller then 1tb
	  	if ($size < 1099511627776) return sprintf("%4.2f GB", $size/1073741824);
	  # size larger then 1tb
	  	else return sprintf("%4.2f TB", $size/1073741824);
	}

	function array_sort_by_column(&$array, $column, $direction)
	{
	    $reference_array = array();

	    foreach($array as $key => $row) {
	        $reference_array[$key] = $row[$column];
	    }

	    array_multisort($reference_array, $direction, $array);
		   
	}
	// ============================================= 
	/**
	* class   : 
	* menthod : 
	* @param  : 
	* @output : 
	* @Description : for print 
	**/
	// ==============================================    
	function pdump() {
	    list($callee) = debug_backtrace();
	    $arguments = $callee['args'];
	    $total_arguments = count($arguments);

	    echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">';
	    echo '<legend style="background:lightgrey; padding:5px;">' . $callee['file'] . ' @ line: ' . $callee['line'] . '</legend><pre>';

	    $i = 0;
	    foreach ($arguments as $argument) {
	        echo '<br/><strong>Debug #' . (++$i) . ' of ' . $total_arguments . '</strong>: ';
	        print_r($argument);
	    }

	    echo "</pre>";
	    echo "</fieldset>";
	}

	function time_diff($date1,$date2)
	{
		$diff = abs(strtotime($date2) - strtotime($date1));

		$years = floor($diff / (365*60*60*24)); $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24)); $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));

		return $minuts = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60)+($hours * 60);

		$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minuts*60));
	}
  // ============================================= 
  /**
  * menthod : CheckScheduleOfMeeting
  * @Description : check schedule for daily,weekly,monthly and not repeated
  **/
  // ============================================= 
  function CheckScheduleOfMeeting($meeting_id)
  {
  	$meetingTable = TableRegistry::get('meetings');
      $meeting = $meetingTable->find()
      ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
      ->first();
       $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
        $today = date('Y-m-d H:i');
        $today1 = date('Y-m-d');
	   if($meeting->repeatTime == '')
        {  
			$finaltime=$meeting_date.' '.$meeting->meeting_time;
			$finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
			$duaration=($meeting->meeting_duration) +30;
			$enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;

		}
        //if Meeting Repeated weekly
        if($meeting->repeatTime == 'weekly')
        {              
       		$day=date('D');
           	$day1=$meeting->repeatDay;
            if($meeting_date < $today1)
            {
               $day1=$meeting->repeatDay;
               if($day1 == $day)
               {
                $meeting_date= date('Y-m-d');
               } 
               else
               {
               $meeting_date=date('Y-m-d',strtotime('next '.$day1 ));
                }
            }
            else
            {
               $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
            }
            $finaltime=$meeting_date.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
      

        }
        // if Meeting Repeated daily
         if($meeting->repeatTime == 'daily')
        {  	       
	        if($meeting_date > $today1 )
	        {
	           $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
	        }   
	        else
	        {
	            $meeting_date= date('Y-m-d'); 
	        }
          $finaltime=$meeting_date.' '.$meeting->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          $duaration=($meeting->meeting_duration) +30;
          $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        }

        // if Meeting Repeated monthly
        if($meeting->repeatTime == 'monthly')
        {  
         	$month = date('F');
            $year = date('Y');
            $meeting->meeting_time;
            $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
            
            $meeting->monthrepeat;
            $day=date('F Y');
            $monthrepeat=explode(' ',$meeting->monthrepeat);
          if($monthrepeat['1'] != '')
          {
            if($meeting_date < $today1)
	          {
	              //if month do not fifth monday or tuesday
                  
                  $thismonth = date('F',strtotime('first day of +1 month'));
                  $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +1 month'));
                  $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +1 month'));
                    if($thismonth != $nextmonth)
                    { 

                      $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +2 month'));
                      $thismonth = date('F',strtotime('first day of +2 month'));
                      $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +2 month'));

                        if($thismonth != $nextmonth)
                        {
                           $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +3 month'));
                          $thismonth = date('F',strtotime('first day of +3 month'));
                          $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +3 month'));
                        }
                    }
                        
	          }
	           else
	          {
	            $row_date= date('Y-m-d',strtotime($meeting->meeting_date));
	          }
            $finaltime=$row_date.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
          }
          else
          {
          	 if($meeting_date < $today1)
            {
              $monthrepeat=explode('th',$meeting->monthrepeat);
              $day=date('Y-m-'.$monthrepeat[0],strtotime('+1 month '));
            }
            else
            {
              $row->monthrepeat. ' of '. $day;
              $day= date('Y-m-d',strtotime($meeting->meeting_date));
            }    
        
            $monthrepeat=explode('th',$meeting->monthrepeat);
            $day=date('Y-m-'.$monthrepeat[0]);
            $finaltime=$day.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
          }
        }
	  
       return  array ('finaldate'=>$finaldate, 'enddate'=>$enddate);
     }   

    // Function to remove folders and files 
    function rrmdir($dir) {
        if (is_dir($dir)) {
            $files = scandir($dir);
            foreach ($files as $file)
                if ($file != "." && $file != "..") rrmdir("$dir/$file");
            rmdir($dir);
        }
        else if (file_exists($dir)) unlink($dir);
    }
     
?>