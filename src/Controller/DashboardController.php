<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use BigBlueButton\BigBlueButton as BigBlueButton;
use BigBlueButton\Core\ApiMethod as ApiMethod;

use BigBlueButton\Parameters\CreateMeetingParameters as CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters as JoinMeetingParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters as IsMeetingRunningParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use BigBlueButton\Responses\GetDefaultConfigXMLResponse;

use BigBlueButton\Parameters\GetRecordingsParameters as GetRecordingsParameters;
use DateTimeZone;
use DateTime;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class DashboardController extends AppController
{
   
    var $paginate = array(
        'limit' => '5',
        'order' => array(
            'Meetings.created_at' => 'asc'
        )
    );

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $query = TableRegistry::get('Meetings');
        $meetings = $query->find();
        $meetings->where(['created_by' => $this->Auth->user('id'),'deleted' => '0'])->order(['created_at' => 'DESC']);
        $count = $query->find()
        ->where(['created_by' => $this->Auth->user('id'),'deleted' => '0'])->count();

        $bbb = new BigBlueButton;
        $data=array();
       foreach ($meetings as $row){
            $meeting=$row->meeting_id;
            $password=$row->password;
            $meeting_id=json_decode(json_encode($meeting));
            $Recording= new GetRecordingsParameters($meeting_id,$password);
            $Recordingdata= $bbb->getRecordingsWithXmlResponseArray($Recording);

            $arr=xml2array($Recordingdata);

            $vals= array_values($arr);
            if(!empty((@$vals[3])))
            {
              array_push($data,@$vals[3]);
            }
        }
     //echo   $meetings->recording=$data;

        $this->set(compact('meetings', $this->paginate($meetings)));
        $this->set(compact('data', $data));
        $this->set(compact('count', $count));
        $this->set('_serialize', ['dashboard']);
    }


     public function GetRecording()
    {
          $query = TableRegistry::get('Meetings');
          $meetings = $query->find();
          $meetings->where(['created_by' => $this->Auth->user('id'),'deleted' => '0'])->order(['meeting_time' => 'DESC']);

          $bbb = new BigBlueButton;
          $dataxml=array();

            foreach ($meetings as $row){
            $meeting=$row->name;
            $password=$row->password;
            $meeting_id=json_decode(json_encode($meeting));
            $Recording= new GetRecordingsParameters($meeting_id);
            $Recordingdata= $bbb->getRecordingsWithXmlResponseArray($Recording);

            $arr=$Recordingdata;

         
            if(empty(($arr['messageKey'])))
            {
             array_push($dataxml,$arr);
            }
          }
            $json = json_encode($dataxml);
            $data = json_decode($json,TRUE);
          
        $this->set(compact('meetings', $this->paginate($meetings)));
        $this->set(compact('data', $data));
      //  echo "<pre>";print_r($data); exit;
        $this->set('_serialize', ['GetRecording']);


    }
     public function GetRecordingcopy()
    {
          $query = TableRegistry::get('Meetings');
          $meetings = $query->find();
          $meetings->where(['created_by' => $this->Auth->user('id'),'deleted' => '0'])->order(['meeting_time' => 'DESC']);

          $bbb = new BigBlueButton;
          $dataxml=array();

            foreach ($meetings as $row){
            $meeting=$row->name;
            $password=$row->password;
            $meeting_id=json_decode(json_encode($meeting));
            $Recording= new GetRecordingsParameters($meeting_id);
            $Recordingdata= $bbb->getRecordingsWithXmlResponseArray($Recording);

            $arr=$Recordingdata;

         
            if(empty(($arr['messageKey'])))
            {
             array_push($dataxml,$arr);
            }
          }
            $json = json_encode($dataxml);
            $data = json_decode($json,TRUE);
          
        $this->set(compact('meetings', $this->paginate($meetings)));
        $this->set(compact('data', $data));
      // echo "<pre>";print_r($data); exit;
        $this->set('_serialize', ['GetRecording']);


    }
        public function DeleteRecording($record_id)
        {
              $bbb = new BigBlueButton;

              $Recording= new DeleteRecordingsParameters($record_id,'123456');
              $Recordingdata= $bbb->deleteRecordingsWithXmlResponseArray($Recording);
                        
              return $this->redirect(['controller'=>'Dashboard','action' => 'GetRecording']);

        }
    public function DownloadRecording($meeting_id)
    {
       

        if (file_exists("/home/olympic/www/webroot/files/recordings/".$meeting_id."/download.mp4")) {
          $file_name="/home/olympic/www/webroot/files/recordings/".$meeting_id."/download.mp4";
            $this->response->file("/home/olympic/www/webroot/files/recordings/".$meeting_id."/download.mp4" ,
            array('download'=> true, 'name'=> $meeting_id.'mp4'));
            
            return $this->response;
        }else{
            $this->Flash->error(__('Recordnig file not found'));
            return $this->redirect(['controller'=>'Dashboard','action' => 'GetRecording']);
        }
    }

        public function delete($id = null)
      {
          $MeetingsTable = TableRegistry::get('Meetings');
          $meeting = $MeetingsTable->get($id);
           if(($meeting->presentation_file)!= '')
            {
             $file=WWW_ROOT.'files/Meetings/presentation_file/'.$meeting->presentation_file;
             unlink($file);
            }
          if ($MeetingsTable->delete($meeting)) {
              $this->Flash->success(__('The meeting has been deleted.'));
          } else {
              $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
          }
          return $this->redirect(['action' => 'index']);
      }


}
