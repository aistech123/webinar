  <?php  $controller=$this->request->params['controller'];
         $action=$this->request->params['action'];?>
  <section>  
    <div class="container">
      <div class="page_title">
        <div class="text-center">
          <h1><span>Default Settings</span></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="main_content">
  	<div class="container">
      	<div class="box_design">
          	<section class="row">
              	<div class="meeting_schedule_btn">
                    <div class="">
                    	<div class="center_menu_inner">
                      <ul>
                        <li class="
                         <?php if(($controller == 'Setting') && ($action == 'index'))  { ?>active <?php } ?>
                         hvr-grow-shadow"><a class="btn btn-primary" href="/Setting/index">Invitations</a></li>
                        <li class="
                         <?php if(($controller == 'Setting') && ($action == 'settingRegister'))  { ?>active <?php } ?>
                        hvr-grow-shadow"><a class="btn btn-primary" href="/Setting/setting_register">Registration</a></li>
                        <li class="
                         <?php if(($controller == 'Setting') && ($action == 'settingNotes'))  { ?>active <?php } ?>
                        hvr-grow-shadow"><a class="btn btn-primary" href="/Setting/setting_notes">Notes</a></li>
                        <li class="
                         <?php if(($controller == 'Setting') && ($action == 'settingLayout'))  { ?>active <?php } ?>
                        hvr-grow-shadow"><a class="btn btn-primary" href="/Setting/setting_layout">Layout</a></li>
                      </ul>
                      </div>
                    </div>
                   </div>
              </section>