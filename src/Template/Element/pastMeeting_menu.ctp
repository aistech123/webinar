<section class="inner_menu">
  <div class="container">
  	<div class="center_menu">
        <ul>
           <li class="active" id="pastlink"><a href="/meetings/schedule_meeting/past" class="btn btn-primary">Past Meetings</a></li>
              <li id="upcominglink"><a href="/meetings/schedule_meeting/upcoming" class="btn btn-primary" >Upcomming Meetings</a></li>
        </ul>
    </div>
  </div>
</section>
<section>  
<div class="container">
    <div class="page_title">
      <div class="text-center">
      <?php foreach ($meeting as $meeting) {
       
      } ?>
        <h1><span><?= $meeting->meeting_name; ?></span></h1>
      </div>
    </div>
  </div>
</section>
<section class="main_content">
  <div class="container">
      <div class="box_design">
                    <!--<div class="center_menu_inner">-->
                        <div class="tabbable">
                            <div class="row">
								<div class="meeting_schedule_btn">
                                <ul class="nav nav-tabs">
                                  <li class="active hvr-grow-shadow"><a class="btn btn-primary" href="#overview" data-toggle="tab">Overview</a></li>
                                 <!--  <li class="hvr-grow-shadow"><a class="btn btn-primary" href="#chat_history" data-toggle="tab">Chat History</a></li> -->
                                  <li class="hvr-grow-shadow"><a class="btn btn-primary" href="#recordings" data-toggle="tab">Recording</a></li>
                                  <li class="hvr-grow-shadow"><a class="btn btn-primary" href="#attendees" data-toggle="tab">Attendees</a></li>
                                </ul>
								</div>
            