  <?php  $controller=$this->request->params['controller'];
         $action=$this->request->params['action'];?>
  <section class="inner_menu">
    <div class="container">
      <div class="center_menu">
          <ul>
            <li 
            <?php if(($controller == 'Dashboard') && ($action == 'index') || ($controller == 'Meetings') && ($action == 'newmeetingroomschedule' ||$action == 'emailInvitation' ||$action == 'addImageUpload' ||$action == 'editBasic'
             ||$action == 'meetingLayout' ||$action == 'meetingPolls'||$action == 'emailMeetingNote'))  { ?>class="active" <?php } ?>>
            <a class="btn btn-primary" href="/dashboard"><img src="/img/webinar-room-icon.png" width="20" /> MY MEETING ROOMS</a></li>
            <li
             <?php if(($controller == 'Meetings') && ($action == 'add'))  { ?>class="active" <?php } ?>>
             <a href="/meetings/add" class="btn btn-primary"><img src="/img/webinar-add-new.png" width="20" /> New Room</a></li>
          <!--   <li
             <?php if(($controller == 'Meetings') && ($action == 'contentLibrary'))  { ?>class="active" <?php } ?>
            ><a href="/meetings/Content_library"  class="btn btn-primary"><img src="/img/webinar-files.png" width="20" /> content library</a></li> -->
            <li
            <?php if(($controller == 'Dashboard') && ($action == 'getRecording'))  { ?>class="active" <?php } ?>index
            ><a href="/dashboard/GetRecording" class="btn btn-primary"><img src="/img/webinar-recordings.png" width="20" /> Recordings</a></li>
           <!--  <li
             <?php if(($controller == 'Setting') && ($action == 'index')||($action == 'settingRegister')||($action == 'settingLayout')||($action == 'settingNotes'))  { ?>class="active" <?php } ?>
            ><a href="/Setting/setting_register" class="btn btn-primary">Default settings</a></li> -->
          </ul>
      </div>
    </div>
  </section>