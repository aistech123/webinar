  <?php


  $pollkey = isset($pollkey) ? $pollkey : '<%= pollkey %>';

  $anskey = isset($anskey) ? $anskey : '<%= anskey %>';
  ?>
    <div class="deleterow<?php echo $pollkey  ?>"  >     
    <h4 class="m-t-b-5 text-center" id="poll">-- Poll <?php echo $pollkey  ?> --</h4>
        
                <div class="form-group">
                      <label>poll topic</label>
                      <div class="row">
                        <div class="col-sm-10">
                         <?php echo $this->Form->input("polls.{$pollkey}.description",['label'=>false,'data-bvalidator'=>'required','class'=>'form-control','type'=>'textarea']); ?>
                            <input type="hidden" name="polls[<?php echo $pollkey?>][meeting_id]" value="<?php echo $meeting->id; ?>">
                             <input type="hidden" name="polls[<?php echo $pollkey?>][pollid]" value="<?php echo $pollkey  ?>">
                          </div>
                          
                          <div class="col-sm-2">
                            <label class="bottom_text"></label>
                          </div>
                      </div>                    
                  </div>
                 <div id="ansrow<?php echo $pollkey ?>">
                <input type='button' class="btn btn-primary" id="addans" name="<?php echo $pollkey ?>" value="Add Another Answer" />
                           
                 </div>
          
      <?php //} } ?>
     </div>
    