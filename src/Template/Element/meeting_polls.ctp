
 <?php echo $this->element('inner_menu'); ?>
  <?php echo $this->element('center_menu'); ?>
<section class="main_content">
<div class="page_title">
        <div class="text-center">
          <h1><span>Coming Soon</span></h1>
        </div>
		</div>
      </div>
</section>
</div>
<!-- 
  <?php echo $this->Html->script(array(
      '//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js',
      '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js'
  ));?>
  <script type="text/javascript">
  function pollans(poll_id,meeting_id)
  {
      //this is the sample function code executed on "unload"
     
       var data = {poll_id: poll_id,meeting_id:meeting_id};
         $.ajax({
               type: "post",  // Request method: post, get
               url: "/meetings/findans/", // URL to request
               data: data,  // post data
                
               success: function(response) {
              
                           
                                    document.getElementById("ansrow"+poll_id).innerHTML= response;
                             },
                             error:function (XMLHttpRequest, textStatus, errorThrown) {
                                    alert(textStatus);
                             }
            });
            return false;
  }
  </script>
  
     <?php foreach($polldata as $data){ ?>
  <div class="deleterow<?php echo $data->poll_id  ?>"  >     
    <h4 class="m-t-b-5 text-center" id="poll">-- Poll <?php echo  $data->poll_id   ?> --</h4>
      
                <div class="form-group" >
                      <label>poll topic</label>
                      <div class="row">
                        <div class="col-sm-10">
                         <?php echo $this->Form->input("polls.{$data->poll_id }.description",['label'=>false,'data-bvalidator'=>'required','class'=>'form-control','type'=>'textarea','value'=> $data->description]); ?>
                            <input type="hidden" name="polls[<?php echo  $data->poll_id ?>][meeting_id]" value="<?php echo $meeting->id; ?>">
                             <input type="hidden" name="polls[<?php echo  $data->poll_id ?>][pollid]" value="<?php echo  $data->poll_id   ?>">
                          </div>


                          <div class="col-sm-2">
                            <label class="bottom_text"></label>
                          </div>
                      </div>   
                       </div>
   <input type='button' class="btn btn-primary" id="addans" name="<?php echo  $data->poll_id ?>" value="Add Another Answer" />
                           
         <div id="ansrow<?php echo  $data->poll_id ?>">
               
              

               </div>     
                <?php $anskey=0;
                      echo "<script>pollans($data->poll_id,$data->meeting_id);</script>";
                                           ?>    
      <?php  } ?>
     </div>

  <?php

  $numpolls = $polldata->count();
  $pollkey = isset($pollkey) ? $pollkey : '<%= pollkey %>';

  $anskey = isset($anskey) ? $anskey : '<%= anskey %>';
  ?>
          <?= $this->Form->create($meeting,array('id'=>'meeting')) ?>
           <?php echo $this->Form->hidden("numpolls",['value'=>$numpolls,'name' =>'numpolls','id'=>'numpolls']) ?>
          <div id="poll-form" class="poll">
          <?php /* if ($numpolls != '0') {
               for($pollkey=0; $pollkey < $numpolls; $pollkey++)
                {
                          */?>
        <script id="polls-template" type="text/x-underscore-template">
        <?php echo $this->element('polls'); ?>
        </script>
  <script id="ans-template" type="text/x-underscore-template">
                <?php echo $this->element('ans'); ?>
                </script>
               
   <div id="newpoll" >
     <div id="newans" >
        </div>
     </div>
    </div>
         
  <div class="row m-t-b-15">
            <div class="col-sm-12 text-center">
               <input type='button' class="btn btn-primary btn-lg" id="addpoll" name="addpoll" value="Add Another Poll" />
                 <input type='button' class="btn btn-danger" id="removepoll" name="removepoll" value="Remove Poll" />
            </div>        
          </div>     
                          
                  <hr class="m-t-b-5">
            
                  <div class="row">
                      <div class="col-sm-4">
                          <button class="btn btn-primary btn-block">Save Progress</button>
                      </div>
                     
                      <div class="col-sm-4 col-xs-offset-2">
                          <button class="btn btn-success btn-block">Start Now</button>
                      </div>
                  </div>
              </form>
          </div>
          
          
      </div>
  </section>

   
  </script>
  <script>
  $.noConflict();
   $('#meeting').bValidator();

  $(document).ready(function() {
      var
        pollFrom = $('#poll-form'),
        pollBody = pollFrom.find('#newpoll'),
        ansBody = pollFrom.find('#newans'),
        pollTemplate = _.template($('#polls-template').remove().text()),
         meetingTemplate = _.template($('#meeting_poll-template').remove().text()),
        ansTemplate = _.template($('#ans-template').remove().text()),
        numberRows = $('#numpolls').val();
        numberofans = $('#numpolls').val();

         $(pollTemplate({pollkey: numberRows++,anskey: numberofans}))
                  .hide()
                  .appendTo(pollBody)
                  .appendTo(ansBody)
                  .fadeIn('fast');
   
                         
                         /*   $(ansTemplate({anskey: numberofans++,pollkey: numberRows}))
                                .hide()
                                .appendTo(ansBody)

                                .fadeIn('fast');*/
                       

                   $("#addpoll").click(function (e) {
                      
                            e.preventDefault();
                            
                            $(pollTemplate({pollkey: numberRows++,anskey: numberofans}))
                                .hide()
                                .appendTo(pollBody)
                                .appendTo(ansBody)
                                .fadeIn('fast');
                               
                        });
                     $("#removepoll").on("click", function(e) {
                  
                       numberRows--;
                         $(".deleterow"+(numberRows)).remove();
                                  e.preventDefault();
                             
                          $(this)
                              .closest('.deleterow')
                              .closest('fast', function() {
                                  $(this).remove();
                              });
                      });

                      if (numberRows === 0) {
                        
                         pollFrom.find('#addpoll').click();
                          
                          
                      }
                      $(document).on('click',"#addans",function(e) {
                          var name = $(this).attr("name");
                          
                            ansBody1 = pollFrom.find('#ansrow'+name);

                                      e.preventDefault();
                                     
                                                $(ansTemplate({anskey: numberofans++,pollkey: numberRows-1}))
                                                    .hide()
                                                    .appendTo(ansBody1)

                                                    .fadeIn('fast');  

                                                      
                      });
                      $(document).on('click',"#removeans",function(e) {
                       var name = $(this).attr("name");
                     
                       numberofans--;
                         $("#ansdiv"+(name)).remove();
                                  e.preventDefault();
                             
                          $(this)
                             
                              .closest('fast', function() {
                                  $(this).remove();
                              });
                      });

  });


  </script>

   <!--<div id="POLLGroup">
              <h4 class="m-t-b-5 text-center">-- Poll 1 --</h4>
            <?= $this->Form->create($meeting) ?>
                  <?php foreach($polldata as $data){
                      print_r($data->title);
                      } ?>
              	<div class="form-group">
                      <label>poll topic</label>
                      <div class="row">
                      	<div class="col-sm-10">
                          	<textarea class="form-control" type="text" name="title[]"></textarea>
                          </div>
                          <div class="col-sm-2">
                          	<label class="bottom_text"></label>
                          </div>
                      </div>                    
                  </div>
                  <div id="TextBoxesGroup">
                    <?php //foreach($polldata as $data){  ?>
                  <div class="form-group" >
                      <label>Answer 1:</label>
                      <div class="row">
                      	<div class="col-sm-10">
                              <input class="form-control" type="text" name="title[]">  
                          </div>
                          <div class="col-sm-2">
                          	<label class="bottom_text"></label>
                          </div>
                      </div>                    
                  </div>
                <?php //} ?>
                  </div>
                  <input type='button' class="btn btn-primary" value='Add Another Answer' id='addButton'>
                 <input type='button' class="btn btn-danger" value='Remove Answer' id='removeButton'>
  				
  				<div class="row m-t-b-5">
  					<div class="col-sm-12">
  						<button class="btn btn-primary">Save</button>
  						<button class="btn btn-danger">Delete</button>
  					</div>
  				</div>
  				 </div>
  				<div class="row m-t-b-15">
  					<div class="col-sm-12 text-center">
  						 <input type='button' class="btn btn-primary btn-lg" id="addpoll" value="Add Another Poll" />
                <input type='button' class="btn btn-danger" id="removepoll" value="remove Poll" />
  					</div>
  				</div>
                  
                  
                   
                  <hr class="m-t-b-5">
                	
                
                  <div class="row">
                      <div class="col-sm-4">
                        <?= $this->Form->button('Save Process',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                      </div>
                     
                      
                  </div>
           
          </div>
            <?= $this->Form->end() ?>
          
      </div>
     
  </section>



  <script type="text/javascript">

  $(document).ready(function(){

      var counter = 2;
      var pollcounter=2;      

      $("#addButton").click(function () {
                  
      if(counter>10){
             // alert("Only 10 textboxes allow");
             // return false;
      }   
          
      var newTextBoxDiv = $(document.createElement('div'))
           .attr("id", 'TextBoxDiv' + counter);
                  
      newTextBoxDiv.after().html('<div class="form-group" > <div class="row"><div class="col-sm-10"><label>Answer '+ counter + ':</label>' +
            '<input type="text" class="form-control" name="title[]" id="textbox' + counter + '" value="" ></div><div class="col-sm-1"><label class="bottom_text"></label></div></div></div>');
              
      newTextBoxDiv.appendTo("#TextBoxesGroup");

                  
      counter++;
       });

       $("#removeButton").click(function () {
      if(counter==1){
            alert("No more textbox to remove");
            return false;
         }   
          
      counter--;
              
          $("#TextBoxDiv" + counter).remove();
              
       });
          
        
      $("#addpoll").click(function () {
             
      if(pollcounter>10){
             // alert("Only 10 textboxes allow");
             // return false;
      }   
          
      var newTextBoxpollDiv = $(document.createElement('div'))
           .attr("id", 'newTextBoxpollDiv' + pollcounter);
                  
      newTextBoxpollDiv.after().html('<h4 class="m-t-b-5 text-center">-- Poll '+pollcounter + ' --</h4><div class="form-group"><label>poll topic</label><div class="row"><div class="col-sm-10"><textarea name="title[]" type="text" class="form-control"></textarea></div><div class="col-sm-2"><label class="bottom_text"></label></div></div></div>  <div id="TextBoxesGroup'+pollcounter + '"><div class="form-group" ><label>Answer 1:</label><div class="row"><div class="col-sm-10"><input class="form-control" type="text" name="title[]"></div><div class="col-sm-2"><label class="bottom_text"></label></div></div></div></div></div><input type="button" class="btn btn-primary" value="Add Another Answer" id="addButton'+pollcounter + '"/><input type="button" class="btn btn-danger" value="Remove Answer" id="removeButton'+pollcounter + '"/>');
              
      newTextBoxpollDiv.appendTo("#POLLGroup");

                  
      pollcounter++;
       });

      $("#removepoll").click(function () {
      if(pollcounter==1){
            alert("No more textbox to remove");
            return false;
         }   
         
      pollcounter--;
           
          $("#newTextBoxpollDiv" + pollcounter).remove();
             
       });
      var newcounter=2;
      $("body").on("click", "#addButton"+pollcounter, function(){
          
          var newTextBoxDiv = $(document.createElement('div'))
           .attr("id", 'TextBoxDiv' + newcounter);
         
          newTextBoxDiv.after().html('<div class="form-group" > <div class="row"><div class="col-sm-10"><label>Answer '+ newcounter + ':</label>' +
                '<input type="text" class="form-control" name="title[]" id="textbox' + newcounter + '" value="" ></div><div class="col-sm-1"><label class="bottom_text"></label></div></div></div>');
                  
         newTextBoxDiv.appendTo("#TextBoxesGroup"+pollcounter);
                 
          newcounter++; 
      });
    });
  </script> --> 