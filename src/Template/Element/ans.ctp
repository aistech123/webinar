   
  <?php 

  $anskey = isset($anskey) ? $anskey : '<%= anskey %>';
  $pollkey = isset($pollkey) ? $pollkey : '<%= pollkey %>';


  ?>
        
                  <div class="form-group" id="ansdiv<?php echo $pollkey.$anskey ;?>">
                      <label>Answer </label>
                      <div class="row">
                        <div class="col-sm-10">
                              <?php echo $this->Form->input("polls.{$anskey}.answer",['label'=>false,'class'=>'form-control','data-bvalidator'=>'required','type'=>'text']); ?><input type="hidden" name="polls[<?php echo $anskey?>][meeting_id]" value="<?php echo $meeting->id; ?>">
                             <input type="hidden" name="polls[<?php echo $anskey?>][pollid]" value="<?php echo $pollkey  ?>">
                          </div>
                          <div class="col-sm-2">
                            <label class="bottom_text"></label>
                             <input type='button' class="btn btn-danger" id="removeans" name="<?php echo $pollkey.$anskey ;?>" value="Remove Answer" />
                          </div>
                          
                      </div>                    
                  </div>
                