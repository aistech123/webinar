  <?php  $controller=$this->request->params['controller'];
         $action=$this->request->params['action'];?>
  <section>  
    <div class="container">
      <div class="page_title">
        <div class="text-center">
          <h1><span><?php echo $meeting->headerText; ?></span></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="main_content">
      <div class="container">
          <div class="box_design">
              <section class="row">
              <div class="meeting_schedule_btn">
                    <div class="">
                      <div class="center_menu_inner">
                      <ul>
                        <li class="
                         <?php if(($controller == 'Meetings') && ($action == 'editBasic'))  { ?>active <?php } ?>
                        hvr-grow-shadow"><a class="btn btn-primary" href="../editBasic/<?php echo $meeting->id; ?>">Basics</a></li>
                        <li class="
                   <?php if(($controller == 'Meetings') && ($action == 'addImageUpload'))  { ?>active <?php } ?>
                  hvr-grow-shadow"><a class="btn btn-primary" href="../add-image-upload/<?php echo $meeting->id; ?>">Registration</a></li>

                        <li class="
                         <?php if(($controller == 'Meetings') && ($action == 'newmeetingroomschedule'))  { ?>active <?php } ?>
                        hvr-grow-shadow"><a class="btn btn-primary" href="../newmeetingroomschedule/<?php echo $meeting->id; ?>">Schedule</a></li>

                        <li class="
                   <?php if(($controller == 'Meetings') && ($action == 'meetingLayout'))  { ?>active <?php } ?>
                  hvr-grow-shadow"><a class="btn btn-primary" href="../meetingLayout/<?php echo $meeting->id; ?>">Layout</a></li>

                  <li
             <?php if(($controller == 'Meetings') && ($action == 'contentLibrary'))  { ?>class="active" <?php } ?>
            ><a href="/meetings/Content_library/<?php echo $meeting->id; ?>"  class="btn btn-primary"><img src="/img/webinar-files.png" width="20" /> content library</a></li> 
              <!--     <li class="
                  <?php if(($controller == 'Meetings') && ($action == 'emailInvitation'))  { ?>active <?php } ?>
                  hvr-grow-shadow"><a class="btn btn-primary" href="../emailInvitation/<?php echo $meeting->id; ?>">Invitations</a></li>
                  

                  <li class="
                  <?php if(($controller == 'Meetings') && ($action == 'emailMeetingNote'))  { ?>active <?php } ?>
                  hvr-grow-shadow"><a class="btn btn-primary" href="../emailMeetingNote/<?php echo $meeting->id; ?>">Notes</a></li>
                 
                  <li class="
                   <?php if(($controller == 'Meetings') && ($action == 'meetingPolls'))  { ?>active <?php } ?>
                  hvr-grow-shadow"><a class="btn btn-primary" href="../meetingPolls/<?php echo $meeting->id; ?>">Polls</a></li> -->
                      </ul>
                      </div>
                    </div>
                   </div>
              </section>