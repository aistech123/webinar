    <?php 
    use Cake\Routing\Router;
    echo $this->element('inner_menu');
    ?>
 <section>
    <section>
        <div class="container">        
            <div class="page_title">
                <div class="text-center">
                    <h1><span><img src="/img/webinar-recordings.png" style="margin-top:-10px;"> Recordings</span>
                    </h1>
                     <h4 class="m-t-20">Download links of recorded Meetings are available approximately 3 hours after the meeting has closed. Recordings are available for up to 2 days after the Meeting</h4>
                </div>

            </div>  
        </div>
    </section>

    <section class="main_content">
        <div class="container">

          <?php 
            $sortArray=array();

           foreach ($data as $key => $value) 
            {
             foreach ($value as $key => $row) {

                if(@$row['recordId']['0'])
                {
                $startTime=date('Y-m-d H:s:i',($row['startTime']['0'])/1000);
                $sortArray[] = array('meetingId'=>$row['meetingId']['0'],'recordId' => $row['recordId']['0'] ,'startTime'=>$startTime,'playbackFormatLength'=>$row['playbackFormatLength']['0'],'playbackFormatUrl'=>$row['playbackFormatUrl']['0']);
                }
               }
              }  
            array_sort_by_column($sortArray,'startTime',SORT_DESC);

            $i=0;
            $count=count($sortArray);
            foreach ($sortArray as $row) { 
             
            ?>
         
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="text-center"><?php echo  MeetingName($row['meetingId']); ?></h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-3">
                    <a href="<?php echo $row['playbackFormatUrl']; ?>" target="_blank">
                        <div class="btn btn-primary btn-lg btn-block play_btn"><h1><i class="fa fa-play-circle-o"></i> Play </h1></div>
                        </a>
                    </div>
                    <div class="col-sm-3">
                        <div class="list_btn">
                           <!--  <div class="viewers_btn hvr-reveal">
                            <?php 
                            /* if (file_exists("/home/olympic/www/webroot/files/recordings/".$row['recordId']."/download.mp4")) {
                            $url='http://'.$_SERVER['HTTP_HOST'].'/dashboard/download-recording/'.$row['recordId'].'/'.$row['meetingId']; 
                            }else {
                                $url ="Recording Not Found";}?>
                             <?= $this->Html->link('<i class="fa fa-code"></i><span>Download Link</span>',
                         array(
                               
                                 'action' => 'DownloadRecording', $row['recordId'],
                                    ),
                                array(
                                    'escape' => false,
                                    'confirm' => __($url),
                                )
                            );*/ ?>
                               
                           <!-- </div> -->

                      <div class="viewers_btn hvr-reveal" id="download<?= $i ?>">
                            <a href="javascript:void(0);"><i class="fa fa-code"></i><span>Download Link</span></a>
                        </div>
                        <?php 
                             if (file_exists("/home/olympic/www/webroot/files/recordings/".$row['recordId']."/download.mp4")) {
                                $url='http://'.$_SERVER['HTTP_HOST'].'/dashboard/download-recording/'.$row['recordId'].'/'.$row['meetingId']; 
                                }else {
                                    $url ="Recording Not Found";}?>
                              <script>
                              count='<?php echo $i ?>';
                            
                                  $( function() {
                                  
                                     
                                      $( "#dialog-confirm"+'<?php echo $i ?>' ).dialog({
                                        autoOpen: false,
                                        width:'360px',
                                        buttons: {
                                          "Download": function() {
                                              
                                            $( this ).dialog( "close" );
                                              window.open('<?php echo $url ?>','_top');
                                           },
                                          Cancel: function() {
                                            $( this ).dialog( "close" );
                                          }
                                        }
                                      });
                                      $( "#download"+'<?php echo $i ?>' ).click(function() {
                                       $( "#dialog-confirm"+'<?php echo $i ?>' ).dialog( "open" );
                                    });
                                   
                                 });
   
                           </script>
                            <div id="dialog-confirm<?= $i ?>" title="Download Link" >
                              <a href="<?php echo $url; ?>"><p><?php echo $url; ?></p></a>
                            </div>
                            <div class="delete_btn hvr-reveal">
                           <?= $this->Html->link('<i class="fa fa-remove"></i><span>Delete</span>',
                            array(
                               
                             'action' => 'DeleteRecording', $row['recordId'],
                                ),
                            array(
                                'escape' => false,
                                'confirm' => __('Are you sure you want to delete # {0}?', $row['meetingId']),
                             )
                            ); ?>
                            
                            </div>                        
                        </div>
                    </div>                
                    <div class="col-sm-3">
                        <div class="text-center m-t-10">
                            <label>Starting Time</label>
                            <p><?php echo date('l, F dS Y @ h:i a T',strtotime($row['startTime'])); ?></p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="text-center">
                            <label>Duration</label>
                            <p><?php echo $row['playbackFormatLength']; ?> minutes</p>
                            
                        </div>
                    </div>         
                </div>
            </div>

           <?php $i++; } 
           ?> 
         </div>
    </section>
   
   <?= $this->Html->css('jquery-ui.css') ?>

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
