  
<!DOCTYPE html>
<html>
<body>




  <?php 
      echo $this->element('inner_menu');
      error_reporting(0);
   ?>
<section>
  <div class="container">
    <div class="page_title">
      <div class="text-center">
        <h1><span><img style="margin-top:-10px;" src="/img/webinar-room-icon.png" />Meeting Rooms</span></h1>
      </div>
    </div>
  </div>
</section>
<section class="main_content">
  <div class="container">
        
        <?php
          if($count > 0)
          {
            $i=0;
            foreach ($meetings as $row):
            @date_default_timezone_set($row->timezone);
            $meeting_date=date('Y-m-d',strtotime($row->meeting_date));
            $today1 = date('Y-m-d');
           //meeting is not Repeated 
           if($row->repeatTime == '')
           {  
              @$finaltime=$meeting_date.' '.$row->meeting_time;
              @$today = date('Y-m-d H:i');

              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
                if($enddate > $today)
                {
                  $final="<h4 class='text-center'>Next Scheduled Meeting: ";
                  $final.=date('l, F dS Y @ h:i:s a T ',strtotime(($finaltime)));
                  $final.="</h4>";

                }
                if(($today >= $finaldate && $today <= $enddate))
                {
                  $final="<h4 class='text-center'>";
                  $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaltime)));
                  $final.="</h4>";

                }
                if($enddate < $today)
                {
                  $final="<h4 class='text-center'>No Future Meetings have been Scheduled";
                  $final.="</h4>";
                }
            }
       

            //meeting is Repeated Monthly 
           if($row->repeatTime == 'monthly')
           {                   
              $today = date('Y-m-d H:i');
              $month = date('F');
              $year = date('Y');
              $row->meeting_time;
              $meeting_date=date('Y-m-d',strtotime($row->meeting_date));
              $row->monthrepeat;
              $day=date('F Y');
              $monthrepeat=explode(' ',$row->monthrepeat);
              //meeting is repeated week day of each month like fifth sat
            if($monthrepeat['1'] != '')
            {

              if($meeting_date < $today1)
              {
                //if month do not fifth monday or tuesday
              
              $thismonth = date('F',strtotime('first day of +1 month'));
              $nextmonth = date('F',strtotime($row->monthrepeat. ' of +1 month'));
              $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +1 month'));
                if($thismonth != $nextmonth)
                { 

                  $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +2 month'));
                  $thismonth = date('F',strtotime('first day of +2 month'));
                  $nextmonth = date('F',strtotime($row->monthrepeat. ' of +2 month'));

                    if($thismonth != $nextmonth)
                    {
                      $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +3 month'));
                      $thismonth = date('F',strtotime('first day of +3 month'));
                      $nextmonth = date('F',strtotime($row->monthrepeat. ' of +3 month'));
                    }
                }
            
              }
             if($meeting_date >= $today1)
              {                 
               $row_date= date('Y-m-d', strtotime($row->meeting_date));
              }
              $finaltime=$row_date.' '.$row->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
              $final="<h4 class='text-center'>Next Scheduled Meeting: ";
              $final.= date('l, F dS Y @ h:i a T ',strtotime(($finaltime)));
              $final.="</h4>";
    
            }
            else
            {
               //meeting is repeated fix date of each month like 30th of each month
              if($meeting_date < $today1)
                {
                  $monthrepeat=explode('th',$row->monthrepeat);
                  $day=date('Y-m-'.$monthrepeat[0],strtotime('+1 month '));
                }
              if($meeting_date == $today1)
                {
                  $monthrepeat=explode('th',$row->monthrepeat);
                  $day=date('Y-m-'.$monthrepeat[0]);
                }  
              if($meeting_date > $today1)
                {
                  $row->monthrepeat. ' of '. $day;
                  $day= date('Y-m-d',strtotime($row->meeting_date));
                }    
              $finaltime=$day.' '.$row->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
              $final="<h4 class='text-center'>Next Scheduled Meeting: ";
              $final.= date('l, F dS Y @ h:i a T ',strtotime(($finaldate)));
              $final.="</h4>";
            }

          }
           //meeting is Repeated weekly 
            if($row->repeatTime == 'weekly')
           {
              $day=date('D');
              $day1=$row->repeatDay;
              if($meeting_date < $today1)
              {
                $day1=$row->repeatDay;
                $meeting_date=date('Y-m-d',strtotime('next '.$day1 ));
              }
              else
              {
               $meeting_date= date('Y-m-d',strtotime($row->meeting_date));
              }
             
              $finaltime=$meeting_date.' '.$row->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        
              $finaltime1=$meeting_date.' '.$row->meeting_time;
              $finaldate1= date('Y-m-d H:i',strtotime ($finaltime1 )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
              $today = date('Y-m-d H:i');

              $final="<h4 class='text-center'>Next Scheduled Meeting: ";
              $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaldate1)));
              $final.="</h4>";
           }

           //meeting is Repeated daily 
           if($row->repeatTime == 'daily')
           {  
              $today = date('Y-m-d H:i');
              $meeting_date1= date('Y-m-d',strtotime($row->meeting_date));
              if($meeting_date1 > $today1 )
              {               
                 $meeting_date= date('Y-m-d',strtotime($row->meeting_date));
              }   
              else
              {
                 $meeting_date= date('Y-m-d'); 
              }
              $finaltime=$meeting_date.' '.$row->meeting_time;
              $finaldate1= date('Y-m-d H:i',strtotime ( '+1 day' , strtotime ( $finaltime ) )) ;

              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
              $final="<h4 class='text-center'>Next Scheduled Meeting: ";
              $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaldate)));
              $final.="</h4>";
            }
          //set class disble 
          if($row->is_meeting_room_active  == '1') { $class='disabled'; } else { $class= 'hvr-reveal';}

              $today = date('Y-m-d H:i');
               //find status of meeting   
              if(($today >= $finaldate && $today <= $enddate) && ($row->is_meeting_room_active == '1'))
              {              
                 $status="Active";
              }
              if(($today >= $finaldate && $today <= $enddate) && ($row->is_meeting_room_active == '0'))
              {
                 $status="Open";
              }
              if((!($today >= $finaldate && $today <= $enddate)) && ($row->is_meeting_room_active == '0'))
              {
                $status="Closed";
              }
              if((!($today >= $finaldate && $today <= $enddate)) && ($row->is_meeting_room_active == '1') )
              {
                $status="Active";
              }
          //make array    
       $data1[] =
            array('headerText'=>$row->headerText,'name'=>$row->name, 'date'=>$finaldate,'id'=>$row->id,'meeting_id'=>$row->meeting_id,'is_meeting_room_active'=>$row->is_meeting_room_active,'final'=>$final,'class'=>$class,'status'=>$status,'startdate'=>$startdate,'enddate'=>$enddate,'today'=>$today);
       $i++;
     endforeach; 
       // array_sort_by_column($data1, 'name');
     
           $i=0; foreach ($data1 as $row): 
     //  echo "<pre>";print_r($row); echo "</pre>";
           ?>
        <div class="recording_box">
           <h3 class="text-center"><?= $row['headerText'] ?></h3>
           <div class="link_title">
              <h4>Meeting URL: <a  href="<?php echo $this->Url->build('/meetings/JoinAttendee/'.$row['meeting_id'], true); ?>">
                 <?php echo 'http://www.jointhismeeting.com/'.$row['meeting_id']; ?></a>
              </h4>
           </div>
           <div class="box">
              <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
                 <div class="btn btn-primary btn-block start_meeting meeting">
                    <a href="<?php echo $this->Url->build('/meetings/joinMeeting/'.$row['meeting_id'], true); ?>" oncontextmenu="return false;">
                      
                       <?php if($row['status']  == 'Active') { echo '<h1><i class="fa fa-play-circle-o"></i> Enter Meeting</h1>
                       <h4>Status: <span>Active'; } if($row['status']  == 'Closed') { echo '<h1><i class="fa fa-play-circle-o"></i> Start Meeting</h1>
                       <h4>Status: <span>Closed';} ?>
                        <?php if($row['status']  == 'Open') { echo '<h1><i class="fa fa-play-circle-o"></i> Enter Meeting</h1>
                       <h4>Status: <span>Open';} ?>
                       </span></h4>

                    </a>
                 </div>
              </div>
              <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
                 <div class="list_btn">
                  
                   
                        <?php if($row['is_meeting_room_active']  != '1') { ?>
                         <div class="viewers_btn <?php echo $row['class']; ?>">
                          <a href="meetings/editBasic/<?php echo $row['id'];?>" class="<?php echo $row['class']; ?>"> <?php } else {?>
                           <div class="viewers_btn <?php echo 'hvr-reveal'; ?>">
                             <a href="meetings/endMeetings/<?php echo $row['meeting_id'];?>" class="<?php echo 'hvr-reveal'; ?>">
                             <?php } ?>
                             <i class="fa fa-wrench"></i> 
                             <span>
                               <?php if($row['is_meeting_room_active']  != '1')
                                {  echo 'Edit Meeting Room'; } else { echo 'Close Meeting'; } ?></span>
                         </a> 
                     </div>
                    <div class="delete_btn <?php echo $row['class']; ?>"> 
                        <a href="meetings/newmeetingroomschedule/<?php echo  $row['id'];?>" class="<?php echo $row['class']; ?>">
                          <i class="fa fa-calendar"></i>
                           <span>Scheduling</span>
                          </a>
                      </div>
                        <div class="delete_btn <?php echo $row['class']; ?>"> 
                        <a href="/meetings/add-image-upload/<?php echo  $row['id'];?>" class="<?php echo $row['class']; ?>">
                            <i class="fa fa-pencil-square-o"></i>
                             <span>Registration</span>
                         </a>
               </div>

                 </div>
              </div>
              <div class="col-sm-4 col-ms-4 col-xs-12 col-lg-4">
                 <div class="list_btn">
                
                    <div class="delete_btn <?php echo $row['class']; ?>">
                         <a href="#" class="<?php echo $row['class']; ?>">
                            <i class="fa fa-search"></i> <span>Reports</span>
                         </a> 
                     </div>
                  
                      <div class="delete_btn <?php echo $row['class']; ?>">
                      <?php
                          $link= '';
                          foreach ($data as $row1):
                          if(@$row1['meetingId']['0'] == @$row['meeting_id'])
                          {
                          @$link= $row1['playbackFormatUrl'][0];
                      ?>

                        <a href="<?php echo @$link; ?>" target="_blank" class="<?php echo $row['class']; ?>">
                            <i class="fa fa-bar-chart"></i> <span>Recording</span>
                         </a> 
                         <?php } endforeach; 
                         if(@$link == '') { ?>
                            <a href="javascript:void(0);" class="<?php echo $row['class']; ?>">
                            <i class="fa fa-bar-chart"></i> <span>Recording</span>
                         </a>
                         <?php } ?>
                         
                     </div>
                      <div class="delete_btn <?php echo $row['class']; ?>">
                       <?= $this->Html->link('<i class="fa fa-remove"></i><span>Delete Meeting Room</span>',
                         array(
                                'action' => 'delete', $row['id'],
                              ),
                                array(
                                    'escape' => false,
                                    'class'=>$row['class'],
                                    'confirm' => __('Are you sure you want to completely delete this meeting room, as well as all reports connected to it?', $row['id']),
                                  )
                            );
                        ?>
                         
                     </div>
                 </div>
              </div>
              <div class="clearfix">
                
            <?php echo $row['final']; ?>
              </div>
           </div>
        </div>
        <?php
         $i++;
         endforeach; 
        ?>
        <div class="pagination_box">
           <div class="row">
              <div class="col-sm-6">
                 <div class="numeric_paginaion">
                    <ul>
                      <?php   echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
                          ['escape' => false]); ?>
                       <?PHP echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
                        <?php   echo $this->Paginator->next('<i class="fa fa-fast-forward"></i>',
                          ['escape' => false]); ?>
                    </ul>
                 </div>
              </div>
            </div>
        </div>
         <?php
          }
            else
          {
            echo '<h1>No Meeting Found.</h1>';
          }
        ?>
    </div>
  </section>
  

