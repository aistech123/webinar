  <?php 
 date_default_timezone_set($meeting->timezone);
         $today = date('Y-m-d H:i');
         $meeting_date=$meeting->meeting_date;
            if($meeting->repeatTime == '')
        {  
         $finaltime=$meeting_date.' '.$meeting->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          $duaration=($meeting->meeting_duration) +30;
          $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        }

         if($meeting->repeatTime == 'weekly')
        {  
            
            $day=date('D');
            if($day == $meeting->repeatDay)
            {
              $meeting_date= date('Y-m-d');
              $finaltime=$meeting_date.' '.$meeting->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        
             }
             else
             {
              $finaldate="''";
              $enddate="''";
             }

        }
//daily
         if($meeting->repeatTime == 'daily')
        {  
          $meeting_date= date('Y-m-d');
          $finaltime=$meeting_date.' '.$meeting->meeting_time;

          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        
        }

//monthly
          if($meeting->repeatTime == 'monthly')
           {  
              $meeting->monthrepeat;
              $day=date('F Y');
              $monthrepeat=explode(' ',$meeting->monthrepeat);
                if($monthrepeat['1'] != '')
                {
                  $meeting->monthrepeat. ' of '. $day;
                  $meeting_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of '. $day));

                  $finaltime=$meeting_date.' '.$meeting->meeting_time;
                  $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
                  $enddate= date('Y-m-d H:i',strtotime ( '30 minute' , strtotime ( $finaldate ) )) ;
                }
                else
                {
                  $monthrepeat=explode('th',$meeting->monthrepeat);
                  $day=date('Y-m-'.$monthrepeat[0]);

                  $finaltime=$day.' '.$meeting->meeting_time;
                  $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
                   $duaration=($meeting->meeting_duration) +30;
                 $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
        
                }
         }
 
   if($meeting->Reg_HeaderText != '') {
      $Reg_HeaderText =html_entity_decode($meeting->Reg_HeaderText);
    }
    else
    {
      $Reg_HeaderText ='';
    }

   ?>

    

 <?php 


 
 ?> 
 <?php if($meeting->Reg_HeaderText != '') {
                                        $Reg_HeaderText =html_entity_decode($meeting->Reg_HeaderText);
                                      }
                                      else
                                      {
                                        $Reg_HeaderText ='';
                                      }
        if($meeting->joinText != '') {
                                        $joinText =$meeting->joinText;
                                      }
                                      else
                                      {
                                        $joinText ='Login by entering your information below';
                                      }
        if($meeting->joinButtonText != '') {
                                        $joinButtonText =$meeting->joinButtonText;
                                      }
                                      else
                                      {
                                        $joinButtonText ='Join Meeting';
                                      }  
                                      ?>               
<div class="hdr_title">
  <h2><?= $Reg_HeaderText ?></h2>
</div>
<div class="banner_join_webinar">
    
   <?php  if($meeting->presentation_file != '')
                              { ?>
                                   <img src="/webroot/files/Meetings/presentation_file/<?php echo $meeting->presentation_file; ?>" />
                                   <?php }
                                      else
                                      { ?>
                                     <img src="/webroot/img/Reg_Logo.png" />

                                     <?php  }
                                    ?>
</div>

<div class="module form-module">
  <div class="login-header">
    <h2><?= $joinText; ?></h2>
  </div>
  <div class="form">
              <?= $this->Form->create($attendees,array('id'=>'attedee')) ?>
                <div class="form-group row">
                    <div class="col-sm-12">
                                    <?php if($meeting->attendeeName != '') {
                                        $attendeeName =$meeting->attendeeName;
                                      }
                                      else
                                      {
                                        $attendeeName ='Name';
                                      }
                                     if($meeting->attendeeEmail != '') {
                                        $attendeeEmail =$meeting->attendeeEmail;
                                      }
                                      else
                                      {
                                        $attendeeEmail ='Email';
                                      }

                                       if($meeting->attendeePassword != '') {
                                        $attendeePassword =$meeting->attendeePassword;
                                      }
                                      else
                                      {
                                        $attendeePassword ='Password';
                                      }
                                      
                                      ?>
                                    <?= $this->Form->input('username',['label'=>false,'class'=>'form-control','data-bvalidator'=>'required','placeholder'=>$attendeeName]) ?>
                                </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                                    <?= $this->Form->input('email',['label'=>false,'class'=>'form-control','readonly'=>'readonly','placeholder'=>$attendeeEmail]) ?>
                             </div>
                      </div>
                         
                            <?php if($meeting->password != '') { ?>
                              <div class="form-group row">
                                <div class="col-sm-12">
                                      <?= $this->Form->input('password',['label'=>false,'class'=>'form-control','readonly'=>'readonly','placeholder'=>$attendeePassword]) ?>
                               
                                <input type="hidden" id="confpassword" value="<?php echo $meeting->password; ?>">
                             
                              </div>
                          </div>
                         <?php } ?>
                      </div>

<div class="login-footer">
<div class="text-center">
  <?= $this->Form->button($joinButtonText,['type'=>'submit','id'=>'start_meeting','class'=>'btn btn-success','value'=>$joinButtonText]) ?>
  
</div>
<div class="clearfix"></div>
</div>
</div>
<?php
if($countmeeting > 0)
  {   
    if(($today >= $finaldate && $today <= $enddate) || ($meeting->is_meeting_room_active == '1'))
    {

 } 

  }
?>

    
<script type="text/javascript">
    // $('#attedee').bValidator();
      var editor = CKEDITOR.replace( 'editor1', {
          filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
          filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
          filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
          filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
          filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
          filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
</script>
 
