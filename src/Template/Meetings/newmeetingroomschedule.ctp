    <?= $this->Html->css('timepicki.css') ?>
  <?php echo $this->element('inner_menu'); ?>
  <?php echo $this->element('center_menu'); ?>
              <?= $this->Form->create($meeting,array('id'=>'meeting')) ?>
                  <div class="row">
                      <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Date</label>
                          <div class="input-group date" id="datetimepicker1" >
                                <?= $this->Form->input('meeting_date',['label'=>false,'type'=>'text','name'=>'meeting_date','value'=>$meetingDate,'class'=>'form-control date-picker']) ?>

                           
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                          </div>
                      </div>
                      </div>
                      <div class="col-sm-6">
                       <div class="form-group">
                          <label class="control-label">Time</label>
                          <div class="input-group date">
                              <div class="time_pick">
                               <?= $this->Form->input('meeting_time',['label'=>false,'type'=>'text','id'=>'timepicker','data-bvalidator'=>'required','value'=>$meeting->meeting_time,'name'=>'meeting_time','class'=>'form-control']) ?>

                              <div class="timepicker_wrap "><div class="arrow_top"></div><div class="time"><div class="prev action-prev"></div><div class="ti_tx"><input class="timepicki-input" type="text"></div><div class="next action-next"></div></div><div class="mins"><div class="prev action-prev"></div><div class="mi_tx"><input class="timepicki-input" type="text"></div><div class="next action-next"></div></div><div class="meridian"><div class="prev action-prev"></div><div class="mer_tx"><input class="timepicki-input" readonly="readonly" type="text"></div><div class="next action-next"></div></div></div></div>
                              <span class="input-group-addon">
                                  <span class="glyphicon glyphicon-time"></span>
                              </span>
                          </div>
                      </div>
                      </div>
                  </div>
                  <div class="row">
                      <!-- <div class="col-sm-6">
                          <div class="form-group">
                          <label class="control-label">Duration</label> (In Minutes)
                          <select class="form-control" name="meeting_duration">
                              <option value="10" <?php if($meeting->meeting_duration == '10') { echo "selected=selected"; }?>>10</option>
                              <option value="15" <?php if($meeting->meeting_duration == '15') { echo "selected=selected"; }?>>15</option>
                              <option value="30" <?php if($meeting->meeting_duration == '30') { echo "selected=selected"; }?>>30</option>
                              <option value="60" <?php if($meeting->meeting_duration == '60') { echo "selected=selected"; }?>>60</option>
                          </select>                        
                          </div>
                      </div> -->
                      <div class="col-sm-6">
                          <div class="form-group">
                          <label class="control-label">Time Zone</label>
                          <?= $this->Form->select('timezone',$timezoneList,['label'=>false,'class'=>'form-control']) ?>             
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="checkbox checkbox-default">
                              <input id="repeatcheck" name="repeat" class="" type="checkbox" <?php if($meeting->repeatTime != ''){ echo "checked=checked"; } ?>>
                              <label>Repeat Event</label>                    
                          </div>
                      </div>
                  </div>
                  <div id="repeatdiv">
                  <div class="row">
                      <div class="col-sm-6">
                          <div class="form-group">
                          <label class="control-label">Meeting Repeats</label>
                          <select class="form-control" name="repeatTime" id="repeatTime">
                             <option value="">--</option>
                              <option value="daily" <?php if($meeting->repeatTime == 'daily') { echo "selected=selected"; }?>>Daily</option>
                              <option value="weekly" <?php if($meeting->repeatTime == 'weekly') { echo "selected=selected"; }?>>Weekly</option>
                              <option value="monthly" <?php if($meeting->repeatTime == 'monthly') { echo "selected=selected"; }?>>Monthly</option>
                          </select>                        
                          </div>
                      </div>                    
                  </div>
                    <?php $repeatDay=explode(',',$meeting->repeatDay); ?>
                  <hr class="m-t-b-5">
                  <div class="row" id="repeatDay">
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input  class="styled" id="checkbox2" type="radio" value="Mon" name="repeatDay[]" <?php if (in_array("Mon", $repeatDay)){ echo "checked=checked"; }?>>
                              <label>Mon</label>                    
                          </div>
                            
                      </div>
    
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input  name="repeatDay[]" class="styled" id="checkbox2" value="Tue" type="radio" <?php if (in_array("Tue", $repeatDay)){ echo "checked=checked"; }?>>
                              <label>Tue</label>                    
                          </div>
                      </div>
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input name="repeatDay[]" class="styled" id="checkbox2" value="Wed" <?php if (in_array("Wed", $repeatDay)){ echo "checked=checked"; }?> type="radio">
                              <label>Wed</label>                    
                          </div>
                      </div> 
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input name="repeatDay[]" class="styled" id="checkbox2" value="Thu" <?php if (in_array("Thu", $repeatDay)){ echo "checked=checked"; }?> type="radio">
                              <label>Thu</label>                    
                          </div>
                      </div>
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input name="repeatDay[]" class="styled" id="checkbox2" value="Fri"  <?php if (in_array("Fri", $repeatDay)){ echo "checked=checked"; }?> type="radio">
                              <label>Fri</label>                    
                          </div>
                      </div>
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input name="repeatDay[]" class="styled" id="checkbox2" value="Sat"  <?php if (in_array("Sat", $repeatDay)){ echo "checked=checked"; }?> type="radio">
                              <label>Sat</label>                    
                          </div>
                      </div>
                      <div class="col-sm-1 col-xs-3">
                          <div class="">
                              <input name="repeatDay[]" class="styled" id="checkbox2" value="Sun"  <?php if (in_array("Sun", $repeatDay)){ echo "checked=checked"; }?> type="radio">
                              <label>Sun</label>   


                          </div>
                      </div>
                  </div>

                 
                  <div class="row"  id="repeatMonth">
                      <div class="col-sm-6 col-xs-12">
                          <div class="">
                           <input type="radio" class="styled" id="monthrepeat" name="monthrepeat[]" value="" > 
                            <label>On the <label for="repeat2"></label><sup>th</sup> of each Month</label>               
                          </div>
                      </div>
                      <div class="col-sm-6 col-xs-12">
                          <div class="" >
                            <input type="radio" class="styled" id="monthrepeat1" name="monthrepeat[]" value=""> 
                            <input type="hidden" id="repeatval" value=""/>
                            <label>On the <label for="repeat1"></label> of each Month</label>                                     
                          </div>
                      </div>
					  <div class="col-sm-12"> <hr class="m-t-b-5"> </div>
                  </div>
                  
                  <div class="row">
                     <div class="col-sm-12"><hr class="m-t-b-5"></div>
                  </div>
                  </div>
                  <div class="row">
                      <div class="col-sm-4 col-xs-6">
                       <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                         
                         
                      </div>
                      <div class="col-sm-4 col-xs-6 pull-right">
                        <?= $this->Form->button('Start Now',['type'=>'submit','name'=>'start','class'=>'btn btn-success btn-block']) ?>
                         
                      </div>
                      
                  </div>
                <?= $this->Form->end() ?>
          </div>
          
          
      </div>
  </section>

</div>

  <script>
   function weekAndDay(meeting_date) {

    var date = new Date(meeting_date),
        days = ['sun','mon','tue','wed',
                'thu','fri','sat'],
        prefixes = ['first', 'second', 'third', 'fourth', 'fifth'];

    return prefixes[0 | date.getDate() / 7] + ' ' + days[date.getDay()];
    }

    $('#meeting').bValidator();

    $('#repeatDay').hide();
    $('#repeatMonth').hide();
       
      $(".date-picker").datepicker({
      dateFormat: 'yyyy-mm-dd'
      });
      
      $(".date-picker").on("change", function() {

                     meeting_date=$('#meeting-date').val();
                      var d = new Date(meeting_date);
                     
                      jQuery("label[for='repeat1']").html(weekAndDay(meeting_date));
                      $("#monthrepeat1").val(weekAndDay(meeting_date));

                      jQuery("label[for='repeat2']").html(d.getDate());
                        $("#monthrepeat").val(d.getDate()+'th');
                        $("#monthrepeat").prop("checked", false);
                        $("#monthrepeat1").prop("checked", false);
                     });

                      meeting_date=$('#meeting-date').val();
                      var d = new Date(meeting_date);
                     
                      jQuery("label[for='repeat1']").html(weekAndDay(meeting_date));
                      $("#monthrepeat1").val(weekAndDay(meeting_date));

                      jQuery("label[for='repeat2']").html(d.getDate());
                      $("#monthrepeat").val(d.getDate()+'th');
                      var repeatval='<?php echo $meeting->monthrepeat; ?>';
                      if(repeatval == (weekAndDay(meeting_date)))
                      {
                        $("#monthrepeat1").prop("checked", true);
                      }
                      if(repeatval == (d.getDate()+'th'))
                      {
                        $("#monthrepeat").prop("checked", true);
                      }



      $('#timepicker').timepicki(
          {
              show_meridian:false,
              min_hour_value:0,
              max_hour_value:23,
              step_size_minutes:15,
              overflow_minutes:true,
              increase_direction:'up',
              disable_keyboard_mobile: true
          });

    
      
          $("input[name='monthrepeat[]']").on('change', function() {
          $("input[id='checkbox2']").prop("checked", false);
      });

        
     $("input[name='repeatDay[]']").change(function(){
              
              $("#monthrepeat").prop("checked", false);
              $("#monthrepeat1").prop("checked", false);
         
      });
  
      $('#repeatcheck').change(function(){
         
             if ($(this).is(':checked')) {

                      $('#repeatdiv').show();
                      meeting_date=$('#meeting-date').val();
                      var d = new Date(meeting_date);
                     
                      jQuery("label[for='repeat1']").html(weekAndDay(meeting_date));
                      $("#monthrepeat1").val(weekAndDay(meeting_date));

                      jQuery("label[for='repeat2']").html(d.getDate());
                      $("#monthrepeat").val(d.getDate()+'th');

                      $("input[id='checkbox2']").prop("checked", false);
                      $("#monthrepeat").prop("checked", false);
                      $("#monthrepeat1").prop("checked", false);

                  } else {
                      
                      $("input[id='checkbox2']").prop("checked", false);
                      $("#monthrepeat").prop("checked", false);
                      $("#monthrepeat1").prop("checked", false);
                      $("option:selected").removeAttr("selected");
                      $('#repeatdiv').hide();

                  }
           });
              repeatTime=$("#repeatTime").val();
              if(repeatTime =='monthly' )
                        {
                          $('#repeatDay').hide();
                          $('#repeatMonth').show();
                          
                        }
              if(repeatTime =='daily' )
                {
                  $('#repeatDay').hide();
                  $('#repeatMonth').hide();
                  $("#monthrepeat").prop("checked", false);
                  $("#monthrepeat1").prop("checked", false);
                  $("input[id='checkbox2']").prop("checked", false);
                }
              if(repeatTime =='weekly' )
                {
                  $('#repeatDay').show();
                  $('#repeatMonth').hide();
                 
                }

                   if(repeatTime =='')
                   {
                      
                      $('#repeatdiv').hide();
                       $("#repeatcheck").prop("checked", false);
                   }
              $('select').on('change', function() {
                if(this.value =='monthly' )
                {
                  $('#repeatDay').hide();
                  $('#repeatMonth').show();
                  
                }
                 
                  });
              $('select').on('change', function() {
                if(this.value =='daily' )
                {
                  $("#monthrepeat").prop("checked", false);
                  $("#monthrepeat1").prop("checked", false);
                  $("input[id='checkbox2']").prop("checked", false);
                   $('#repeatDay').hide();
                  $('#repeatMonth').hide();
                }
                 
          });
              $('select').on('change', function() {
                if(this.value =='weekly' )
                {
                  $('#repeatDay').show();
                  $('#repeatMonth').hide();
                 
                }
                 
                  });

  </script>
