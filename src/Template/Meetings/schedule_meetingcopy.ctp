    <section class="inner_menu">
      <div class="container">
        <div class="center_menu">
            <ul>
              <li class="active" id="pastlink"><a href="/meetings/schedule_meeting/past" class="btn btn-primary">Past Meetings</a></li>
              <li id="upcominglink"><a href="/meetings/schedule_meeting/upcoming" class="btn btn-primary" >Upcoming Meetings</a></li>
            </ul>
        </div>
      </div>
    </section>
   <section class="main_content">
<!--<div class="page_title">
        <div class="text-center">
          <h1><span>Coming Soon</span></h1>
        </div>
        </div>
      </div>
</section> -->
      <section>
        <div class="container">        
            <div class="page_title">
                <div class="text-center">
                    <h1><span id="label">Past Meetings</span></h1>
                    <input type="hidden" name="type" id="type" value="<?= $type; ?>">
                </div>
            </div>  
        </div>
    </section>

    <section class="main_content">
        <div class="container" id="past">
            <?php 
            $pastData = array();
            foreach ($Pastmeeting as $row) { 
                 @date_default_timezone_set($row->timezone);
            $pastData[] = array('id'=>$row->id,'meeting_name' => $row->meeting_name ,'durationOfMeeting'=>$row->durationOfMeeting,'TotalAttendee'=>$row->TotalAttendee,'created_at'=>$row->created_at,'meeting_id'=>$row->meeting_id,'attendeesMeeting_id'=>$row->attendeesMeeting_id);
                }
            /*    echo "<pre>";print_r($pastData); exit;
           array_sort_by_column($pastData,'created_at',SORT_DESC);*/
        if((count($pastData) > 0))
         {
            foreach ($pastData as $row) { 
                $meetingDate= base64_encode($row['created_at']);   
                $meetingName= base64_encode($row['attendeesMeeting_id']);   
                ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="text-center"><?= $row['meeting_name']; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-4 col-xs-12 col-md-6 col-sm-6">
                        <div class="btn btn-primary btn-block play_btn"><a href="../PastmeetingDetails/<?php echo $row['meeting_id'].'/'.$meetingDate; ?>"><h1><i class="fa fa-bar-chart"></i> Meeting Report </h1></a></div>
                    </div>
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                        <div class="text-center m-t-10">
                            <label>Started Time</label>
                                 <p><?= date('l, F dS Y @ h:i a T ',strtotime(($row['created_at']))); ?>  </p>
                        </div>
                    </div>                
                    <div class="col-lg-2 col-xs-12 col-md-6 col-sm-6">
                        <div class="text-center m-t-10">
                            <label>Duration</label>
                            <p><?= $row['durationOfMeeting'] ?> minutes</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-12 col-md-6 col-sm-6">
                <div class="text-center m-t-10">
                    <label>Total Attendees</label>
                      <p><?= $row['TotalAttendee'] ?></p>
                       <?= $this->Html->link('<button class="btn btn-danger">Delete</button>',
                         array(
                               'action' => 'Deleteattendees',$meetingName,$meetingDate
                                ),
                            array(
                                'escape' => false,
                'confirm' => __('Are you sure you want to delete',$row['meeting_name']),
                            )
                            ); ?>
                        </div>
                       
                    </div>         
                </div>
            </div>
            <?php }
           ?>
            <div class="pagination_box">
             <div class="row">
                <div class="col-sm-6">
                   <div class="numeric_paginaion">
                      <ul>
                         <?PHP echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
                      </ul>
                   </div>
                </div>
                <div class="col-sm-6 text-right">
                   <div class="next_previous">
                      <ul>
                         <?php 
                           echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
                            ['escape' => false]); ?>
                     <?php 
                           echo $this->Paginator->next('<i class="fa fa-fast-forward"></i>',
                            ['escape' => false]); ?>
                      </ul>
                   </div>
                </div>
             </div>
          </div>
          <?php } else { echo "<h2>Data Not Available.</h2>"; } ?>
        </div>
        <div class="container" id="upcoming">
            <?php 
           $data=array();
           $i=0;     
         foreach ($upComingmeeting as $row){
          @date_default_timezone_set($row->timezone);
          $meeting_date=date('Y-m-d',strtotime($row->meeting_date));
          $today = date('Y-m-d H:i');
          $today1 = date('Y-m-d');
          if($row->repeatTime == '')
          {

            @$finaltime=$meeting_date.' '.$row->meeting_time;
             $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($row->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;

              if($enddate > $today)
              {
                $final="<h4 class='text-center'>";
                $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaltime)));
                $final.="</h4>";

              }
              if(($today >= $finaldate && $today <= $enddate))
              {
                $final="<h4 class='text-center'>";
                $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaltime)));
                $final.="</h4>";

              }
              if($enddate < $today)
              {
                $final="<h4 class='text-center'>No Future Meetings have been Scheduled";
                $final.="</h4>";
              }
          }
 
//monthly Repeating
         if($row->repeatTime == 'monthly')
         {
            $today = date('Y-m-d H:i');
            $month = date('F');
            $year = date('Y');
            $row->meeting_time;
            $meeting_date=date('Y-m-d',strtotime($row->meeting_date));
            $row->monthrepeat;
            $day=date('F Y');
            $monthrepeat=explode(' ',$row->monthrepeat);
          if($monthrepeat['1'] != '')
          {

            if($meeting_date < $today1)
            {
              //if month do not fifth monday or tuesday
            
            $thismonth = date('F',strtotime('first day of +1 month'));
            $nextmonth = date('F',strtotime($row->monthrepeat. ' of +1 month'));
            $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +1 month'));
              if($thismonth != $nextmonth)
              { 

                $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +2 month'));
                $thismonth = date('F',strtotime('first day of +2 month'));
                $nextmonth = date('F',strtotime($row->monthrepeat. ' of +2 month'));

                  if($thismonth != $nextmonth)
                  {
                     $row_date= date('Y-m-d', strtotime($row->monthrepeat. ' of +3 month'));
                    $thismonth = date('F',strtotime('first day of +3 month'));
                    $nextmonth = date('F',strtotime($row->monthrepeat. ' of +3 month'));
                  }
              }
          
            }
             if($meeting_date >= $today1)
              {                 
               $row_date= date('Y-m-d', strtotime($row->meeting_date));
              }
              $finaltime=$row_date.' '.$row->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($row->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
              $final="<h4 class='text-center'>Next Scheduled Meeting: ";
              $final.= date('l, F dS Y @ h:i a T ',strtotime(($finaltime)));
              $final.="</h4>";
    
          }
          else
          {
            if($meeting_date < $today1)
              {
                $monthrepeat=explode('th',$row->monthrepeat);
                $day=date('Y-m-'.$monthrepeat[0],strtotime('+1 month '));
              }
            if($meeting_date == $today1)
              {
                $monthrepeat=explode('th',$row->monthrepeat);
                $day=date('Y-m-'.$monthrepeat[0]);
              }  
              if($meeting_date > $today1)
              {
                $row->monthrepeat. ' of '. $day;
                $day= date('Y-m-d',strtotime($row->meeting_date));
              }    
            $finaltime=$day.' '.$row->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($row->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
            $final="<h4 class='text-center'>Next Scheduled Meeting: ";
            $final.= date('l, F dS Y @ h:i a T ',strtotime(($finaldate)));
            $final.="</h4>";
          }
                    
      }
      //weekly
        if($row->repeatTime == 'weekly')
       {
        
       $day=date('D');
       $day1=$row->repeatDay;
        if($meeting_date < $today1)
        {
           $day1=$row->repeatDay;
           if($day1 == $day)
           {
            $meeting_date= date('Y-m-d');
           } 
           else
           {
           $meeting_date=date('Y-m-d',strtotime('next '.$day1 ));
            }
        }
        else
        {
           $meeting_date= date('Y-m-d',strtotime($row->meeting_date));
        }

          $finaltime=$meeting_date.' '.$row->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ($finaltime )) ;
          $today = date('Y-m-d H:i');
          $final="<h4 class='text-center'>";
          $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaldate)));
          $final.="</h4>";
       }

       //daily
       if($row->repeatTime == 'daily')
       { 
          $today = date('Y-m-d H:i');
          $meeting_date1= date('Y-m-d',strtotime($row->meeting_date));
          if($meeting_date1 > $today1 )
          {
             $meeting_date= date('Y-m-d',strtotime($row->meeting_date));
          }   
          else
          {
              $meeting_date= date('Y-m-d'); 
          }
          $finaltime=$meeting_date.' '.$row->meeting_time;

          $finaldate1= date('Y-m-d H:i',strtotime ( '+1 day' , strtotime ( $finaltime ) )) ;
      
           $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          $duaration=($row->meeting_duration) +30;
          $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;

         $final="<h4 class='text-center'>Next Scheduled Meeting: ";
         $final.=date('l, F dS Y @ h:i a T ',strtotime(($finaldate)));
         $final.="</h4>";
        }
              $today = date('Y-m-d H:i');
                  if(($today >= $finaldate && $today <= $enddate) && ($row->is_meeting_room_active == '1'))
                  {
                  
                     $status="Active";
                
                  }
                    if(($today >= $finaldate && $today <= $enddate) && ($row->is_meeting_room_active == '0'))
                  {
                  
                     $status="Open";
                
                  }
                  if((!($today >= $finaldate && $today <= $enddate)) && ($row->is_meeting_room_active == '0'))
                  {
                  
                     $status="Closed";

                  }
                  if((!($today >= $finaldate && $today <= $enddate)) && ($row->is_meeting_room_active == '1') )
                  {
                     $status="Active";

                  }
               if(($status == 'Closed' && ($finaldate > $today)))
               {   
                $data[] =
                array('name'=>$row->headerText, 'date'=>$final,'id'=>$row->id,'finaltime'=>$finaldate,'status'=>$status);
              }

                $i++;
          }

            array_sort_by_column($data, 'finaltime',SORT_ASC);
             
            foreach ($data as $key => $row) {
                  // echo "<pre>";print_r($row['name']);
                ?>
           <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="text-center"><?= $row['name']; ?></h3>
                </div>
                <div class="panel-body">
                    <div class="col-lg-6 col-xs-12 col-md-6 col-sm-6">
                        <div class="text-center">
                            <label>Starting Time</label>
                            <p><?= $row['date'] ?><!-- <?= date('D,M d Y',strtotime(($row['date']))); ?><br><?=  date('H:i',strtotime($row['date'])); ?> --></p>
                        </div>
                    </div>                
                    <div class="col-lg-4 col-xs-12 col-md-6 col-sm-6">
                        <div class="list_btn">
                            <div class="delete_btn hvr-reveal">
                                <a href="/meetings/add-image-upload/<?php echo $row['id'];?>"><i class="fa fa-pencil-square-o"></i> <span>Registration</span></a>
                            </div>
                            <div class="viewers_btn hvr-reveal"> 
                                <a href="/meetings/newmeetingroomschedule/<?php echo $row['id'];?>"><i class="fa fa-wrench"></i> <span>Edit Meeting Room</span></a> 
                            </div>
                        </div>
                    </div>
                 
                    <div class="col-lg-2 col-xs-12 col-md-6 col-sm-6">
                        <div class="btn btn-primary btn-block cancel_meeting_btn">
                             <?= $this->Html->link('<h1>Cancel Meeting</h1>',
                         array(
                               
                             'action' => 'delete', $row['id'],
                                ),
                            array(
                                'escape' => false,
                                'confirm' => __('Are you sure you want to delete # {0}?', $row['id']),
                               )
                            ); ?>
                            
                       </div>
                    </div>       
                </div>
            </div>
            <?php  } 
           
            ?>
        <div class="pagination_box">
             <div class="row">
                <div class="col-sm-6">
                   <div class="numeric_paginaion">
                      <ul>
                        <?php 
                           echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
                            ['escape' => false]); ?>
                         <?PHP echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
                           <?php 
                           echo $this->Paginator->next('<i class="fa fa-fast-forward"></i>',
                            ['escape' => false]); ?>
                      </ul>
                   </div>
                </div>
               
             </div>
          </div>

        </div>
    </section> 
    <script type="text/javascript">
    $(document).ready(function() {
        var type= $("#type").val();
        
         if(type == 'upcoming')
         {
            $("#past").fadeOut("slow");
            $("#upcoming").fadeIn("slow");
            $("#label").text('Upcoming Meetings');

            $("li#upcominglink").addClass('active');
            $('li#pastlink').removeClass('active');
          }
         else
         {
            $("#past").fadeIn("slow");
            $("#upcoming").fadeOut("slow");
            $("#label").text('Past Meetings');

            $("li#pastlink").addClass('active');
            $('li#upcominglink').removeClass('active');
         }
          //  $("#type").hide();
          $("#pastlink").click(function() {
            $("#past").fadeOut("slow");
            $("#upcoming").fadeIn("slow");
            $("#label").text('Past Meetings');
            $("li#pastlink").addClass('active');
            $('li#upcominglink').removeClass('active');
          });
          $("#upcominglink").click(function() {
              $("#label").text('Upcoming Meetings');
              $("li#upcominglink").addClass('active');
              $('li#pastlink').removeClass('active');
              $("#past").fadeIn("slow");
              $("#upcoming").fadeOut("slow");
          });
      
    });
    </script>

        

