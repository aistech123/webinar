  <?php 
      date_default_timezone_set($meeting->timezone);
      $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
      $today = date('Y-m-d H:i');
      $today1 = date('Y-m-d');
      if($meeting->repeatTime == '')
      {
        $finaltime=$meeting_date.' '.$meeting->meeting_time;
        $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
        $duaration=($meeting->meeting_duration) +30;
        $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
      }
      if($meeting->repeatTime == 'weekly')
      {
          $day=date('D');
          $day1=$meeting->repeatDay;
          if($meeting_date < $today1)
          {
             $day1=$meeting->repeatDay;
             if($day1 == $day)
             {
              $meeting_date= date('Y-m-d');
             } 
             else
             {
             $meeting_date=date('Y-m-d',strtotime('next '.$day1 ));
              }
          }
          else
          {
             $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
          }
          $finaltime=$meeting_date.' '.$meeting->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          $duaration=($meeting->meeting_duration) +30;
          $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
      }
      //daily
      if($meeting->repeatTime == 'daily')
      {
          if($meeting_date > $today1 )
          {
             $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
          }   
          else
          {
              $meeting_date= date('Y-m-d'); 
          }
          $finaltime=$meeting_date.' '.$meeting->meeting_time;
          $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
          $duaration=($meeting->meeting_duration) +30;
          $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;

      }
      //monthly
      if($meeting->repeatTime == 'monthly')
      {
          $month = date('F');
          $year = date('Y');
          $meeting->meeting_time;
          $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
          
          $meeting->monthrepeat;
          $day=date('F Y');
          $monthrepeat=explode(' ',$meeting->monthrepeat);
          if($monthrepeat['1'] != '')
          {
            if($meeting_date < $today1)
            {
                //if month do not fifth monday or tuesday
                  
              $thismonth = date('F',strtotime('first day of +1 month'));
              $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +1 month'));
              $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +1 month'));
                if($thismonth != $nextmonth)
                { 
                  $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +2 month'));
                  $thismonth = date('F',strtotime('first day of +2 month'));
                  $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +2 month'));

                    if($thismonth != $nextmonth)
                    {
                       $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +3 month'));
                      $thismonth = date('F',strtotime('first day of +3 month'));
                      $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +3 month'));
                    }
                }   
            }
             else
            {
              $row_date= date('Y-m-d',strtotime($meeting->meeting_date));
            }
            $finaltime=$row_date.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $duaration=($meeting->meeting_duration) +30;
            $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
          }
          else
          {
              if($meeting_date < $today1)
              {
                $monthrepeat=explode('th',$meeting->monthrepeat);
                $day=date('Y-m-'.$monthrepeat[0],strtotime('+1 month '));
              }
              else
              {
                $row->monthrepeat. ' of '. $day;
                $day= date('Y-m-d',strtotime($meeting->meeting_date));
              }    
          
              $monthrepeat=explode('th',$meeting->monthrepeat);
              $day=date('Y-m-'.$monthrepeat[0]);
              $finaltime=$day.' '.$meeting->meeting_time;
              $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
              $duaration=($meeting->meeting_duration) +30;
              $enddate= date('Y-m-d H:i',strtotime ( '+'.$duaration.' minute' , strtotime ($finaltime) )) ;
          }
    }

      if($meeting->Reg_HeaderText != '') {
        $Reg_HeaderText =html_entity_decode($meeting->Reg_HeaderText);
      }
      else
      {
       $Reg_HeaderText ='';
      }

   ?>

 <?php 
 if($countmeeting > 0)
  {   
    if(($today >= $finaldate && $today <= $enddate) || ($meeting->is_meeting_room_active == '1'))
    {
 ?> 
 <?php 
          if($meeting->Reg_HeaderText != '')
          {
           $Reg_HeaderText =html_entity_decode($meeting->Reg_HeaderText);
          }
          else
          {
            $Reg_HeaderText ='';
          }

          if($meeting->joinText != '') {
            $joinText =$meeting->joinText;
          }
          else
          {
            $joinText ='Login by entering your information below';
          }

          if($meeting->joinButtonText != '') {
           $joinButtonText =$meeting->joinButtonText;
          }
          else
          {
            $joinButtonText ='Join Meeting';
          }
  ?>               
<div class="hdr_title">
  <h2><?= $Reg_HeaderText ?></h2>
</div>
<div class="banner_join_webinar">
    
   <?php  if($meeting->presentation_file != '')
          { ?>
            <img src="/webroot/files/Meetings/presentation_file/<?php echo $meeting->presentation_file; ?>" />
           <?php }
              else
              { ?>
             <img src="/webroot/img/Reg_Logo.png" />

    <?php  } ?>
            
</div>

<div class="module form-module">
  <div class="login-header">
    <h2><?= $joinText; ?></h2>
  </div>
  <div class="form">
    <?= $this->Form->create($attendees,array('id'=>'attedee')) ?>
      <div class="form-group row">
          <div class="col-sm-12">
              <?php if($meeting->attendeeName != '') {
                  $attendeeName =$meeting->attendeeName;
                }
                else
                {
                  $attendeeName ='Name';
                }
               if($meeting->attendeeEmail != '') {
                  $attendeeEmail =$meeting->attendeeEmail;
                }
                else
                {
                  $attendeeEmail ='Email';
                }

                 if($meeting->attendeePassword != '') {
                  $attendeePassword =$meeting->attendeePassword;
                }
                else
                {
                  $attendeePassword ='Password';
                }
                
                ?>
              <?= $this->Form->input('username',['label'=>false,'class'=>'form-control','data-bvalidator'=>'required','placeholder'=>$attendeeName]) ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-12">
          <?= $this->Form->input('email',['label'=>false,'class'=>'form-control','data-bvalidator'=>'required,email','placeholder'=>$attendeeEmail]) ?>
          </div>
        </div>
             
      <?php if($meeting->password != '') { ?>
        <div class="form-group row">
          <div class="col-sm-12">
                <?= $this->Form->input('password',['label'=>false,'class'=>'form-control','data-bvalidator'=>"equalto[confpassword],valempty",'data-bvalidator-msg'=>'Password is Wrong','placeholder'=>$attendeePassword]) ?>
         
          <input type="hidden" id="confpassword" value="<?php echo $meeting->password; ?>">
       
        </div>
    </div>
   <?php } ?>
 </div> <!-- form class end-->
    <div class="login-footer">
      <div class="text-center">
        <?= $this->Form->button($joinButtonText,['type'=>'submit','id'=>'start_meeting','class'=>'btn btn-success','value'=>$joinButtonText]) ?>
  
        <?= $this->Form->end() ?>
      </div>
      <div class="clearfix"></div>
    </div>
</div>
<?php } 
  else
  { ?>

    <div class="hdr_title">
      <h2><?= $Reg_HeaderText ?></h2>
    </div>
    
    <div class="banner_join_webinar">
    
       <?php  if($meeting->presentation_file != '')
            { ?>
             <img src="/webroot/files/Meetings/presentation_file/<?php echo $meeting->presentation_file; ?>" />
            <?php }
            else
            { ?>
             <img src="/webroot/img/Reg_Logo.png" />
            <?php  }
            ?>

    </div>
    <div class="module form-module">
      <div class='meeting-error'><h2><?php echo (isset($error_msgs)?$error_msgs:'The Meeting is closed.'); ?></h2></div>
    </div> 

 <?php } //else end
  } //if end
  else { ?>

      <div class="banner_join_webinar">
        
        <?php  if($meeting->presentation_file != '')
        { ?>
            <img src="/webroot/files/Meetings/presentation_file/<?php echo $meeting->presentation_file; ?>" />
        <?php }
        else
        { ?>
            <img src="/webroot/img/Reg_Logo.png" />
        <?php  }
        ?>
      </div>

      <div class="module form-module">
        <div class="meeting-error">
          <h2><?php echo (isset($error_msgs)?$error_msgs:'Incorrect Meeting URL'); ?></h2>
        </div>
      </div>
 <?php } ?>

    
<script type="text/javascript">
     $('#attedee').bValidator();
     
</script>
 
