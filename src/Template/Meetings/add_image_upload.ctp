<?php //echo "<pre>";print_r($meeting); 

 if($meeting->Reg_HeaderText != '') {
                                        $Reg_HeaderText =html_entity_decode($meeting->Reg_HeaderText);
                                      }
                                      else
                                      {
                                        $Reg_HeaderText ='';
                                      }
        if($meeting->joinText != '') {
                                        $joinText =$meeting->joinText;
                                      }
                                      else
                                      {
                                        $joinText ='Login by entering your information below';
                                      }
        if($meeting->joinButtonText != '') {
                                        $joinButtonText =$meeting->joinButtonText;
                                      }
                                      else
                                      {
                                        $joinButtonText ='Join Meeting';
                                      }  
         if($meeting->attendeeName != '') {
                                        $attendeeName =$meeting->attendeeName;
                                      }
                                      else
                                      {
                                        $attendeeName ='Name';
                                      }
        if($meeting->attendeeEmail != '') {
                                        $attendeeEmail =$meeting->attendeeEmail;
                                      }
                                      else
                                      {
                                        $attendeeEmail ='Email';
                                      }                                   

                                      ?>             
 


    <?php echo $this->element('inner_menu'); ?>
    <?php echo $this->element('center_menu'); ?>
    <p><strong>Image</strong> (Recommended size : 600x400)</p>
                  <?= $this->Form->create($meeting,['type'=>'file','id'=>'meeting']) ?>
               <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            
                            <div class="col-sm-4">
                                <label>Select Image For Meeting</label>
                               <?= $this->Form->input('presentation_file', ['type' => 'file','label'=>false,'id'=>'imgInp','required'=>'false','data-bvalidator'=>'extension[jpg:png]', 'data-bvalidator-msg'=>'Please select file of type .jpg, .png']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <div>
                            <label>Image Preview</label>
                        <?php if($meeting->presentation_file != '') { ?>
                             <img id="blah" src="/webroot/files/Meetings/presentation_file/<?= $meeting->presentation_file; ?>" alt="Image Preview" class="image_preivew" />
                          <?php } 
                          else {
                            ?> 
                        <img id="blah" src="/webroot/img/Reg_Logo.png" alt="Image Preview" class="image_preivew" />
                            <?php } ?>
                        </div>
                    </div>
                </div>
          
<!-- 
                    <div class="form-group">
                        <div class="checkbox checkbox-default">
                            <input class="styled" value="password" type="checkbox"  <?php if($meeting->password != '') { ?>checked="checked" <?php } ?>>
                            <label for="checkbox2">Make Meeting Private</label>
                        </div>
                    </div>
                    
                    <div class="form-group password_hide password_show">
                        <label>Password</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" id="password"  name="password" value="<?php echo $meeting->password ?>">
                            </div>
                        </div>
                    </div> -->
                     <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Header</label>
                               <?= $this->Form->input('Reg_HeaderText',['label'=>false,'type'=>'textarea','class'=>'form-control','id'=>'editor1','value'=>$Reg_HeaderText]) ?>
                                </div>
                        </div>                    
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">join Text</label>
                               <?= $this->Form->input('joinText',['label'=>false,'type'=>'text','class'=>'form-control','id'=>'joinText','value'=>$joinText]) ?>
                                </div>
                        </div>                    
                    </div>

                     <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">join Button</label>
                               <?= $this->Form->input('joinButtonText',['label'=>false,'type'=>'text','class'=>'form-control','id'=>'joinButton','value'=>$joinButtonText]) ?>
                                </div>
                        </div>                    
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Form Fields</label>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">Field Name</div>
                                  <!--   <div class="col-sm-6 col-xs-6">Required</div> -->
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                    <?= $this->Form->input('attendeeName',['label'=>false,'type'=>'text','placeholder'=>'Name','class'=>'form-control','data-bvalidator'=>'required','value'=>$attendeeName]) ?>
                                    </div>
                                 <!--    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox" checked="checked">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div> -->
                                </div>
                                </br>
                              <!--  <div class="row">
                                    <div class="col-sm-6"><input class="form-control" placeholder="Form" type="text"> </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                       <?= $this->Form->input('attendeeEmail',['label'=>false,'type'=>'text','class'=>'form-control','placeholder'=>'Email','data-bvalidator'=>'required','value'=>$attendeeEmail]) ?>  
                                   </div>
                                    <!-- <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox" checked="checked">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div> -->
                                </div>
                                 <?php if($meeting->password != '') { ?>
                                 </br>
                                <div class="row">
                                    <div class="form-group password_hide password_show">
                                        <div class="col-sm-6 col-xs-12">
                                            <?= $this->Form->input('attendeePassword',['label'=>false,'type'=>'text','class'=>'form-control','placeholder'=>'Password','data-bvalidator'=>'required']) ?> 
                                        </div>
                                    </div>
                                </div> 
                                <?php } ?>
                               <div class="row m-t-b-5">
                                    <div class="col-sm-4">
                                      <!--   <button class="btn btn-primary">Add Field</button>-->
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4 mob-top-5">
                                          <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                                    </div>
                                    <div class="col-sm-4 mob-top-5">
                               <?php 
                                    echo $this->Html->link(
                                    'Preview Registration Page',
                                    '/meetings/PreviewJoin/'.$meeting->name,
                                    ['class' => 'btn btn-default btn-block', 'target' => '_blank']
                                    );?>
                                      
                                    </div>
                                       <div class="col-sm-4 mob-top-5">
                                        <?= $this->Form->button('Start Now',['type'=>'submit','name'=>'start','class'=>'btn btn-success btn-block']) ?>
                                         
                                      </div>
                                </div>
                            </div>
                        </div>                    
                    </div>
             
              <?= $this->Form->end() ?>
              
            </div>
            
            
        </div>
    </section>


    <script type="text/javascript">
    $('#meeting').bValidator();
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

            $("#imgInp").change(function(){
                readURL(this);
            });

        $(".date-picker").datepicker({dateFormat: 'yyyy-mm-dd'}
           
            );
        $('#timepicker').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23
        });
        password=$("#password").val();
       
        if(password != '')
        {
              
              $(".password_show").toggle();
              $("#attendeePassword").val('');
        }

         $('input[type="checkbox"]').click(function(){
            if($(this).attr("value")=="password"){
                $(".password_show").toggle();
            }      
        });

        var editor = CKEDITOR.replace( 'editor1', {
            filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
            $( "#reset" ).click(function() {
                $('.form-control').val('');
                $("textarea#editor1").val('');
            });
    </script>