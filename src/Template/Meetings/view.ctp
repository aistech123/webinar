    <nav class="large-3 medium-4 columns" id="actions-sidebar">
        <ul class="side-nav">
            <li class="heading"><?= __('Actions') ?></li>
            <li><?= $this->Html->link(__('Edit Meeting'), ['action' => 'edit', $meeting->id]) ?> </li>
            <li><?= $this->Form->postLink(__('Delete Meeting'), ['action' => 'delete', $meeting->id], ['confirm' => __('Are you sure you want to delete # {0}?', $meeting->id)]) ?> </li>
            <li><?= $this->Html->link(__('List Meetings'), ['action' => 'index']) ?> </li>
            <li><?= $this->Html->link(__('New Meeting'), ['action' => 'add']) ?> </li>
        </ul>
    </nav>
    <div class="meetings view large-9 medium-8 columns content">
        <h3><?= h($meeting->name) ?></h3>
        <table class="vertical-table">
            <tr>
                <th><?= __('Name') ?></th>
                <td><?= h($meeting->name) ?></td>
            </tr>
            <tr>
                <th><?= __('Password') ?></th>
                <td><?= h($meeting->password) ?></td>
            </tr>
            <tr>
                <th><?= __('Presentation File') ?></th>
                <td><?= h($meeting->presentation_file) ?></td>
            </tr>
            <tr>
                <th><?= __('Id') ?></th>
                <td><?= $this->Number->format($meeting->id) ?></td>
            </tr>
            <tr>
                <th><?= __('Meeting Duration') ?></th>
                <td><?= $this->Number->format($meeting->meeting_duration) ?></td>
            </tr>
            <tr>
                <th><?= __('Meeting Time') ?></th>
                <td><?= h($meeting->meeting_time) ?></td>
            </tr>
            <tr>
                <th><?= __('Created At') ?></th>
                <td><?= h($meeting->created_at) ?></td>
            </tr>
            <tr>
                <th><?= __('Updated At') ?></th>
                <td><?= h($meeting->updated_at) ?></td>
            </tr>
            <tr>
                <th><?= __('Deleted') ?></th>
                <td><?= $meeting->deleted ? __('Yes') : __('No'); ?></td>
            </tr>
        </table>
        <div class="row">
            <h4><?= __('Topic') ?></h4>
            <?= $this->Text->autoParagraph(h($meeting->topic)); ?>
        </div>
    </div>
