    <section>
        <div class="breadcrumb">
            <div class="container">
                <ul>
                    <li>
                        <a href="#">
                            <i class="fa fa-arrow-circle-left"></i>
                            Go Back To Account Manager
                        </a>
                   </li>
                   <li>
                        <a href="#">
                            <i class="fa fa-arrow-circle-left"></i>
                            Brand this meeting room
                        </a>
                   </li>
                </ul>
            </div>
        </div>
        
        <div class="container">
            <div class="account_manager_title">       
                <h4>Your Account Manager</h4>
            </div>
            <div class="page_title">
                <div class="text-center">
                    <h2><span>Edit Meeting Room Information (Advanced Options)</span></h2>
                </div>
            </div>    
            
            <div class="message_box">
    			<div class="message_close"><a href="#"><i class="fa fa-times-circle"></i></a></div>
            	<p>Use this screen to make changes to your virtual 'Meeting Room'. You can change the Title, security settings, Exit URL and more.</p>
            </div>    
        </div>
    </section>
    <section class="main_content">
    	<div class="container">
        	<div class="box_design">
            	<?= $this->Form->create($meeting) ?>
                	<div class="form-group">
                        <?= $this->Form->label('meetingroom','Meeting Room') ?>
                        <div class="row">
                        	<div class="col-sm-10">
                            	<?= $this->Form->input('name',['label'=>false,'class'=>'form-control','placeholder'=>'An Olympic Idea']) ?>
                            </div>
                            <div class="col-sm-2">
                            	<label class="bottom_text">(100 character max)</label>
                            </div>
                        </div>                    
                    </div>
                    <div class="form-group">
                        <?= $this->Form->label('topic','Topic') ?>
                        <div class="row">
                        	<div class="col-sm-10">
                                <?= $this->Form->input('topic',['label'=>false,'type'=>'textarea','class'=>'form-control','rows'=>2]) ?>
                                <i>This is for your personal reference.</i>
                            </div>
                            <div class="col-sm-2">
                            	<label class="bottom_text">(100 character max)</label>
                            </div>
                        </div>                    
                    </div>

            </div>
            
            <div class="box_design">

                    <div class="form-group">
                        <label>Meeting Room</label>
                        <div class="row">
                        	<div class="col-sm-12">
                        	    <div>
                                    <?= $this->Form->radio('is_meeting_password_protected',[
                                        ['value' => '0', 'text' => 'No security is needed for this event.']
                                     ]); ?>
                                </div>
                                <div>
                                     <?= $this->Form->radio('is_meeting_password_protected',[
                                         ['value' => '1', 'text' => 'Use a password.']
                                     ]); ?>
                                </div>
                            </div>
                            
                        </div>    
                        <div class="row">
                        	<div class="col-sm-12">
                            	<div class="col-sm-10">
                                    <div class="form-group">
                                        <?= $this->Form->input('password',['label'=>false,'type'=>'text','class'=>'form-control','id'=>'password','readonly'=>'true']) ?>
                                    </div>  
                                </div>  
                                <div class="col-sm-2">
                                	<label class="bottom_text">(16 character max)</label>
                                </div>                        
                            </div>                        
                        </div>   
                    </div>  
                        <div class="col-sm-12">
                            <p>Provide a password if you would like your meeting to be password protected. Everyone trying to log in to your meeting will need to know this password. <br>
    <strong>Note: This password will not be used for meetings that have a registration form.</strong> </p>
                        </div>       
                        <div class="clearfix"></div>

            </div>
            
            <div class="box_design">

                    <div class="form-group">
                        <?= $this->Form->label('meetingexiturl','Meeting Exit url') ?>
                        <label>Meeting Exit url</label>                        
                        <div class="row">
                        	<div class="col-sm-12">
                            	<div class="col-sm-10">
                                    <div class="form-group">
                                        <?= $this->Form->input('meeting_exit_url',['label'=>false,'class'=>'form-control']) ?>
                                    </div>  
                                </div>  
                                <div class="col-sm-2">
                                	<label class="bottom_text">(255 character max)</label>
                                </div>                        
                            </div>                        
                        </div>   
                    </div>  
                        <div class="col-sm-12">
                            <p>Provide a URL (web address) where to send attendees at the end of the meeting. Please include "http://" such as "<a href="#">http://www.yahoo.com</a>" Leave blank if you would not like to forward your attendees any address at the end of the meeting. </p>
                        </div>       
                        <div class="clearfix"></div>

            </div>
            
            <div class="box_design">

                    <div class="form-group">
                        <label>Meeting Room is</label>                        
                        <div class="row">
                        	<div class="col-sm-12">
                            	<div class="col-sm-10">
                                    <div>
                                        <?= $this->Form->checkbox('is_meeting_room_active', ['value' => '1']) ?>
                                        <label for="checkbox2">
                                            Primary
                                        </label>
                                    </div>  
                                </div>                                                      
                            </div>                        
                        </div>   
                    </div>  
                        <div class="message_box">
                            <p><strong><u>Active:</u></strong> Un-check this box to hide the meeting room from the Account Manager. The meeting will become in-active. You restore it by checking "Show Inactive Meetings" from the account manager window. </p>
                        </div>       
                        <div class="clearfix"></div>

            </div>
            
            <div class="row">
            	<div class="col-sm-12">
                	<?= $this->Form->button('Update',['type'=>'submit','class'=>'btn btn-primary']) ?>
                    <button class="btn btn-danger">Cancel</button>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>
    </section>
    <script>
        $(document).ready(function(){
            var password = $('input:radio[name="is_meeting_password_protected"]:checked').val();
            if (password == '1') {
                $("#password").removeAttr('readonly');
            }

        });

        $('input:radio[name="is_meeting_password_protected"]').change(
                           function(){
                               if ($(this).is(':checked') && $(this).val() == '1') {
                                    $("#password").removeAttr('readonly');

                               }
                                else if ($(this).is(':checked') && $(this).val() == '0') {
                                      $("#password").attr('readonly','true');
                                      $("#password").val('');
                               }else{}
                           });

    </script>