   <div class="tab-pane" id="attendees">
    	<div class="col-sm-12">
			<div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th><a href="javascript:sort('username');" ><img src="" />Name</a></th>
                        <th><a href="javascript:sort('email');" >Email</a></th>
                        <th><a href="javascript:sort('created_at');" >Login Time</a></th>
                        <th><a href="javascript:sort('exit_time');" >Exit Time</a></th>
                        <th><a href="javascript:sort('duration');" >Attendance(Minutes)</a></th>
                    </tr>
                </thead>
            <tbody>
             <?php foreach ($attendeestab as $row) {  ?>
            <tr>
                <td>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">
                <?= $row->username ?></td>
              <!--  <td><?= $row->timezone ?></td>-->
                <td><?= $row->email; ?></td>
                <td><?= $row->created_at ?></td>
                <td><?= $row->exit_time ?></td>
                <td><?= $row->minute; ?> Minute</td>
                
            </tr>
             <?php } ?>  
            </tbody>
            </table>
        </div>
			</div>
	</div>
    </div>