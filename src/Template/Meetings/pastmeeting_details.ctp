  <?= $this->Html->script('https://code.jquery.com/jquery-1.12.3.js'); ?>
  <?= $this->Html->script('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') ?> 
  <?= $this->Html->script('https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js') 
  ?>
 <!--  <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css'); ?> -->
    <?= $this->Html->css('https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'); ?>
    <?php echo $this->element('pastMeeting_menu'); ?>
      <div class="tab-content">
        <div class="tab-pane active" id="overview">
          <div class="col-sm-12">
    <?php foreach ($meeting as $meeting) { 
      //echo "<pre>";print_r($meeting);
      date_default_timezone_set($meeting->timezone);
     // echo "<pre>";print_r($this->request->params['pass']['0']); exit;
       ?>

        <div class="row">
            <div class="col-sm-12">
                <label>Date:</label><?= date(' D, M d Y',strtotime(($meeting->created_at))); ?> <br>
                <label>Start Time:</label> <?=  date('h:i a T',strtotime($meeting->created_at)); ?><br>
                <label>Duration:</label> <?= $meeting->durationOfMeeting; ?> minutes
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
               <!-- <div id="chartContainer" style="height:500px;">-->

                <div id="container" style="height: 400px; min-width: 310px">
                </div>
            </div>
        </div>
    <?php } ?>
      </div>
   </div>

   <!--  <div class="tab-pane" id="chat_history">
     <?php 
     /* if(count($chat_array) > 0)
       {
          foreach ($chat_array as $row) {   
           echo $row['name'] ." : ".$row['message']."</br>"; 
          }
       }
       else
       {
        echo "<h2>Chat History Not Found.</h2>";
       }*/
      ?>
    </div> -->
    <div class="tab-pane" id="recordings">
    	<div class="col-sm-12">
      <?php if(count($Recording_array) > 0) { ?>
            <div class="row m-t-b-5">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>URL</label>
                        <p><?= $Recording_array['url']; ?></p>
                    </div>
                </div>
            </div>
            <div class="row m-t-b-5">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Duration:</label>
                        <p><?= $Recording_array['size']; ?> minutes</p>
                    </div>
                </div>
            </div>
        <!--     <div class="row m-t-b-5">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label>Size:</label>
                        <p>42MB</p>
                    </div>
                </div>
            </div> -->
            <div class="row m-t-b-5">
                <div class="col-sm-12">
                    <button class="btn btn-primary">Download</button>
                    <?= $this->Html->link('<button class="btn btn-danger">Delete</button>',
                         array(                               
                             'action' => 'DeleteRecording', $Recording_array['recordId'], $Recording_array['Id'], $Recording_array['meeting_date']
                                ),
                            array(
                                'escape' => false,
                                'confirm' => __('Are you sure you want to delete # {0}?', $Recording_array['recordId']),
                            )
                           ); 
                    ?>
                 
                </div>
            </div>
            <?php } else { echo "<h2>Recording Not Found.</h2>"; } ?>
        </div>
    </div>
    <div class="tab-pane" id="attendees">
    	<div class="col-sm-12">
			<div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Login Time</th>
                        <th>Exit Time</th>
                        <th>Attendance(Minutes)</th>
                    </tr>
                </thead>
            <tbody>
             <?php foreach ($attendeestab as $row) {  ?>
            <tr>
                <td>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#myModal">
                <?= $row->username ?></td>
              <!--  <td><?= $row->timezone ?></td>-->
                <td><?= $row->email; ?></td>
                <td><?= $row->created_at ?></td>
                <td><?= $row->exit_time ?></td>
                <td><?= $row->minute ?> </td>
                
            </tr>
             <?php } ?>  
          </tbody>
          </table>
        </div>
			</div>
	</div>
    </div>
    </div>
    <!-- </div>-->
    </div>
    </section>

    
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Attendance Details</h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label></label>
                <input type="text" class="form-control" placeholder="Tiger Nixon">
            </div>
            <div class="form-group">
                <label>From</label>
                <input type="text" class="form-control" placeholder="UK">
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" placeholder="demo@gmail.com">
            </div>
            <div class="form-group">
                <label>Other Options</label>
                <textarea class="form-control" row="5" placeholder="Other Options"></textarea>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>


 <script type="text/javascript">
   $(document).ready(function() {
      $('#example').DataTable();
    } );
   $(function () {
   // $.getJSON('', function (data) {
      var result=<?php echo json_encode($data_points, JSON_NUMERIC_CHECK); ?>;
        // Create the chart
        // alert(result); 
        $('#container').highcharts('StockChart', {
            chart:{},
            rangeSelector: {
                  selected: 1,
                  inputEnabled: false,
                  buttonTheme: {
                      visibility: 'hidden'
                  },
                  labelStyle: {
                      visibility: 'hidden'
                  },
                   labels: {
                      enabled: true,
                      formatter: function() { return result[this.value][0];},
                  }
              },

            title: {
                text: 'Attendance Graph'
            },
            
            yAxis:{
              opposite:false,
               title: {
                      text: 'No Of attendees',
                    },
            },
             legend: {
                        enabled: false
                    },
             xAxis: {
                  tickInterval: 1,
                 range:{function() { return result[this.value][0];}},
                  labels: {
                      enabled: true,
                      formatter: function() { return result[this.value][0];},
                  },
                  title: {
                      text: 'Minutes',
                    },
              },
      
            credits: {
              enabled: false
            },
            navigator: {
              enabled: true,
              xAxis: {
                         labels: {
                         enabled: true,
                         formatter: function() {
                          return result[this.value][0];},
                     },
                      events: {
                       setExtremes: function (e) {
                           if (e.trigger === "navigator") {
                               var c = this;
                               setTimeout(function() {
                                   forceRebuildSeries(); //Get all data points

                                   // Set Extremes (redisplay with new data points)
                                   c.chart.xAxis[0].setExtremes(e.min, e.max);  

                               }, 1);
                           }
                       }
                   }
            
                 },
                 series: {
                          id: 'navigator',
                  },
            },
            series: [{
                name: 'Total Attendees',
                data: result,
                marker: {
                    enabled: true,
                    radius: 3
                },
                allowDecimals:false,
                shadow: true,
                tooltip: {
                   // valueDecimals: 0
                },
                 showInLegend:false,
            }]
        });
   //});
});
 </script>
<?= $this->Html->script('highstock.js') ?>
     



