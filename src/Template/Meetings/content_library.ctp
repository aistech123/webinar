    <?php echo $this->element('inner_menu'); ?>
    <?php echo $this->element('center_menu'); ?>
    <section>
      <div class="">
        <div class="page_title">
          <div class="text-center">
            <h1><span><img src="/img/webinar-files.png" style="margin-top:-10px;"> Content Library</span></h1>
          </div>
        </div>
        <div class="upload_btn">
          <div class="upload_btn">

        <?php echo $this->Form->create($librarie, ['type' => 'file','id'=>'upload-form']);

             echo $this->Form->input('file', ['type' => 'file','label'=>false,'id'=>'browse','required'=>'false','class'=>'btn btn-primary btn-block','onchange'=>'Handlechange();','style'=>'display: none']); ?>
         
        <input id="fakeBrowse" class="btn btn-primary btn-block" type="button" onclick="HandleBrowseClick();" value="Click Here to Upload Additional Files">
        </div>
        </div>
        <?= $this->Form->input('meeting_id',['type'=>'hidden','value'=>$meeting->id,'id'=>'meeting_id']) ?>  

        <?php echo $this->Form->end(); ?>      
     
      <div id="progress-wrp" style="width: 100%;" class="progress progress-striped active">
        <div class="progress-bar" ></div>
        <div class="status process_status">0%</div>
      </div>

      <div id="output"></div>
      </div>
      </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
   <!--  <script src="upload.js"></script>
 -->
  <section class="main_content">
    <div class="content-library">
      <div class="">
        <div class="table-responsive">
          
          <table class="table-bordered" cellpadding="0" cellspacing="0" border="0" width="100%">
            <tbody><tr>
              <th>file name</th>
              <th>file size</th>
              <th>Upload Date</th>
              <th style="text-align:center;">preview</th>
              <th style="text-align:center;">delete</th>
            </tr>
            <?php foreach ($librarieData as $data) :?>
            <tr>
              <td><?php echo $data->file; ?></td>
              <td><?php echo byte_convert($data->size); ?>YTES</td>
              <td><?php echo $final=date('D, F d Y  ',strtotime(($data->created_at))); ?></td>
              <td align="center"><a href="/files/library/<?php echo $data->file ;?>" target="_blank"><i class="fa fa-search"></i></a></td>
              <td align="center">
                <?= $this->Html->link('<span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-close fa-stack-1x fa-inverse"></i>
                </span>',
                array(
                'action' => 'DeleteLibrary',$meeting->id,$data->id,
                ),
                array(
                'escape' => false,
                'confirm' => __('Are you sure you want to delete ?', $data->id),
                )
                ); ?>
              </td>
              
            </tr>
            <?php endforeach; ?>
          </tbody></table>
        </div>
      </div>
    </div>
    <div class="">
      <div class="pagination_box">
        <div class="row">
          <div class="col-sm-6">
            <div class="numeric_paginaion">
              <ul>
                <?php echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
              </ul>
            </div>
          </div>
          <div class="col-sm-6 text-right">
            <div class="next_previous">
              <ul>
                <?php   echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
                ['escape' => false]); ?>
                <?php   echo $this->Paginator->next('<i class="fa fa-fast-forward"></i>',
                ['escape' => false]); ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </section>
    <script type="text/javascript" >
     
     function HandleBrowseClick()
      {
          var fileinput = document.getElementById("browse");
          fileinput.click();
      }
      /*function formatBytes(bytes,decimals) {
         if(bytes == 0) return '0 Byte';
         var k = 1000;
         var dm = decimals + 1 || 3;
         var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
         var i = Math.floor(Math.log(bytes) / Math.log(k));
         return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + sizes[i];
      }*/

    function Handlechange()
      {
          var fileinput = document.getElementById("browse");
          var textinput = document.getElementById("filename");
          var meeting_id = document.getElementById("meeting_id").value;
        
          var file_data = $('#browse').prop('files')[0];  
          var validExtensions = ['jpg','gif','png','pdf','pptx','ppt']; //array of valid extensions
          var fileName = file_data.name;
          var filesize= file_data.size;
          var form_data = new FormData();   
          var progress_bar_id     = '#progress-wrp'
          var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);

            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, validExtensions) == -1){
               alert("Invalid file type");
               //$("#yourElem").uploadifyCancel(q);
               return false;
            }
            var regexp = /^\S*$/; // a string consisting only of non-whitespaces
          
            if(!regexp.test(fileName))
            {
              alert("Invalid file name.");
              return false;
            } 
       
            if(filesize > '104857600')
            {
              alert("file size must be less than or equal to 25MB.");
              return false;
            }
             $body = $("body");
            
          
          form_data.append('file', file_data);
          form_data.append('meeting_id', meeting_id);
        
                $.ajax({
                  url : 'content-library',
                  type: "POST",
                  data : form_data,
                  contentType: false,
                  cache: false,
                  processData:false,
                  xhr: function(){
                      //upload Progress
                      var xhr = $.ajaxSettings.xhr();
                      if (xhr.upload) {
                          xhr.upload.addEventListener('progress', function(event) {
                              var percent = 0;
                              var position = event.loaded || event.position;
                              var total = event.total;
                              if (event.lengthComputable) {
                                  percent = Math.ceil(position / total * 100);
                              }


                           /*  $(progress_bar_id +" .progress-bar").animate({
                                    width: percent
                                }, 50000 ); */
                              //update progressbar

                              $(progress_bar_id +" .progress-bar").css("width", + percent +"%");

                              $(progress_bar_id + " .status").text(percent +"%");
                              // $(progress_bar_id    //(result_output).html(res); //output response from server
                 // submit_btn.val("Upload").prop( "disabled", false); //enable submit button once ajax is done+" .progress-bar").animate(50000);
                          }, true);
                      }
                      return xhr;
                  },
                  mimeType:"multipart/form-data"
              }).done(function(res){ //
                  $('#upload-form')[0].reset(); //reset form
                  $('#output').html('file successfully uploaded');
                  location.reload();
                 //(result_output).html(res); //output response from server
                 // submit_btn.val("Upload").prop( "disabled", false); //enable submit button once ajax is done
              });
              $("#cancel").click(function(){
               xhr.abort();
               
               var percent = 0;
               $(progress_bar_id +" .progress-bar").css("width", + percent +"%");
               $(progress_bar_id + " .status").text(percent +"%");
               $('#upload-form')[0].reset();
                return false;
               });

         /* var fileinput = document.getElementById("browse");
          var textinput = document.getElementById("filename");
          var meeting_id = document.getElementById("meeting_id").value;
        
          var file_data = $('#browse').prop('files')[0];  
          var validExtensions = ['jpg','gif','png','pdf','pptx','ppt']; //array of valid extensions
          var fileName = file_data.name;
          var filesize= file_data.size;
                    
            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
            if ($.inArray(fileNameExt, validExtensions) == -1){
               alert("Invalid file type");
               //$("#yourElem").uploadifyCancel(q);
               return false;
            }
            var regexp = /^\S*$/; // a string consisting only of non-whitespaces
          
            if(!regexp.test(fileName))
            {
              alert("Invalid file name.");
              return false;
            } 
       
            if(filesize > '104857600')
            {
              alert("file size must be less than or equal to 25MB.");
              return false;
            }
             $body = $("body");


            var form_data = new FormData();                  
            form_data.append('file', file_data);
            form_data.append('meeting_id', meeting_id);

            $.ajax({
                      url: 'content-library', // point to server-side PHP script 
                      dataType: 'text',  // what to expect back from the PHP script, if anything
                      cache: false,
                      contentType: false,
                      processData: false,
                      data: form_data,                         
                      type: 'post',
                      beforeSend: function() { 
                       openModal();
                      },
                 
                      success: function(php_script_response){
                           
                           location.reload();
                           
                          // alert(php_script_response);
                            closeModal();
                      },
                      
                  });
*/
      }

    </script>
 


