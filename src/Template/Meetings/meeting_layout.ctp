  <?php echo $this->element('inner_menu'); ?>
  <?php echo $this->element('center_menu'); ?>

       <p>Choose the Starting Layout of your Meeting Room</p>
        <div class="row">
          <div class="col-sm-6">
            <form name="metting" id="metting" enctype="multipart/form-data" action="/meetings/meetingLayout/<?= $meeting->id ?>" method="post">
           
              
          
              <div class="row">
                <div class="col-sm-6 col-xs-12">
                  <div class="form-group">
                  
                      <?php 

                         $this->Form->templates([
                            'nestingLabel' => '{{input}}<label{{attrs}}>{{text}}</label>',
                            'formGroup' => '{{input}}{{label}}',
                        ]);
                       $options2= array(
                            'bbb.layout.name.defaultlayout' => 'Default Layout',
                            'bbb.layout.name.videochat' => 'Video Chat',
                            'bbb.layout.name.webcamsfocus' => 'Webcam Meeting',
                             'bbb.layout.name.presentfocus' => 'presentation Meeting',
                            'bbb.layout.name.lectureassistant' => 'Lecture Assistant',
                            'bbb.layout.name.lecture' => 'Lecture',
                        );   
                                                
          
                        $selectedsections =$meeting->sections;
                         echo $this->Form->input('sections', array(
                                                'label' => false,
                                                'type' => 'select',
                                                'class'=>'form-control',
                                                //'multiple' => 'checkbox',
                                                'options' => $options2,
                                                 'default' => $selectedsections,
                                                'data-bvalidator'=>'required'
                                              ));
                        ?>
                </div>
              </div>
             </div>
            </div>
          </div>

<div class="row m-t-10">
  <div class="col-sm-3">
    <div class="form-group">
      <label>Welcome Chat Message</label>
      <div class="input select">
          <?= $this->Form->input('default_message',['label'=>false,'type'=>'text','class'=>'form-control','value'=>$default_message]) ?>
       </div>
      </div>
    </div>
  </div>        
<div class="row m-t-10">
 <!--  <div class="col-sm-3">
    <div class="form-group">
      <label>Pre-loaded Presentation</label>
      <div class="input select">
        <select class="form-control" name="default_presentation" >
          <option value="default.pdf" selected="selected">Default</option>
            <?php
            foreach($librarieData as $row){
               // echo "<pre>";print_r($row); 
                 $filename =$row->file;
                 echo "<option value='" . $filename . "'>".$filename."</option>";
              } ?>
          </select>
         </div>
      </div>
    </div> -->
  </div>

        
        <div class="row m-t-10">
		
            <div class="col-sm-3">            
                
                    <div class="form-group">
                        <label>Select Skin</label>

                       <?php    $skinoptions= array(
                            'BBBDefault.css.swf' => 'Default',
                            'BBBGrey.css.swf' => 'Dark Grey',
                            'BBBBlue.css.swf' => 'Blue',
                            'BBBwhite.css.swf' => 'White',
                            ); 
                         $selectedskin =$meeting->skin;
                         echo $this->Form->input('skin', array(
                                                'label' => false,
                                                'type' => 'select',
                                                'class'=>'form-control',
                                                'id'=>'skin',
                                                //'multiple' => 'checkbox',
                                                'options' => $skinoptions,
                                                 'default' => $selectedskin,
                                                'data-bvalidator'=>'required'
                                              ));
                             ?> 
                        
                      </div>
                      
              </div>
              <div class="col-sm-6 pull-right">
                <div class="">
                      <!--<label>Preview Theme</label>-->
                      <img class="image_preivew" alt="Image Preview" id="theme">
                  </div>
              </div>
       
        </div>
		
        
    <hr class="m-t-b-5">
      <div class="row">
                      <div class="col-sm-4 mob-top-5">
                       <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                         
                      </div>
                      <div class="col-sm-4 mob-top-5">

                          <button class="btn btn-default btn-block" id="reset" name="reset">Reset to Default</button>
                      </div>
                      <div class="col-sm-4 mob-top-5">
                         <?= $this->Form->button('Start ',['Now'=>'submit','name'=>'start','class'=>'btn btn-primary btn-block']) ?>
                          
                      </div>
                  </div>
        
        
         </form>        
      </div>
    </div>
    
    
  </section>

  <script>
  $('#metting').bValidator();

  skin=$("#skin").val();
      if(skin == 'BBBDefault.css.swf')
      {
         $('#theme').attr('src','/webroot/img/bbbdefault.png');

      }
      if(skin == 'BBBGrey.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbgrey.png');
      }
      if(skin == 'BBBBlue.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbblue.png');
      }
      if(skin == 'BBBwhite.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbwhite.png');
      }

 $('#skin').on('change', function() {
 
      if(this.value == 'BBBDefault.css.swf')
      {
         $('#theme').attr('src','/webroot/img/bbbdefault.png');

      }
      if(this.value == 'BBBGrey.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbgrey.png');
      }
      if(this.value == 'BBBBlue.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbblue.png');
      }
        if(this.value == 'BBBwhite.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbwhite.png');
      }
});
 
  </script>
