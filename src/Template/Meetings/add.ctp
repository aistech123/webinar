    <?php echo $this->element('inner_menu'); ?>

    <section class="main_content">
        <div class="container">
	    <div class="page_title">
                  <div class="text-center">
                      <h1><span><img src="/img/webinar-add-new.png" style="margin-top:-10px;"> New Meeting</span></h1>
                  </div>
              </div>  
            <div class="box_design">
                <?= $this->Form->create($meeting,array('id'=>'meeting')) ?>
                    <div class="row">                    
                          <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4"><label>Meeting Room URL: http://www.jointhismeeting.com/</label></div>
                                    <div class="col-sm-6">
                                        <?php $custom_class = $this->Form->isFieldError('name')?'form-input-boarder':'' ?>
                                     <?= $this->Form->input('name',['label'=>false,"class"=>"form-control $custom_class",/*'data-bvalidator'=>'required',*/'placeholder'=>'Enter the URL of your new Meeting Room']) ?>
                                       <?php $error = $this->Form->isFieldError('name') ? $this->Form->error('name') : '';?>

                                       
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="bottom_text">(100 character max)</label>
                                    </div>
                                </div>
                            </div>

                        
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4"><label>Meeting Room Name : </label></div>
                                    <div class="col-sm-6">
                                    <?= $this->Form->input('headerText',['label'=>false,'type'=>'text','class'=>'form-control']) ?>
                                    </div>
                                </div>
                            </div>
                      
                            
                            <div class="form-group">
                                <div class="checkbox checkbox-default">
                                    <input type="checkbox" class="styled" value="password">
                                    <label for="checkbox2">Make Meeting Private</label>
                                </div>
                            </div>
                            
                            <div class="form-group password_hide password_show">
							<div class="row">
                                <div class="col-sm-4"><label>Password : </label></div>
							</div>
                                <div class="row">
                                    <div class="col-sm-6">
                                         <?= $this->Form->input('password',['label'=>false,'class'=>'form-control','type'=>'text']) ?>
                                    </div>
                                </div>
                            </div>
                          </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <button class="btn btn-primary btn-block" id="profile">Save Process</button>
                        </div>
                       <!--  <div class="col-sm-4 col-xs-6 pull-right">
                        <?= $this->Form->button('Start Now',['type'=>'submit','id'=>'start_meeting','name'=>'start_meeting','class'=>'btn btn-success btn-block']) ?>
                           
                        </div> -->
                    </div>
                     <?= $this->Form->input('created_by',['type'=>'hidden','value'=>$user_id]) ?>
               <?= $this->Form->end() ?>
            </div>
            
            
        </div>
    </section>

        <script>
    // $('#meeting').bValidator();

     /* var options5 = {
        errorValidateOn: null
    }*/
    
   /* $(document).ready(function () {
        $('#meeting').bValidator(options5);
    });
    
    function checkMeetingName(name){
        //name=$("#name").val();
        var ret = false;
        var regExp = /_-/;
        var matches = regExp.exec(name);
        alert(matches[1]);
        return false;
        $.ajax({    
            type: 'POST',
            url: '/meetings/checkMeetingName',
            async: false,
            data: {'meetingName':name},
            success: function(data){
                if(data == 'ok')
                    ret = true
            }
        });

        return ret;
    }*/
    $(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).attr("value")=="password"){
                $(".password_show").toggle();
            }      
        });

          var editor = CKEDITOR.replace( 'editor1', {
            filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
    });
    </script>

        <script>
       /* $(document).ready(function(){
            $("#start_meeting").click(function(e){
                e.preventDefault();
                window.location.href="http://webinar.aistechnolabs.in/wireframe/new-meeting-room-schedule.html";
            });
        });*/
        
    </script>
