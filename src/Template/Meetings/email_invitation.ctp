 <?php echo $this->element('inner_menu'); ?>
  <?php echo $this->element('center_menu'); ?>
  <div class="section">
<section class="main_content">
<div class="page_title">
        <div class="text-center">
          <h1><span>Coming Soon</span></h1>
        </div>
		 </div>
      </div>
      </section>
      </div>

<!--  
                <p>You can send an Invitation for your Meeting to a maximum of 50 Attendees</p>
               <?= $this->Form->create($meeting,array('id'=>'meeting')) ?>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Email Subject</label>
                                <?= $this->Form->input('email_subject',['label'=>false,'class'=>'form-control','data-bvalidator'=>'required','placeholder'=>'Meeting Invitation: "An Olympic Idea!"']) ?>
                            </div>
                        </div>                    
                    </div>
                    
     
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
    <?= $this->Form->input('email_message',['label'=>false,'type'=>'textarea','class'=>'form-control','id'=>'editor1']) ?>
    </div>
                        </div>                    
                    </div>
                    
                    
                    <p>Please enter the emails of the people you want to 
    invite to your meeting OR use the drop down list to select e-mails from a
     previous meeting. Separate e-mails using a comma "," or enter one 
    e-mail per line.</p>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Copy from previous meeting</label>                        
                                <select class="form-control">
                                    <option selected="selected">Meeting Name/Date(invitation emails)</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                              <?= $this->Form->input('attendees',['label'=>false,'type'=>'textarea','data-bvalidator'=>'required','class'=>'form-control','placeholder'=>'Attendees','rows'=>5]) ?>
                                    
                            </div>
                        </div>                    
                    </div>
                    
                     <div class="row">
                        <div class="col-sm-12">
                         <?= $this->Form->button('Send Now',['type'=>'submit','name'=>'sendmail','class'=>'btn btn-primary btn-block btn-lg']) ?> 
                           
                        </div>
                     </div>
                    <hr class="m-t-b-5">
                    
                  
                    <div class="row">
                        <div class="col-sm-4">
                         <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                           
                        </div>
                        <div class="col-sm-4">

                            <button class="btn btn-default btn-block" id="reset" name="reset">Reset to Default</button>
                        </div>
                        <div class="col-sm-4">
                           <?= $this->Form->button('Start ',['Now'=>'submit','name'=>'start','class'=>'btn btn-primary btn-block']) ?>
                            
                        </div>
                    </div><?= $this->Form->end() ?>
            </div>
            
            
        </div>
    </section>

    <script type="text/javascript">
    $('#meeting').bValidator();
        $(".date-picker").datepicker({dateFormat: 'yyyy-mm-dd'}
           
            );
        $('#timepicker').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23
        });

        var editor = CKEDITOR.replace( 'editor1', {
        	filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
        	filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
        	filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
        	filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        	filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        	filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
    $( "#reset" ).click(function() {
        $('.form-control').val('');
        $("textarea#editor1").val('');
    });
    </script>

 -->