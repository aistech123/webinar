    <?php echo $this->element('inner_menu'); ?>
    <?php echo $this->element('center_menu'); ?>

    <section class="main_content">
        
            <div class="box_design">
                <?= $this->Form->create($meeting,array('id'=>'meeting')) ?>
                    <div class="row">                    
                          <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4"><label>Meeting Room URL: http://www.jointhismeeting.com/</label></div>
                                    <div class="col-sm-6">
                                        <?php $custom_class = $this->Form->isFieldError('name')?'form-input-boarder':'' ?>
                                     <?= $this->Form->input('name',['label'=>false,"class"=>"form-control $custom_class",/*'data-bvalidator'=>'required',*/'placeholder'=>'Enter the URL of your new Meeting Room']) ?>
                                       <?php $error = $this->Form->isFieldError('name') ? $this->Form->error('name') : '';?>

                                       
                                    </div>
                                    <div class="col-sm-2">
                                        <label class="bottom_text">(100 character max)</label>
                                    </div>
                                </div>
                            </div>

                        
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-4"><label>Meeting Room Name : </label></div>
                                    <div class="col-sm-6">
                                    <?php 
                                  //  print_r($meeting);
                                     if($meeting->headerText != '') {
                                        $headerText =html_entity_decode($meeting->headerText);
                                      }
                                      else
                                      {
                                        $headerText ='';
                                      }
                                    ?>
                                        <?= $this->Form->input('headerText',['label'=>false,'type'=>'text','class'=>'form-control','value'=>$headerText]) ?>
                                    </div>
                                </div>
                            </div>
                            
                     <div class="form-group">
                            <div class="checkbox checkbox-default">
                                <input type="checkbox" id="passcheck" class="styled" value="password" >
                                <label for="checkbox2">Make Meeting Private</label>
                            </div>
                        </div>
                            
                            <div class="form-group password_hide password_show">
							<div class="row">							
                               <div class="col-sm-4"><label>Password : </label></div>
							</div>
                                <div class="row">
                                    <div class="col-sm-6">
                                         <?= $this->Form->input('password',['label'=>false,'class'=>'form-control','type'=>'text','id'=>'password']) ?>
                                    </div>
                                </div>
                            </div> 
                           
                          </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                            <button class="btn btn-primary btn-block" id="profile">Save Process</button>
                        </div>
                       <!--  <div class="col-sm-4 col-xs-6 pull-right">
                         <?= $this->Form->button('Start Now',['type'=>'submit','name'=>'start','class'=>'btn btn-success btn-block']) ?>
                           
                        </div> -->
                    </div>
                     <?= $this->Form->input('created_by',['type'=>'hidden','value'=>$user_id]) ?>
               <?= $this->Form->end() ?>
            </div>
               
    </section>
</div>
</div>
<script>
 /*     var options5 = {
        errorValidateOn: null
    }
    
    $(document).ready(function () {
        $('#meeting').bValidator(options5);
    });
    
    function checkMeetingName(name){
        id=$("#id").val();
        var ret = false;
        if (name.match(/^[a-zA-Z\\-\\_]/g)) {
          $.ajax({
            type: 'POST',
            url: '/meetings/EditcheckMeetingName',
            async: false,
            data: {'meetingName':name,'id':id},
            success: function(data){
                if(data == 'ok')
                ret = true
            }
        });

        return ret;
             
           }
      else
      {

            alert(name);
            return false;
      }  
       
    
    }*/
    $(document).ready(function(){
       /* $('input[type="checkbox"]').click(function(){
            if($(this).attr("value")=="password"){
                $(".password_show").toggle();
            }      
        });*/
 

     password=$("#password").val();
       
        if(password != '')
        {
               $("#passcheck").prop("checked", true);
              $(".password_show").toggle();
        }
         $('input[type="checkbox"]').click(function(){
            if($(this).attr("value")=="password"){
                $(".password_show").toggle();
            } 

          if($("#passcheck"). prop("checked") == false)
          {
             $("#password").val('');
          }  

        });    
    });
    </script>

        <script>
       /* $(document).ready(function(){
            $("#start_meeting").click(function(e){
                e.preventDefault();
                window.location.href="http://webinar.aistechnolabs.in/wireframe/new-meeting-room-schedule.html";
            });

      

        });*/
        
    </script>