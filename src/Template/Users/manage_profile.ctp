<section>  
  <div class="container">
    <div class="page_title">
      <div class="text-center">
        <h1><span>Profile</span></h1>
      </div>
    </div>
  </div>
</section>
<section class="main_content">
    <div class="container">
        <div class="box_design">
           <?= $this->Form->create($user) ?>
                <div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Display Name</label>
                             <?= $this->Form->input('username',['label'=>false,'class'=>'form-control']) ?>
                           
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Time Zone</label>
                          <?= $this->Form->select('timezone',$timezoneList,['label'=>false,'class'=>'form-control']) ?>
                        </div>                        
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Max Attendees</label>
                            <?= $this->Form->input('max_attendees',['disabled'=>'disabled','label'=>false,'type'=>'text','class'=>'form-control','placeholder'=>'Enter Max Attendees For Meeting Room Default(20)']) ?>
                        </div>                        
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                     <?= $this->Form->button('Update',['type'=>'submit', 'id'=>'profile','class'=>'btn btn-primary']) ?>
                        <?php //echo $this->Form->button(__('Cancel'), array('name' => 'cancel','class'=>'btn btn-danger')); ?>
                        
                    </div>
                </div>
             <?= $this->Form->end() ?>
        </div>
        
        
    </div>
</section>
