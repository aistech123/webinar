<section>
    <div class="breadcrumb">
        <div class="container">
            <ul>
                <li>
                    <a href="#">
                        <i class="fa fa-arrow-circle-left"></i>
                        Go Back To Account Manager
                    </a>
               </li>
               <li>
                    <a href="#">
                        <i class="fa fa-arrow-circle-left"></i>
                        Upload Your Photo
                    </a>
               </li>
               <li>
                    <a href="#">
                        <i class="fa fa-arrow-circle-left"></i>
                        My Webinar Hub
                    </a>
               </li>
            </ul>
        </div>
    </div>
    
    <div class="container">
        <div class="account_manager_title">       
            <h4>Your Account Manager</h4>
        </div>
        <div class="page_title">
            <div class="text-center">
                <h2><span>Your Profile</span></h2>
            </div>
        </div>    
        
        <div class="message_box">
			<div class="message_close"><a href="#"><i class="fa fa-times-circle"></i></a></div>
        	<p>Your Profile Information is listed below. If you need to make any updates to the information displayed, please click on the Edit button beside the appropriate section.</p>
        </div>    
    </div>
</section>
<section class="main_content">
	<div class="container">
    	<div class="box_design">
        	<div class="row">
            	<div class="col-sm-6">
                	<h4 class="m-b-10">Contact Information</h4>
                </div>
                <div class="col-sm-6 text-right">
                	<?= $this->Html->link(__('Edit'), ['controller'=>'Users','action' => 'manageProfile',$user->id]) ?>
                </div>
            </div>
            <div class="row">
            	<hr class="blue_line">
            </div>
        	<form>
            	<div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Meeting Display Name:</label><br>
                            <label class="grey_color"><?= $user->username ?></label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Contact Address</label><br>
                            <label class="grey_color"><?= $user->address ?></label>
                        </div>                
                   </div>
                </div>
                <div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Name:</label><br>
                            <label class="grey_color"><?= $user->firstname." ".$user->lastname ?></label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>E-mail Address:</label><br>
                            <label class="grey_color"><?= $user->email ?></label>
                        </div>                
                   </div>
                </div>
                <div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Country:</label><br>
                            <label class="grey_color"><?= $user->Countries['name'] ?></label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Phone:</label><br>
                            <label class="grey_color"><?= $user->phone ?></label>
                        </div>                
                   </div>
                </div>
                <div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Time Zone:</label><br>
                            <label class="grey_color"><?= $user->timezone ?></label>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Fax:</label><br>
                            <label class="grey_color"><?= $user->fax ?></label>
                        </div>                
                   </div>
                </div>
                <div class="row">                    
                      <div class="col-sm-6">
                        <div class="form-group innerbox_design">
                            <label>Password:</label><br>
                            <label class="grey_color"><?= $user->password ?></label>
                        </div>
                      </div>                      
                </div>  
            </form>
        </div>
        
        
    </div>
</section>