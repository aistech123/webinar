<div class="logo">
  <h1>Logo</h1>
</div>
<!-- Form Module-->
<div class="module form-module">
<div class="login-header"><h2>User Sign up</h2></div> 
<strong style="color:red;text-align: center;"><?= $this->Flash->render() ?></strong>
 <div class="form"> 	   
      <?= $this->Form->create() ?>
    	 <div class="form-group row">
            <?= $this->Form->label('Firstname:','Firstname:',['class'=>'col-sm-3 form-control-label']); ?>
              <div class="col-sm-9">
                <?= $this->Form->input('firstname',['type'=>'text','label'=>false,'placeholder'=>'Firstname','class'=>'form-control']); ?>
              </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Lastname:','Lastname:',['class'=>'col-sm-3 form-control-label']); ?>
              <div class="col-sm-9">
                <?= $this->Form->input('lastname',['type'=>'text','label'=>false,'placeholder'=>'Lastname','class'=>'form-control']); ?>
              </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Email:','Email:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('email',['type'=>'email','label'=>false,'placeholder'=>'Email','class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Username:','Username:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('username',['type'=>'text','label'=>false,'placeholder'=>'Username','class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Password:','Password:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('password',['type'=>'password','label'=>false,'placeholder'=>'Password','class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Phone:','Phone:',['class'=>'col-sm-3 form-control-label']); ?>
             <div class="col-sm-9">
                <?= $this->Form->input('phone',['type'=>'text','label'=>false,'placeholder'=>'Phone','class'=>'form-control']); ?>
             </div>
        </div>
        <div class="form-group row">
              <label class="col-sm-3 form-control-label">Country:</label>
              <div class="col-sm-9">
                    <?= $this->Form->select('country',$countries,['class'=>'form-control']); ?>
              </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-3">&nbsp;</label>
            <div class="col-sm-9">
                <?= $this->Form->button('Sign Up',['type'=>'submit','class'=>'btn btn-default']) ?>
            </div>
            <label class="col-sm-3">&nbsp;</label>
        </div>
        <?= $this->Form->input('role',array("type"=>"hidden","value"=>"member")) ?>
    <?= $this->Form->end() ?>
  </div>
<!-- <div class="login-footer">
	<div class="row">
	<div class="col-sm-6">
    	<div class="checkbox-btn">
                    <input type="checkbox" value="value-1" id="inlineCheckbox1" name="rc3" />
                    <label for="rc3" onclick>Terms of Conditions</label>
                </div>
    </div>
    <div class="col-sm-6 text-right">
    	<div class="checkbox-btn">
            <input type="checkbox" value="value-1" id="inlineCheckbox1" name="rc3" />
            <label for="rc3" onclick>Privacy Policy</label>
        </div>
    </div>
    </div>
</div> --> 
</div>