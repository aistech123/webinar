    <div class="logo">
      <h1>Logo</h1>
    </div>
    <!-- Form Module-->
    <div class="module form-module">
    <div class="login-header"><h2>Reset Password</h2></div>
    <?= $this->Flash->render() ?>
     <div class="form">
       <?= $this->Form->create() ?>
        
        	<div class="form-group row">
                <?= $this->Form->label('Password','New Password:',['class'=>'col-sm-3 form-control-label']); ?>
                <div class="col-sm-9">
                    <?= $this->Form->input('password',['type'=>'password','label'=>false,'placeholder'=>'New Password','class'=>'form-control']); ?>
                </div>
            </div>
            <div class="form-group row">
                <?= $this->Form->label('Password','Re-enter Password:',['class'=>'col-sm-3 form-control-label']); ?>
                <div class="col-sm-9">
                    <?= $this->Form->input('confirm_password',['type'=>'password','label'=>false,'placeholder'=>'Re-enter Password','class'=>'form-control']); ?>
                </div>
            </div>

      </div>
    <div class="login-footer">
    	<div class="pull-left"><?= $this->Html->link('Back To Login','/users/login') ?></div>
        <div class="pull-right">
            <?= $this->Form->button('Submit',['type'=>'submit','class'=>'btn btn-default']) ?>
        </div>
        <div class="clearfix"></div>
    </div>
     <?= $this->Form->end() ?>
    </div>
