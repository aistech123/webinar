<div class="logo">
  <h1>Logo</h1>
</div>
<!-- Form Module-->
<div class="module form-module">
<div class="login-header"><h2>Forgot Password</h2></div>
<?= $this->Flash->render() ?>
 <div class="form">
   <?= $this->Form->create() ?>
    
    	<div class="form-group row">
            <?= $this->Form->label('email','Email:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('email',['type'=>'email','label'=>false,'placeholder'=>'Email','class'=>'form-control']); ?>
            </div>
        </div>
  </div>
<div class="login-footer">
    <div class="pull-left"><?= $this->Html->link('Back To Login','/users/login') ?></div>
	 <div class="pull-right">
        <?= $this->Form->button('Submit',['type'=>'submit','class'=>'btn btn-default']) ?>
    </div>
    <div class="clearfix"></div>
</div>
 <?= $this->Form->end() ?>
</div>
