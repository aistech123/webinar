 <?= $this->Html->script('https://code.jquery.com/jquery-1.12.3.js'); ?>
 <?= $this->Html->script('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') ?> 
 <?= $this->Html->script('https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js') ?>
 <?= $this->Html->css('https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css'); ?>
    <section>
  <div class="container">
    <div class="page_title">
      <div class="text-center">
        <h1><span><img style="margin-top:-10px;" src="/img/webinar-room-icon.png">Users Details</span></h1>
      </div>
    </div>
  </div>
</section>
    <section class="main_content">
       <div class="container">
        <div class="tab-pane" id="attendees">
        <div class="col-sm-12">
            <div class="table-responsive">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>UserID</th>
                        <th>DisplayName</th>
                        <th>Meeting Rooms</th>
                        <th>MaxAttendees</th>
                        <th>LastLogin</th>
                        <th>peekAttendees</th>
                        <th>Action</th>
                    </tr>
                </thead>
            <tbody>
             <?php 
       
             foreach ($user as $row) { 
               
                    ?>
            <tr>                    
                <td><?= $row->UserID; ?></td>
                <td><?= $row->DisplayName; ?></td>
                <td><?= MeetingsRoom($row->UserID);?></td>
                <td><?= $row->MaxAttendees; ?></td>
                <td><?= $row->LastLogin; ?></td>
                <td><?= peekAttendee($row->UserID); ?></td>
                <th><a href="javascript:void(0);" id="listuser" data-toggle="modal" data-target="#myModal<?= $row->UserID; ?>">Edit</a></th>
            </tr>
             <?php } ?>  
            </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</section>
 <script type="text/javascript">
   $(document).ready(function() {
      $('#example').DataTable();
    } );

    </script>
 <?php 
     
 foreach ($user as $row) {  ?>
<div class="modal fade" id="myModal<?= $row->UserID; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Attendance Details</h4>
      </div>
      <div class="modal-body">
        <form>
            <div class="form-group">
                <label></label>
                  <input type="hidden" name="userid" class="userid" value="<?= $row->UserID; ?>">
                <input type="text" class="form-control username" placeholder="Display Name" name="username" value="<?= $row->DisplayName; ?>" >
            </div>
            <div class="form-group">
                <label>Max Attendees</label>
                <input type="text" class="form-control max_attendees" placeholder="MaxAttendees" name="max_attendees"  value="<?= $row->MaxAttendees; ?>">
            </div>
           
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary saveuser">Save changes</button>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<script type="text/javascript">
$('.saveuser').click(function(){
    var username = $(this).parents('.modal').find('.username').val();
    var max_attendees = $(this).parents('.modal').find('.max_attendees').val();
    var userid = $(this).parents('.modal').find('.userid').val();
    $.ajax
    ({ 
        url: 'admin/ajaxedituser',
        data: {"username": username,"max_attendees":max_attendees,"userid":userid},
        type: 'post',
        success: function(result)
        {
           window.location.reload(); 
        }
    });
});
</script>