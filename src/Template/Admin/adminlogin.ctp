<div class="logo">
  <img id="blah" src="/webroot/img/Reg_Logo.png" alt="Image Preview" class="">
</div>
<!-- Form Module-->
<div class="module form-module">
<div class="login-header"><h2>Login to your account</h2></div>
<?= $this->Flash->render() ?>
 <div class="form">
   <?= $this->Form->create() ?>
    
        <div class="form-group row">
            <?= $this->Form->label('username','Username:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('username',['type'=>'username','label'=>false,'placeholder'=>'Username','class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group row">
            <?= $this->Form->label('Password','Password:',['class'=>'col-sm-3 form-control-label']); ?>
            <div class="col-sm-9">
                <?= $this->Form->input('password',['type'=>'password','label'=>false,'placeholder'=>'Password','class'=>'form-control']); ?>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-2 hidden-xs">&nbsp;</label>
            <div class="col-sm-10">
             <div class="checkbox-btn">
                    <?= $this->Form->checkbox('rc3',['value'=>'value-1','hiddenField'=>'flase','id'=>'inlineCheckbox1']) ?>
                    <?=  $this->Form->label('Keep me loged in.','Keep me loged in.') ?>
                </div>
            </div>
        </div>

  </div>
<div class="login-footer">
    <!-- <div class="pull-left"><?= $this->Html->link('Forgot Password','/users/forgetPassword') ?></div> -->
    <div class="pull-right">
        <?= $this->Form->button('Sign In',['type'=>'submit','class'=>'btn btn-default']) ?>
    </div>
    <div class="clearfix"></div>
</div>
 <?= $this->Form->end() ?>
</div>
