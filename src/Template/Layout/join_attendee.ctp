<?php $cakeDescription = 'Webinar';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('webinar.css') ?>
    <?= $this->Html->css('slider.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('datepicker.css') ?>
      <?= $this->Html->css('bvalidator.css') ?>
  

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

     <?= $this->Html->script('jquery-1.12.0.min.js') ?>
     <?= $this->Html->script('bootstrap.min.js') ?>
     <?= $this->Html->script('jssor.slider.min.js') ?>

     <?= $this->Html->script('bootbox.min.js') ?>
     <?= $this->Html->script('custom.js') ?>
     <?= $this->Html->script('bootstrap-datepicker.js') ?>
     <?= $this->Html->script('timepicki.js') ?>

     <?= $this->Html->script('ckeditor/ckeditor.js') ?>
     <?= $this->Html->script('jquery.bvalidator.js') ?>
      <?= $this->Html->script('jscolor.js') ?>

</head>
</head>
<body class="login">
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    
</body>
</html>