    <?php echo $this->element('inner_menu'); ?>
    <section>
      <div class="container">        
          <div class="page_title">
              <div class="text-center">
                  <h1><span><img src="/img/webinar-files.png" style="margin-top:-10px;"> Content Library</span></h1>
              </div>
          </div>  
          <div class="upload_btn">
        <div class="upload_btn">



        <?php echo $this->Form->create($librarie, ['type' => 'file']); 

             echo $this->Form->input('file', ['type' => 'file','label'=>false,'id'=>'browse','required'=>'false','class'=>'btn btn-primary btn-block','onchange'=>'Handlechange();','style'=>'display: none']); ?>
             
           <!--     <input id="browse" type="file" onchange="Handlechange();" style="display: none" name="fileupload"> -->
<input id="fakeBrowse" class="btn btn-primary btn-block" type="button" onclick="HandleBrowseClick();" value="Click Here to Upload Additional Files">
          </div>
          </div>
        <?= $this->Form->input('created_by',['type'=>'hidden','value'=>$user_id,'id'=>'created-by']) ?>   
        <?php echo $this->Form->end(); ?>      
      </div>
 
    </section>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script src="upload.js"></script>

    <section class="main_content">
    <div class="container content-library">
      <div class="">
	  <div class="table-responsive">
    <div id="fade"></div>
    <div id="modal">
            <img id="loader" src="/img/loading.gif" />
        </div>
        <table class="table-bordered" cellpadding="0" cellspacing="0" border="0" width="100%">
          <tbody><tr>
            <th>file name</th>
            <th>file size</th>
            <th>Upload Date</th>
            <th style="text-align:center;">preview</th>
            <th style="text-align:center;">delete</th>
          </tr>
          <?php foreach ($librarieData as $data) :?>
          <tr>
            <td><?php echo $data->file; ?></td>
            <td><?php echo byte_convert($data->size); ?>YTES</td>
            <td><?php  echo  $final=date('D, F d Y  ',strtotime(($data->created_at))); ?></td>
            <td align="center"><a href="/files/libraries/file/<?php echo $data->file ;?>" target="_blank"><i class="fa fa-search"></i></a></td>
            <td align="center">   <?= $this->Html->link('<span class="fa-stack fa-lg">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-close fa-stack-1x fa-inverse"></i>
                    </span>',
                     array(
                           
                         'action' => 'DeleteLibrary', $data->id,
                            ),
                        array(
                            'escape' => false,
                            'confirm' => __('Are you sure you want to delete ?', $data->id),
                        )
                        ); ?>
            </td>
          
          </tr>
        <?php endforeach; ?>
          

        </tbody></table>
		</div>
        </div>

          
    </div>
    <div class="container">
		<div class="pagination_box">
           <div class="row">
              <div class="col-sm-6">
                 <div class="numeric_paginaion">
                    <ul>
                       <?PHP echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1)); ?>
                    </ul>
                 </div>
              </div>
              <div class="col-sm-6 text-right">
                 <div class="next_previous">
                    <ul>
                       <?php   echo $this->Paginator->prev('<i class="fa fa-fast-backward"></i>',
                          ['escape' => false]); ?>
                       <?php   echo $this->Paginator->next('<i class="fa fa-fast-forward"></i>',
                          ['escape' => false]); ?>
                    </ul>
                 </div>
              </div>
           </div>
		   </div>
        </div>
     </div>

    </section>

    <script type="text/javascript" >

      function openModal() {
        document.getElementById('modal').style.display = 'block';
        document.getElementById('fade').style.display = 'block';
        }

        function closeModal() {
            document.getElementById('modal').style.display = 'none';
            document.getElementById('fade').style.display = 'none';
        }

       function HandleBrowseClick()
        {
            var fileinput = document.getElementById("browse");
            fileinput.click();
        }
       function formatBytes(bytes,decimals) {
           if(bytes == 0) return '0 Byte';
           var k = 1000;
           var dm = decimals + 1 || 3;
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
           var i = Math.floor(Math.log(bytes) / Math.log(k));
           return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + sizes[i];
        }
        function Handlechange()
          {
              var fileinput = document.getElementById("browse");
              var textinput = document.getElementById("filename");
              var created_by = document.getElementById("created-by").value;
            
               var file_data = $('#browse').prop('files')[0];  

                var validExtensions = ['jpg','gif','png','pdf','pptx']; //array of valid extensions
                var fileName = file_data.name;
                var filesize= formatBytes(file_data.size) 
                var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                if ($.inArray(fileNameExt, validExtensions) == -1){
                  /* alert("Invalid file type");
                   //$("#yourElem").uploadifyCancel(q);
                   return false;*/
                }
                if(filesize > '100.000MB')
                {
                  alert("file size must be less than 100MB.");
                  return false;
                }
                 $body = $("body");


                 var form_data = new FormData();                  
                form_data.append('file', file_data);
                 form_data.append('created_by', created_by);

                $.ajax({
                          url: 'content-library', // point to server-side PHP script 
                          dataType: 'text',  // what to expect back from the PHP script, if anything
                          cache: false,
                          contentType: false,
                          processData: false,
                          data: form_data,                         
                          type: 'post',
                          beforeSend: function() { 
                           openModal();
                          },
                     
                          success: function(php_script_response){
                            
                            location.reload();
                             closeModal();
                          },
                          
                      });

          }

    </script>

<style>
#fade {
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

#modal {
    display: none;
    position: absolute;
    top: 45%;
    left: 45%;
    width: 64px;
     height: auto;
    padding:30px 15px 0px;
    border: 3px solid #ababab;
    box-shadow:1px 1px 10px #ababab;
    border-radius:20px;
    background-color: white;
    z-index: 1002;
    text-align:center;
    overflow: auto;
}
</style>