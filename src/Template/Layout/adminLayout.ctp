<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Webinar';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
  <?= $this->Html->meta(
    'favicon.ico',
    '/favicon.ico',
    ['type' => 'icon']
);
?>
  <!--  <?= $this->Html->meta('icon') ?>-->

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('webinar.css') ?>
    <?= $this->Html->css('slider.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('datepicker.css') ?>
      <?= $this->Html->css('bvalidator.css') ?>
  

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

     <?= $this->Html->script('jquery-1.12.0.min.js') ?>
     <?= $this->Html->script('bootstrap.min.js') ?>
     <?= $this->Html->script('jssor.slider.min.js') ?>

     <?= $this->Html->script('bootbox.min.js') ?>
     <?= $this->Html->script('custom.js') ?>
     <?= $this->Html->script('bootstrap-datepicker.js') ?>
     <?= $this->Html->script('timepicki.js') ?>

     <?= $this->Html->script('ckeditor/ckeditor.js') ?>
     <?= $this->Html->script('jquery.bvalidator.js') ?>
      <?= $this->Html->script('jscolor.js') ?>
     

</head>
<body>
<header>
<?php  $controller=$this->request->params['controller'];
       $action=$this->request->params['action'];?>
  <div class="top_header">
    <div class="container-fluid">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="javascript:void(0);"><img src="/webroot/img/logo.png" /></a> </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
           <?php 
         //cho $this->AuthUser->user('username');
           if($userid): ?>
           
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a><?php echo 'welcome '.$userid; ?></a></li>
             
              <li class="current"><?= $this->Html->link(__('Logout'), ['controller'=>'Admin','action' => 'logout']) ?></li>

            </ul>
          </div>
          <?php endif ?>
          <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
      </nav>
    </div>
  </div>
</header>

<?= $this->Flash->render() ?>
 <div class="clearfix">
        <?= $this->fetch('content') ?>
    </div>
<footer>
<div class="container">
      <div class="col-sm-6 col-xs-6 col-lg-6">
          <h4>Copyright © 2016 OGI Webinar</h4>
        </div>
</div>
	
<script>
        jssor_1_slider_init = function() {

            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
    </script>
<script>
   // jssor_1_slider_init();

    //DELETE POP-UP OF BOOTBOX
    $(".delete-btn").click(function(){
        var delete_form = $(this).parent('form');
        bootbox.dialog({
            message: "Are you sure? you want to delete this record !!!",
            title: "Delete !!!",
            buttons: {
                danger: {
                    label: "Delete",
                    className: "btn-danger",
                    callback: function() {
                        delete_form.submit();            
                    }
                },
                success: {
                    label: "Cancel",
                    className: "btn-success",
                    callback: function() {
                
                    }
                }
            }
        });
    });
</script>
</body>
</html>
