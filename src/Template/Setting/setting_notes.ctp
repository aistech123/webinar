     <?php echo $this->element('inner_menu');?>
    <?php echo $this->element('setting_menu'); ?>
<div class="page_title">
        <div class="text-center">
          <h1><span>Coming Soon</span></h1>
        </div>
      </div>
 

     <!--    <?= $this->Form->create($setting,array('id'=>'setting')) ?>

         <div class="row">
                        <div class="col-sm-6">
                            <div class="checkbox checkbox-default">
                                <input id="checkbox2" class="styled" checked="checked" type="checkbox">
                                <label>Send Meeting Notes to attendees at the end of a Meeting</label>                    
                            </div>
                        </div>
                 </div>
                    <p>At the end of a meeting you can send Attendees 
    Meeting Notes via email. These may include links for them to download 
    the Presentations used or/and the video Recording of the Meeting</p>
                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Email Subject</label>
                                <?= $this->Form->input('notes_subject',['label'=>false,'data-bvalidator'=>'required','class'=>'form-control','placeholder'=>'Meeting Invitation: "An Olympic Idea!"']) ?>
                            </div>
                        </div>                    
                    </div>
                    <p>You can use #Presentation# to send recipients a list 
    of URLs they can use to download all presentations used in the meeting 
    (if available). You can also use #Recording# to send that a URL they can
     use to download a video recording of this meeting (if available).</p>
            <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
    <?= $this->Form->input('notes_message',['label'=>false,'type'=>'textarea','class'=>'form-control','id'=>'editor1']) ?>
    </div>
                        </div>                    
                    </div>
                    
                    
                   
                     <div class="row">
                        <div class="col-sm-12">
                              <?= $this->Form->button('Send Now',['type'=>'submit','name'=>'sendmail','class'=>'btn btn-primary btn-block btn-lg']) ?> 
                        </div>
                     </div>
                     <hr class="m-t-b-5">
                    
                    <div class="row">
                        <div class="col-sm-4 col-xs-6">
                         <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                           
                        </div>
                        <div class="col-sm-4 col-xs-6">

                            <button class="btn btn-default btn-block" id="reset" name="reset">Reset to Default</button>
                        </div>
                        <div class="col-sm-4">
                           <?= $this->Form->button('Start ',['Now'=>'submit','name'=>'start','class'=>'btn btn-primary btn-block']) ?>
                            
                        </div>
                    </div><?= $this->Form->end() ?></form>
            </div>
            
            
        </div>
    </section>

    <script type="text/javascript">
    $('#setting').bValidator();
        $(".date-picker").datepicker();
        $('#timepicker').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23
        });

        var editor = CKEDITOR.replace( 'editor1', {
        	filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
        	filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
        	filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
        	filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        	filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        	filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
    $( "#reset" ).click(function() {
        $('.form-control').val('');
      $("#editor1-textarea").val("");
     
       
    });
    </script> -->