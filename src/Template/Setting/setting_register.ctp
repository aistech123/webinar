    <?php echo $this->element('inner_menu');?>
    <?php echo $this->element('setting_menu'); ?>
    <p><strong>Image</strong> (Recommended size : 600x400)</p>
                  <?= $this->Form->create($setting,['type'=>'file','id'=>'setting']) ?>
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="row">
                            
                            <div class="col-sm-6">
                                <label>Select Image For Meeting</label>
                               <?= $this->Form->input('image', ['type' => 'file','label'=>false,'id'=>'imgInp','required'=>'false']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-6">
                        <div >
                            <label>Image Preview</label>

                             <img id="blah" src="/webroot/files/Setting/image/<?= $setting->image; ?>" alt="Image Preview" class="image_preivew" />
                           
                        </div>
                    </div>
                </div>
          

                    <div class="form-group">
                        <div class="checkbox checkbox-default">
                            <input class="styled" value="password" type="checkbox"  checked="checked">
                            <label for="checkbox2">Make Meeting Private</label>
                        </div>
                    </div>
                    
                    <div class="form-group password_hide password_show">
                        <label>Password</label>
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="password">
                            </div>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Header Text</label>
                               <?= $this->Form->input('name',['label'=>false,'type'=>'textarea','class'=>'form-control','id'=>'editor1','data-bvalidator'=>'required']) ?>
                                </div>
                        </div>                    
                    </div>
                    
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Form Fields</label>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6">Field Name</div>
                                    <div class="col-sm-6 col-xs-6">Required</div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6"><input class="form-control" placeholder="Name" type="text"> </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox" checked="checked">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                              <!--  <div class="row">
                                    <div class="col-sm-6"><input class="form-control" placeholder="Form" type="text"> </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div>
                                </div> -->
                                <div class="row">
                                    <div class="col-sm-6 col-xs-6"><input class="form-control" placeholder="Email" type="text"> </div>
                                    <div class="col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-default">
                                                <input class="styled" value="password" type="checkbox" checked="checked">
                                                <label for="checkbox2"><span class="none">Required</span></label>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                               <!-- <div class="row">
                                    <div class="form-group password_hide password_show">
                                        <div class="col-sm-6">
                                            <input class="form-control" placeholder="Password" type="text">
                                        </div>
                                    </div>
                                </div> -->
                               <div class="row m-t-b-5">
                                    <div class="col-sm-4">
                                      <!--   <button class="btn btn-primary">Add Field</button>-->
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-4 col-xs-6">
                                          <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                                    </div>
                                    <div class="col-sm-4 col-xs-6">
                                        <button class="btn btn-default btn-block">Reset to Default</button>
                                    </div>
                                    <!--<div class="col-sm-4">
                                        <button class="btn btn-success btn-block">Start Now</button>
                                    </div> -->
                                </div>
                            </div>
                        </div>                    
                    </div>
             
              <?= $this->Form->end() ?>
            </div>
            
            
        </div>
    </section>


    <script type="text/javascript">
    $('#setting').bValidator();
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

            $("#imgInp").change(function(){
                readURL(this);
            });

        $(".date-picker").datepicker({dateFormat: 'yyyy-mm-dd'}
           
            );
        $('#timepicker').timepicki({
            show_meridian:false,
            min_hour_value:0,
            max_hour_value:23
        });

         $('input[type="checkbox"]').click(function(){
            if($(this).attr("value")=="password"){
                $(".password_show").toggle();
            }      
        });

        var editor = CKEDITOR.replace( 'editor1', {
            filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
            filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
            filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
            filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
            filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
        });
        CKEDITOR.editorConfig = function( config ) {
        config.allowedContent = true;
        };
            $( "#reset" ).click(function() {
                $('.form-control').val('');
                $("textarea#editor1").val('');
            });
    </script>