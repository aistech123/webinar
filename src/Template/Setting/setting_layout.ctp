  <?php echo $this->element('inner_menu');?>
  <?php echo $this->element('setting_menu'); ?>
  <div class="row">
                <div class="col-sm-6">
  <form name="setting" id="setting" enctype="multipart/form-data" action="/Setting/setting_layout" method="post">
  <p>Choose the Starting Layout of your Meeting Room</p>

             
              <div class="row">
               <div class="col-sm-6 col-xs-12">
                <div class="form-group">
                         <?php 

                         $this->Form->templates([
                            'nestingLabel' => '{{input}}<label{{attrs}}>{{text}}</label>',
                            'formGroup' => '{{input}}{{label}}',
                        ]);
                     

                         $this->Form->templates([
                            'nestingLabel' => '{{input}}<label{{attrs}}>{{text}}</label>',
                            'formGroup' => '{{input}}{{label}}',
                        ]);
                       $options2= array(
                            'bbb.layout.name.defaultlayout' => 'Default',
                            'bbb.layout.name.videochat' => 'Video Chat',
                            'bbb.layout.name.webcamsfocus' => 'Webcams',
                            'bbb.layout.name.presentfocus' => 'Presentation',

                        );  
                          $selectedsections =$setting->sections;
                         echo $this->Form->input('sections', array(
                                                'label' => false,
                                                'type' => 'select',
                                                //'multiple' => 'checkbox',
												'class' => 'form-control',
                                                'options' => $options2,
                                                 'default' => $selectedsections,
                                                'data-bvalidator'=>'required'
                                              ));
                         ?>
              </div>
              </div>
              

  </div>
      
          </div>
        </div>
        
       
      <!--   
        <div class="row">
        		<div class="col-sm-6">
              	<label>Choose Background</label>
                 <select id="background" name="bgselect" class="form-control">
                <?php 
                    if($setting->color == '' && $setting->background == '')
                    {
                      echo $selected='selected="selected"';

                      } 
                      if($setting->color == '')
                      {
                      echo $selected='selected="selected"';

                      } 
                      if($setting->background == '')
                      {
                      echo $selected='selected="selected"';

                      } 

                  ?>
                   <option  <?php 
                    if($setting->color == '' && $setting->background == '')
                    {
                      echo $selected='selected="selected"';

                      } ?>>Choose Background</option>
                    <option value="image"<?php  if($setting->background != '')
                    {
                      echo $selected='selected="selected"';

                      } ?> >Background Image</option>
                  <option value="color" <?php if($setting->color != '')
                    {
                      echo $selected='selected="selected"';

                      } ?>>color</option>
                </select>
              	<div class="text-center" id="color">
                  	<input type="button" class="btn btn-primary jscolor" id="btnSubmit" value="<?= $setting->color ?>" name="color" onchange="update(jscolor)" />Color</button><br>
                      <input type="hidden" name="colorval" id="colorval" value="<?= $setting->color ?>">
                    </div> 
                      <div class="text-center" id="image">
                       <?= $this->Form->input('background', ['type' => 'file','label'=>false,'id'=>'browse','required'=>'false','style'=>'display:none']) ?>
                      <input id="fakeBrowse" class="btn btn-primary btn-block" type="button" onclick="HandleBrowseClick();" value="Upload Background">
                  </div>
              </div>
              <div class="col-sm-6" id="colorp">
              	<div class="image_preivew" id="Preview">
                  <label>Preview Background <br> Image or Color</label>
                 
                  </div>
              </div>
              <div class="col-sm-6" id="imagep">
                <div class="" id="Preview">
                
                  <img id="blah" src="/webroot/files/Setting/background/<?= $setting->background ?>" alt="Image Preview" class="image_preivew" />
                  </div>
              </div>
        </div> -->
      <div class="row m-t-10">
            <div class="col-sm-3">            
                
                    <div class="form-group">
                        <label>Choose Background</label>

                       <?php    $skinoptions= array(
                            'BBBDefault.css.swf' => 'Default',
                            'BBBGrey.css.swf' => 'Dark Grey',
                            'BBBBlue.css.swf' => 'Blue',
                             'BBBWhite.css.swf' => 'White',
                            ); 
                         $selectedskin =$setting->skin;
                         echo $this->Form->input('skin', array(
                                                'label' => false,
                                                'type' => 'select',
                                                'id'=>'skin',
												'class' => 'form-control',
                                                //'multiple' => 'checkbox',
                                                'options' => $skinoptions,
                                                 'default' => $selectedskin,
                                                'data-bvalidator'=>'required'
                                              ));
                             ?> 
                        
                      </div>
                      
              </div>
              <div class="col-sm-6 pull-right">
                <div class="">
                      <!--<label>Preview Theme</label>-->
                      <img class="image_preivew" alt="Image Preview" id="theme">
                  </div>
              </div>
        </div>
        
        
        <hr class="m-t-b-5">
      <div class="row">
                      <div class="col-sm-4 col-xs-6">
                       <?= $this->Form->button('Save Progress',['type'=>'submit','name'=>'save','class'=>'btn btn-primary btn-block']) ?>
                         
                      </div>
                      <div class="col-sm-4 col-xs-6">

                          <button class="btn btn-default btn-block" id="reset" name="reset">Reset to Default</button>
                      </div>
                    <!--  <div class="col-sm-4">
                         <?= $this->Form->button('Start ',['Now'=>'submit','name'=>'start','class'=>'btn btn-primary btn-block']) ?>
                          
                      </div>-->
                  </div>
        
        
         </form>        
      </div>
    </div>
    
    
  </section>

  <script>
  $('#setting').bValidator();

  jscolor =$("#colorval").val();
  skin=$("#skin").val();
      if(skin == 'BBBDefault.css.swf')
      {
         $('#theme').attr('src','/webroot/img/bbbwhite.png');

      }
        if(skin == 'BBBGrey.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbgrey.png');
      }
        if(skin == 'BBBBlue.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbblue.png');
      }

 $('#skin').on('change', function() {
 
      if(this.value == 'BBBDefault.css.swf')
      {
         $('#theme').attr('src','/webroot/img/bbbwhite.png');

      }
        if(this.value == 'BBBGrey.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbgrey.png');
      }
        if(this.value == 'BBBBlue.css.swf')
      {
        $('#theme').attr('src','/webroot/img/bbbblue.png');
      }
});
  /*document.getElementById('Preview').style.backgroundColor = '#' + jscolor
        function update(jscolor) 
        {
         
          document.getElementById('Preview').style.backgroundColor = '#' + jscolor
          $("#colorval").val(jscolor);
        }
           function HandleBrowseClick()
          {
              var fileinput = document.getElementById("browse");
              fileinput.click();
          }
          function readURL(input) {

                  if (input.files && input.files[0]) {
                      var reader = new FileReader();

                      reader.onload = function (e) {
                          $('#blah').attr('src', e.target.result);
                      }

                      reader.readAsDataURL(input.files[0]);
                  }
              }

            $("#browse").change(function(){
                readURL(this);
            });
            bk= $("#background").val();
            if(bk == 'color')
            {
              $("#color").show();
                $("#image").hide();
                $("#colorp").show();
                $("#imagep").hide();
            }
            else
            {
               $("#image").show();
                $("#color").hide();
                $("#imagep").show();
                $("#colorp").hide();
            }
            
            $('#background').on('change', function() {
             background=this.value; // or $(this).val()
             if(background == 'color')
             {
                $("#color").show();
                $("#image").hide();
                $("#colorp").show();
                $("#imagep").hide();
             }
             else
             {
                $("#image").show();
                $("#color").hide();
                $("#imagep").show();
                $("#colorp").hide();
               }
            });*/
  </script>
