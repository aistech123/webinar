<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Meeting Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $topic
 * @property string $password
 * @property string $presentation_file
 * @property \Cake\I18n\Time $meeting_time
 * @property int $meeting_duration
 * @property bool $deleted
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 */
class Meeting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

    /**
     * Fields that are excluded from JSON an array versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
}
