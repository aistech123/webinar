<?php
namespace App\Model\Table;

use App\Model\Entity\Meeting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Meetings Model
 *
 */
class MeetingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('meetings');
        $this->displayField('name');
        $this->primaryKey('id');
     
        $this->addBehavior('Josegonzalez/Upload.Upload', [
           
            
             'presentation_file'=>[],
             'background'=>[]
        ]);
         $this->hasMany('attendees', [
            'className' => 'Attendees',
             'foreignKey' => 'meeting_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Vaeelidation\Validator
     */
     /*add meeting valication*/
    public function validationAdd($validator)
    {
        $validator->add('name',
            ['characters'=>[
                    'rule' => array('custom', '/^[0-9a-zA-Z_\-]+$/'),
                    'message'  => 'Meeting name only allow alphanumeic characters '    
                ]
            ]
        );
        $validator->notEmpty('name');
        $validator->add('name',
            ['unique' => [
                'rule' => 'validateUnique', 
                'provider' => 'table', 
                'message' => 'This meeting name already present in our record please provide different name.'
                ]
            ]
        );
       return $validator;
    }
     public function validationEdit($validator)
    {
        $validator->add('name',
            ['characters'=>[
                    'rule' => array('custom', '/^[0-9a-zA-Z_\-]+$/'),
                    'message'  => 'Meeting name only allow alphanumeic characters '    
                ]
            ]
        );
        $validator->notEmpty('name');
        $validator->add('name',
            ['unique' => [
                'rule' => 'validateUnique', 
                'provider' => 'table', 
                'message' => 'This meeting name already present in our record please provide different name.'
                ]
            ]
        );
       return $validator;
    }
    public function validationDefault(Validator $validator)
    {
        return $validator;
    }
    public function validationOnlyCheck(Validator $validator) {
        $validator = $this->validationDefault($validator);
        $validator->remove('email_subject', 'notEmpty');
         $validator->remove('notes_subject', 'notEmpty');
         $validator->remove('attendees', 'notEmpty');
        return $validator;
        exit;
    }
    public $validate = array(
    'presentation_file' => array(
        'extension' => array(
            'rule' => array('extension', array('png','jpg','jpeg')),
            'message' => 'Only images files',
            'allowEmpty' => true
        )
    )
);
}
