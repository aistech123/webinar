<?php 
namespace App\Model\Table;

use App\Model\Entity\Poll;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AttendeesTable extends Table
{
	  public $name = 'Attendees';
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
           $this->belongsTo('meetings',[
            'foreignKey' => 'meeting_id'
        ]);
   

    }
}

?>