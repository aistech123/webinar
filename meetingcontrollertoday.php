<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\I18n\Time;
use BigBlueButton\BigBlueButton as BigBlueButton;
use BigBlueButton\Parameters\CreateMeetingParameters as CreateMeetingParameters;
use BigBlueButton\Parameters\JoinMeetingParameters as JoinMeetingParameters;
use BigBlueButton\Parameters\GetMeetingInfoParameters as IsMeetingRunningParameters;
use BigBlueButton\Parameters\IsMeetingRunningParameters as GetMeetingInfoParameters;
use BigBlueButton\Core\ApiMethod as ApiMethod;
use BigBlueButton\Responses\GetDefaultConfigXMLResponse;
use BigBlueButton\Parameters\GetRecordingsParameters as GetRecordingsParameters;
use BigBlueButton\Parameters\DeleteRecordingsParameters;
use BigBlueButton\Parameters\EndMeetingParameters;
use BigBlueButton\Responses\EndMeetingResponse;
use BigBlueButton\Responses\GetMeetingsResponse;
use BigBlueButton\Responses\GetMeetingInfoResponse;
use Cake\ORM\TableRegistry;
use DateTimeZone;
use DateTime;
use GuzzleHttp\Client as Client;
use Cake\Routing\Router;
use Cake\Utility\Security;
use Cake\Mailer\Email;
/**
 * Meetings Controller
 *
 * @property \App\Model\Table\MeetingsTable $Meetings
 */
  error_reporting(0);
  class MeetingsController extends AppController
  {

       var $paginate = array(
          'limit' => '5',
          
      );
      /**
       * Index method
       *
       * @return \Cake\Network\Response|null
       */
      public function index()
      {
        echo date('Y-m-d H:i:s');
        echo phpinfo();
        exit;
          /*echo $user_ip = getenv('REMOTE_ADDR');
		$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
		echo "<pre>";print_r($geo); exit;*/
		echo $country = $geo["geoplugin_countryName"];
		echo $city = $geo["geoplugin_city"];
          $meetings = $this->paginate($this->Meetings);
          $this->set(compact('meetings'));
          $this->set('_serialize', ['meetings']);
      }

      /**
       * View method
       *
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|null
       * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
       */
      public function view($id = null)
      {
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          $this->set('meeting', $meeting);
          $this->set('_serialize', ['meeting']);
      }

      /**
       * Add method
       *
       * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
       */
      public function add()
      {
          $meeting = $this->Meetings->newEntity();
          // echo "<pre>";print_r(); exit;
          if ($this->request->is('post')) {
              
              $data = $this->request->data;
              date_default_timezone_set($this->Auth->user('timezone'));
              $data['created_at'] = new Time(date('Y-m-d H:i:s'));
              $data['meeting_date'] = date('m/d/Y');
              $data['timezone'] = $this->Auth->user('timezone');
              $data['meeting_time'] = date('H:i');
              $data['meeting_id'] = $data['name'];

              $meeting = $this->Meetings->patchEntity($meeting, $data,['validate' => 'add']);
              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));
                 return $this->redirect(['action' => 'editBasic',$id]);
                   if (isset($this->request->data['start_meeting'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting', $data['meeting_id']]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      public function editBasic($id = null)
       {
         $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
            
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
                $data['meeting_id'] = $data['name'];
               
                if($data['password'] == '')
                {
                  $data['attendeePassword'] = '';
                }
          $meeting = $this->Meetings->patchEntity($meeting, $data,['validate' => 'edit']);
             // $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $this->Flash->success(__('The meeting has been saved.'));
                 if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              } else {
                 
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }

            }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['edit_basic']);
       }  
 

  //check unique Meeting Name

       public function checkMeetingName($meetingName)
       {
         $meetingName=$_POST['meetingName'];
          $meeting = $this->Meetings->find()
                ->where(['name =' => $meetingName])
                ->count();

               if($meeting > 0)
                {
                  echo $res="not OK";
                }
                else
                {
                  echo $res="ok";
                }

                exit;
       }

  //Edit mode check unique Meeting Name

       public function EditcheckMeetingName($meetingName,$id)
       {
         $meetingName=$_POST['meetingName'];
         $id=$_POST['id'];
         $meeting = $this->Meetings->find()
                ->where(['name =' => $meetingName,'id !=' => $id])
                ->count();

                if($meeting > 0)
                {
                  echo $res="not OK";
                }
                else
                {
                  echo $res="ok";
                }

                exit;
       }

        public function newmeetingroomschedule($id=null)
        {  
           $meeting = $this->Meetings->get($id, [
                  'contain' => []
              ]);
                   if($meeting->meeting_date != '')
                   {
                      $meetingDate = date("m/d/Y", strtotime($meeting->meeting_date)); 
                    }
                    else
                    {
                        $meetingDate = date('m/d/Y');
                    }
              if ($this->request->is(['patch', 'post', 'put'])) {
                  $data = $this->request->data;
                  $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
                

                    $time=$data['meeting_time'] ;
                     $data['meeting_time']= str_replace(' ', '', $time);
       
                     if($data['repeat']=='on')
                     {
                          $monthrepeat=implode(',',$data['monthrepeat']);
                          $data['monthrepeat']=$monthrepeat;

                          $repeateDay=implode(',',$data['repeatDay']);
                          $data['repeatDay']=$repeateDay;

                    }
                    else
                    {
                      $data['repeatTime']='';
                    }

                  $meeting = $this->Meetings->patchEntity($meeting, $data);
                  if ($this->Meetings->save($meeting)) {
                      $id = $meeting->id;

                      $this->Flash->success(__('The meeting has been saved.'));
                      //return $this->redirect(['action' => 'addSuccess',$id]);
                      if (isset($this->request->data['start'])) { 
                        
                          return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                      }
                  return $this->redirect(['action' => 'newmeetingroomschedule',$id]);
                  } else {
                      $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
                  }
          }


          $timezoneList = $this->timezoneList(); 

          $query = TableRegistry::get('Countries');
          $countries = $query->find('list');
          $this->set(compact('user','countries','timezoneList','meeting','meetingDate'));
          $this->set('_serialize', ['newmeetingroomschedule','meeting']);

      }

      /**
       * Add Meeting Image Upload
      */

      public function addImageUpload($id=null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));

              $meeting = $this->Meetings->patchEntity($meeting, $data);

              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));

                   if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /*
       * Add Success
       */

      public function addSuccess($id=null){

          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /**
       * Edit method
       *
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
       * @throws \Cake\Network\Exception\NotFoundException When record not found.
       */
      public function edit($id = null)
      {
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              
              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $this->Flash->success(__('The meeting has been saved.'));
                  return $this->redirect(['action' => 'index']);
              } else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting'));
          $this->set('_serialize', ['meeting']);
      }

      /*
       * Email Invitaion
       */

      public function emailInvitation($id = null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;

              //Convert time to H:i:s format, Need to remove space from get meeting_time and then convert.
              $meeting_time_array = explode(":",$this->request->data('meeting_time'));
              @$meeting_time_string = trim($meeting_time_array[0]).':'.trim($meeting_time_array[1]);
              $meeting_time = new Time(date('H:i:s', strtotime($meeting_time_string)));

              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              $data['meeting_time'] = $meeting_time;
              $meeting_id=$meeting->meeting_id;
              $meeting_name=$meeting->name;
               $password=$meeting->password;
              $meeting_date=$meeting->meeting_date;
              $to=explode(',',$data['attendees']);
              
              if (isset($this->request->data['sendmail'])) {
                  $sub=$data['email_subject'];
                      $email_message=$data['email_message'];
                      $this->sendMail($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name,$password);

                      $this->Flash->success(__('The meeting Invitaion mail send successfully.'));
                    
                  }
                  else
                  {
                        $meeting = $this->Meetings->patchEntity($meeting, $data);
                        if ($this->Meetings->save($meeting)) {

                            $this->Flash->success(__('The meeting has been saved.'));

                       if (isset($this->request->data['start'])) { 
                          
                            return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                        }
                   } 
                else 
                {
                        $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
                 }
       }
   }

          $timezoneList = $this->timezoneList();
          $this->set(compact('meeting','timezoneList'));
          $this->set('_serialize', ['email_invitation']);
      }


       /*
       * Email Meeting Notes
       */

      public function emailMeetingNote($id = null){
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;

              //Convert time to H:i:s format, Need to remove space from get meeting_time and then convert.
              $meeting_time_array = explode(":",$this->request->data('meeting_time'));
              @$meeting_time_string = trim($meeting_time_array[0]).':'.trim($meeting_time_array[1]);
              $meeting_time = new Time(date('H:i:s', strtotime($meeting_time_string)));

              $data['updated_at'] = new Time(date('Y-m-d H:i:s'));
              $data['meeting_time'] = $meeting_time;
              $meeting_id=$meeting->meeting_id;
              $meeting_name=$meeting->name;
              $meeting_date=$meeting->meeting_date;
              $to=explode(',',$meeting->attendees);
              
              if (isset($this->request->data['sendmail'])) {
                  $sub=$data['notes_subject'];
                      $email_message=$data['notes_message'];
                      $this->sendMailNote($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name);

                      $this->Flash->success(__('The meeting Note mail send successfully.'));
                    
              }
              else
              {
                  $meeting = $this->Meetings->patchEntity($meeting, $data);
                if ($this->Meetings->save($meeting)) {

                      $this->Flash->success(__('The meeting has been saved.'));

                   if (isset($this->request->data['start'])) { 
                      
                        return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                    }
                } 
                else 
                {
                        $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
                 }
               }
        }

          $timezoneList = $this->timezoneList();
          $this->set(compact('meeting','timezoneList'));
          $this->set('_serialize', ['email_invitation']);
      }

      /**
       * Delete method
       * @description Meeting cancel from upcoming meeting page
                      1)delete meeting if meeting is not Repeated
                      2)update meeting date as next repeated date if meeting is repeated. 
       * @param string|null $id Meeting id.
       * @return \Cake\Network\Response|null Redirects to index.
       * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
       */
      public function delete($id = null)
      {
          //$this->request->allowMethod(['post', 'delete']);
          $meeting = $this->Meetings->get($id);
        if($meeting->repeatTime == '')
        {
           if ($this->Meetings->delete($meeting)) {
                $this->Flash->success(__('The meeting has been deleted.'));
            } else {
                $this->Flash->error(__('The meeting could not be deleted. Please, try again.'));
              }
        }
        else
        {  @date_default_timezone_set($meeting->timezone);
            $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
            $today = date('Y-m-d H:i');
            $today1 = date('Y-m-d');
            if($meeting->repeatTime == 'daily' )
            {
                $meeting_date1= date('Y-m-d',strtotime($meeting->meeting_date));
                if($meeting_date1 >= $today1 )
                {
                 
                   $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
                }   
                else
                {
                    $meeting_date= date('Y-m-d'); 
                }
              
              $finaldate1= date('Y-m-d',strtotime ( '+1 day' , strtotime ( $meeting_date ) )) ;
            
               $meetingTable = TableRegistry::get('Meetings');
               $meetingdata = $meetingTable->get($id);
               $meetingdata->meeting_date=$finaldate1;
                if($meetingTable->save($meetingdata))
                {
                  echo 'update';
                }
             
            }  
            if($meeting->repeatTime == 'weekly')
            {
               $day=date('D');
               $day1=$meeting->repeatDay;
                if($meeting_date < $today1)
                {
                   $day1=$meeting->repeatDay;
                   if($day1 == $day)
                   {
                    $meeting_date= date('Y-m-d');
                   } 
                   else
                   {
                   $meeting_date=date('Y-m-d',strtotime('next '.$day1 ));
                    }
                }
                else
                {
                   $meeting_date= date('Y-m-d',strtotime($meeting->meeting_date));
                }
                $finaltime=$meeting_date.' '.$meeting->meeting_time;
                $finaldate1= date('Y-m-d',strtotime ( '+7 day' , strtotime ( $meeting_date ) )) ;
            
                $meetingTable = TableRegistry::get('Meetings');
                $meetingdata = $meetingTable->get($id);
                $meetingdata->meeting_date=$finaldate1;
              if($meetingTable->save($meetingdata))
              {
                echo 'update';
              }

           }
        // if Meeting Repeated monthly
          if($meeting->repeatTime == 'monthly')
          {  
           
              $day=date('F Y');
              $monthrepeat=explode(' ',$meeting->monthrepeat);
           
            if($monthrepeat['1'] != '')
            {
              
              if($meeting_date < $today1)
              {
                //if meeting is reapted fifth monday or tuesday 
                  
                  $thismonth = date('F',strtotime('first day of +1 month'));
                  $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +1 month'));
                  $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +1 month'));
                    if($thismonth != $nextmonth)
                    { 

                      $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +2 month'));
                      $thismonth = date('F',strtotime('first day of +2 month'));
                      $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +2 month'));

                        if($thismonth != $nextmonth)
                        {
                           $row_date= date('Y-m-d', strtotime($meeting->monthrepeat. ' of +3 month'));
                          $thismonth = date('F',strtotime('first day of +3 month'));
                          $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of +3 month'));
                        }
                    }
              }
               else
              {

                $row_date= date('Y-m-d',strtotime($meeting->meeting_date));
              }
            //if Meeting Repeated monthly,and cancel this meeting,again arrange next scheduled fifth or forth (repeated week day)
            $finalmonth= date('Y-m-d',strtotime('first day of +1 month' ,strtotime($row_date) )) ;
            $finaltime=$row_date.' '.$meeting->meeting_time;

            $thismonth = date('F',strtotime('first day of'. $finalmonth));
            $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of'. $finalmonth));
            $finaldate1= date('Y-m-d',strtotime ($meeting->monthrepeat. ' of '. $finalmonth))  ;
                   if($thismonth != $nextmonth)
                    { 
                      $thismonth = date('F',strtotime('first day of'. $finaldate1));
                      $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of'. $finaldate1));
                      $finaldate1= date('Y-m-d',strtotime ($meeting->monthrepeat. ' of '. $finaldate1))  ;
                      if($thismonth != $nextmonth)
                      {
                         $thismonth = date('F',strtotime('first day of'. $finaldate1));
                      $nextmonth = date('F',strtotime($meeting->monthrepeat. ' of'. $finaldate1));
                      $finaldate1= date('Y-m-d',strtotime ($meeting->monthrepeat. ' of '. $finaldate1))  ;
                      }

                     } 
             //update meeting date value as a next repeated week day 
                  $meetingTable = TableRegistry::get('Meetings');
                  $meetingdata = $meetingTable->get($id);
                  $meetingdata->meeting_date=$finaldate1;
                if($meetingTable->save($meetingdata))
                {
                  echo 'update';
                }
             }   
            else
            {
              
              if($meeting_date < $today1)
              {              
                $monthrepeat=explode('th',$meeting->monthrepeat);
                $day=date('Y-m-'.$monthrepeat[0],strtotime('+1 month '));
              }
              else
              {             
                $row->monthrepeat. ' of '. $day;
                $day= date('Y-m-d',strtotime($meeting->meeting_date));
              }    
        
            $monthrepeat=explode('th',$meeting->monthrepeat);
            $finaltime=$day.' '.$meeting->meeting_time;
            $finaldate= date('Y-m-d H:i',strtotime ( '-30 minute' , strtotime ( $finaltime ) )) ;
            $finaldate1= date('Y-m-d',strtotime ( '+1 month' , strtotime ( $day ) )) ;

            $meetingTable = TableRegistry::get('Meetings');
            $meetingdata = $meetingTable->get($id);
            $meetingdata->meeting_date=$finaldate1;
            if($meetingTable->save($meetingdata))
            {
              echo 'update';
            }
          
        }
      }

          return $this->redirect(['controller'=>'Meetings','action' => 'schedule_meetingcopy',"upcoming"]);
      
      }
    }
      // ==============================================    
      // ============================================= 
      /**
      * menthod : joinMeeting
      * @Description : 1) host can not Join More Than one Meeting at Same Time.
                       2) Check schedule_time ,current time is running before 30 minute + duration +after 30 minute of  schedule_time
                       3) if not then attendee can not join meeting,but host can join
                       4) if Meeting is Active ,Ateendees can join
                       5)if Host is not in room untill next 30 minute,
                         attendees kicked out from meeting automatically  
         **/
      // ==============================================      
       public function joinMeeting($meeting_id='',$AEmail,$username,$userId)
      {   
          $meeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->first();

          $meetings = $this->Meetings->find()->
          where(['deleted' => '0','created_by'=>$this->Auth->user('id'),'is_meeting_room_active'=>'1','meeting_id !='=>$meeting_id])->count();
                
          // Check Any Meeting Status is Active Or Not
         if($meetings > '0')
         {
          $this->Flash->error(__('You can only have one Active Meeting at the same time.'));
          return $this->redirect(Router::fullbaseUrl());;
          }
          // Set Meeting Attendee PassWord
          if($meeting->password != '')
          {
             $password=$meeting->password;
          }
          else
          {
             $password='1234';
          }
         //set default message 
          if($meeting->default_message != '')
          {
           $default_message=$meeting->default_message;
          }
           else
           {
            $default_message='Welcome to '.$meeting->name;
           }
      
          $mpassword='swM6dfAKVF@$'; //set Moderatore password
          $logoutUrl='http://'.$_SERVER['HTTP_HOST'].'/meetings/endMeeting/'.$meeting->meeting_id.'/'.$mpassword; //set Logout Url
        
        //call CheckScheduleOfMeeting from helper.php     
        $sched= CheckScheduleOfMeeting($meeting_id);
       
        date_default_timezone_set($meeting->timezone);
        $today = date('Y-m-d H:i');
       
        $bbb = new BigBlueButton;
       
        //check time
        if(($today >=  $sched['finaldate'] && $today <=  $sched['enddate']) || (!empty($this->Auth->user('username'))) || ($meeting->is_meeting_room_active == '1'))
        {
         
          $meeting = json_decode(json_encode($meeting));
         
          //CREATE MEETING
          $create_meeting = new CreateMeetingParameters($meeting_id,$meeting->name);
          $duaration=$meeting->meeting_duration+30;
          $create_meeting->setModeratorPassword($mpassword)->setrecord('true')->setautoStartRecording('false')->setattendeePassword($password)->setduration(0)->setwelcome($default_message)->setlogoutUrl($logoutUrl);
         
            $xml = '';

            //fetch library's files of user
            $librariesTable = TableRegistry::get('libraries');
            $librariedata = $librariesTable->find()
            ->where(['meeting_id'=>$meeting->id])->all();
            $xml = "<?xml version='1.0' encoding='UTF-8'?>
            <modules>
            <module name='presentation'>";
              
              //send files to meeting
             
               $file_url = Router::url('/',true).'img/000default.pdf';
                    $xml.="<document name ='default Document' url='".$file_url."' ></document>";     
              foreach ($librariedata as $key => $value) {
                  $file_url = Router::url('/', true).'webroot/files/library/'.$value->file;
                  $xml.="<document name ='default111 Document' url='".$file_url."' ></document>";
              }

                      $xml.="</module>
                      </modules>";

              $result = $bbb->createMeeting($create_meeting,$xml);
            
              // call default config xml file 
              $configXMLuRL = $bbb->getDefaultConfigXMLUrl(ApiMethod::GET_DEFAULT_CONFIG_XML);
              $doc=$bbb->getDefaultConfigXML($configXMLuRL);

            // change config parameter like default layout, lock feature            
              if($meeting->sections != '')
              {
                (string)$doc->layout->attributes()->defaultLayout=$meeting->sections;
              }
              if($meeting->skin != '')
              {
               (string)$doc->skinning->attributes()->url=$bbb->bbbServerURL.'/client/branding/css/'.$meeting->skin;
              }
     
            //make new config.xml file after changed layout and lock features 

            $newXML = $doc;
            str_replace("\n", "", $newXML); //These 4 commands are based on the jsp BBB API.
            str_replace("\t", "", $newXML); 
            str_replace(">  <", "><", $newXML);
            str_replace(">    <", "><", $newXML);
            $setConfigXMLParams = array(
            'meetingId' => $meeting_id,
            'configXML' => $doc
            );
                   
           // set new config.xml and get config token for join Meeting
            
          $newConfigXMLResponse = $bbb->setConfigXMLWithXmlResponseArray($setConfigXMLParams,$newXML);
          $configToken = (string)$newConfigXMLResponse->configToken;
       
           if($AEmail != '' )
           {
             $name = (!empty($this->Auth->user('username')))?$this->Auth->user('username'):$username;

                   //JOIN MEETING
                  if(!empty($result))
                  {
                    
                    $join_meeting = new JoinMeetingParameters($meeting_id,$name,$password,$configToken,$userId);
                   
                    $meeting_url = $bbb->getJoinMeetingURL($join_meeting);
                    $id=$meeting->id;
                    $meetingTable = TableRegistry::get('Meetings');
                    $meetingdata = $meetingTable->get($id);
                    $meetingdata->meeting_exit_url = $meeting_url;

                   // update Data in copy_attendee Table
                     $this->copy_attendees('user',$meeting->meeting_id,$meeting->created_by,$meeting->timezone);

                      if($meetingTable->save($meetingdata))
                      {
                          $this->redirect($meeting_url);
                      }
                }
          }
          else
          {             
             $name = (!empty($this->Auth->user('username')))?$this->Auth->user('username'):Guest;
              
              //JOIN MEETING
              if(!empty($result))
              {
                $join_meeting = new JoinMeetingParameters($meeting_id,$name,$mpassword,$configToken);
                $meeting_url = $bbb->getJoinMeetingURL($join_meeting);
                $id=$meeting->id;
                $meetingTable = TableRegistry::get('Meetings');
                $meetingdata = $meetingTable->get($id);

                $meetingdata->meeting_exit_url = $meeting_url;
                $meetingdata->is_meeting_room_active = '1';
                 
                 //attendee table entry
                  $attendeesTable = TableRegistry::get('attendees');
                  $meeting_date = new Time(date('Y-m-d'));
                  
                  $attData = $attendeesTable->find()
                            ->where(['role =' => 'admin','meeting_id =' => $meeting->name,'meeting_date'=>$meeting_date])
                            ->count();   
                 if($attData > '0') 
                   {   
                      $updated_at= new Time(date('Y-m-d H:i:s'));
                      $query = $attendeesTable->query();
                      $query->update()
                      ->set(['updated_at' =>$updated_at])
                      ->where(['meeting_id =' => $meeting->name])
                      ->execute();

                    }
                    else
                    { 
                        $data['created_at'] = new Time(date('Y-m-d H:i:s'));
                        $data['meeting_id']=$meeting->name;
                        $data['meeting_date'] = new Time(date('Y-m-d'));
                        $data['role']='admin';
                       
                        $attendees = $attendeesTable->newEntity();
                        $attendees = $attendeesTable->patchEntity($attendees, $data);
                           if($attendeesTable->save($attendees))
                           {
                               $id = $attendees->id;  
                           }
                     }
                 //end attendee table entry 

                // update Data in copy_attendee Table              
              $this->copy_attendees('admin',$meeting_id,$meeting->created_by,$meeting->timezone);
             
                if($meetingTable->save($meetingdata))
                {
                   $this->redirect($meeting_url); 
                }
              }       
          }
         }
          else
          {
              $this->Flash->error(__('This Meeting Room is currently Closed.'));
              return $this->redirect(['controller'=>'Dashboard','action' => 'index']);
          }
      }
      /*
          copy_attendees method is cALL in joinMeeting method,insert or update data in copy_attendee table
      */
     public function copy_attendees($role,$meeting_id,$created_by,$timezone)
     {
          $copy_attendees = TableRegistry::get('copy_attendees');
        
          $roledata = $copy_attendees->find()
          ->where(['role =' => 'admin','meeting_id =' => $meeting_id,'created_by'=>$created_by])
          ->count();

            if($roledata == '0')
            {
              $attendees = $copy_attendees->newEntity();
              $attendees->role=$role;
              $attendees->created_at = new Time(date('Y-m-d H:i:s'));
              $attendees->meeting_id=$meeting_id;
              $attendees->timezone = $timezone;
              $attendees->created_by=$created_by;

                if($copy_attendees->save($attendees))
                {

               }
            }
     }

    // ============================================= 
      /**
      * class   : 
      * menthod : JoinAttendee
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function JoinAttendee($meeting_id)
      {  
          $this->viewBuilder()->layout('join_attendee');
          $meeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->first();
          $meeting->meeting_time;
          $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
          $this->set('_serialize', ['meeting']);

          $countmeeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->count();

          if($countmeeting > 0)
          {
            $meeting = $this->Meetings->find()
            ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
            ->first();
            $password=$meeting->password;
            date_default_timezone_set($meeting->timezone);
            $this->set(compact('meeting','countmeeting'));
            $this->set('_serialize', ['JoinAttendee']);
            
           if ($this->request->is(['patch', 'post', 'put']))
            {                            
                  $data = $this->request->data;
                  $attendeesTable = TableRegistry::get('attendees');
                  $email= $data['email'];
                  $password= $data['password'];
                  $username= $data['username'];
                  $today=new Time(date('Y-m-d'));
                
               if($attData > '0') 
                 {   
                    $updated_at= new Time(date('Y-m-d H:i:s'));
                    $query = $attendeesTable->query();
                    $query->update()
                    ->set(['updated_at' =>$updated_at])
                    ->where(['meeting_id =' => $meeting_id,'email =' => $data[email]])
                    ->execute();

                  }
                  else
                  { 
                      $data['created_at'] = new Time(date('Y-m-d H:i:s'));
                      $data['meeting_id']=$meeting_id;
                      $data['meeting_date'] = new Time(date('Y-m-d'));
                      $email= $data['email'];
                      $password= $data['password'];
                      $attendees = $attendeesTable->newEntity();
                      $attendees = $attendeesTable->patchEntity($attendees, $data);
                         if($attendeesTable->save($attendees))
                         {
                             $id = $attendees->id;  
                            
                         }
                  }  
                  return $this->redirect('http://'.$_SERVER['HTTP_HOST'].'/meetings/joinMeeting/'.$meeting_id.'/'.$email.'/'.$username.'/'.$id); 
              
             } 
        }

      }

       // ============================================= 
      /**
      * class   : 
      * menthod : PreviewJoinAttendee
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function previewJoin($meeting_id)
      {  
          $this->viewBuilder()->layout('join_attendee');
          $meeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->first();
          $meeting->meeting_time;
          $meeting_date=date('Y-m-d',strtotime($meeting->meeting_date));
          $this->set('_serialize', ['meeting']);

          $countmeeting = $this->Meetings->find()
          ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
          ->count();

          if($countmeeting > 0)
          {
            $meeting = $this->Meetings->find()
            ->where(['meeting_id =' => $meeting_id,'deleted =' => '0'])
            ->first();
            $password=$meeting->password;
            $this->set(compact('meeting','countmeeting'));
            $this->set('_serialize', ['PreviewJoin']);
                   
        }

      }


     // ============================================= 
      /**
      * class   : 
      * menthod : EndMeeting
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function endMeeting($meetingId,$password)
      {
        echo "<script type='text/javascript'>
        alert('Meetings was ended.');
        </script>";
       
        if($this->Auth->user('role') == 'admin')
        {
          return $this->redirect(['controller'=>'dashboard','action' => 'index']);
          
        }
        else
        {
           return $this->redirect(['action' => 'JoinAttendee',$meetingId]);
         
        }
      }

      // ============================================= 
      /**
      * class   : 
      * menthod : EndMeetings From Dashboard
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function endMeetings($meetingId,$password)
      {

         if($this->Auth->user('role') == 'admin')
          {
              $password='swM6dfAKVF@$';
              $bbb = new BigBlueButton;

              //END MEETING
              $end_meeting = new endMeetingParameters($meetingId,$password);
              $EndMeetingResponse = $bbb-> getEndMeetingURL($end_meeting);
              $response=$bbb->endMeeting($end_meeting);

              $arr=xml2array($response);
              $vals= array_values($arr);
              $message='Meetings was ended successfully.';
              $copy_attendees = TableRegistry::get('copy_attendees');

              $data=$copy_attendees->find()->where(['meeting_id' => $meetingId])->first();
              date_default_timezone_set($data->timezone);
               

              $meetingTable = TableRegistry::get('Meetings');
              $meeting =  $meetingTable->find()
              ->where(['meeting_id =' => $meetingId])
              ->first();
              $id=$meeting->id;
              $meetingdata = $meetingTable->get($id);
              $meeting_date=date('Y-m-d',strtotime ($data->created_at));
              $meetingdata->is_meeting_room_active = '0';
              $meetingTable->save($meetingdata);
              //update attendees table with exit time
               $attendees = TableRegistry::get('attendees');
              
              $query = $attendees->query();
              $query->update()
              ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
              ->where(['meeting_id =' => $meeting->name,'meeting_date'=>$meeting_date,'role'=>'admin'])
              ->execute();
              //update users time in attendee table
                $query1 = $attendees->query();
                $query1->update()
              ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
              ->where(['meeting_id =' => $meeting->name,'meeting_date'=>$meeting_date,'exit_time'=>'0000-00-00 00:00:00','role !='=>'admin'])
              ->execute();                       

               // Counting Duration of Meeting, 
                         

              $attendeesdata=$attendees->find()->where(['meeting_id' => $meetingId,'meeting_date'=>$meeting_date,'role'=>'admin'])->first();

                  $date1=date('Y-m-d H:i:s',strtotime ($attendeesdata->created_at));
                  $date2=date('Y-m-d H:i:s',strtotime ($attendeesdata->exit_time));
                  $duration=time_diff($date2,$date1);
                  $attendees = TableRegistry::get('attendees');
                  $meeting_date=date('Y-m-d',strtotime ($data->created_at));
                
                  $count=$attendees->find()->where(['meeting_id' => $meetingId,'meeting_date'=>$meeting_date,'role'=>'admin'])->count();

                 
                  $id=$attendeesdata->id;
                  
                  $attendeesdata = $attendees->get($id);

                  $attendeesdata->durationOfMeeting=$duration;
                  $attendees->save($attendeesdata);
                    /* Counting Duration of Meeting */

            //delete entry of copy_attendee      
              $copy_attendees->deleteAll(array('meeting_id =' => $meetingId));
              echo "<script type='text/javascript'>
              alert('$message');
              </script>";
              return $this->redirect(['controller'=>'dashboard','action' => 'index']);
              exit;
          }    
         else
         {   

              $meetingTable = TableRegistry::get('Meetings');
              $meeting =  $meetingTable->find()
              ->where(['meeting_id =' => $meetingId])
              ->first();

              $id=$meeting->id;
              $meetingdata = $meetingTable->get($id);
              $meeting_date=date('Y-m-d',strtotime ($data->created_at));
              $meetingdata->is_meeting_room_active = '0';
              $meetingTable->save($meetingdata);
              //update attendees table with exit time
               $attendees = TableRegistry::get('attendees');
              $query = $attendees->query();
              $query->update()
              ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
              ->where(['meeting_id =' => $meeting->name,'meeting_date'=>$meeting_date,'exit_time'=>'0000-00-00 00:00:00','role !='=>'admin'])
              ->execute();                       
            echo "<script type='text/javascript'>
            alert('Meetings was ended.');
            </script>";
            return $this->redirect(['controller'=>'dashboard','action' => 'index']);
            exit;
         }
     }

      // ============================================= 
      /**
      * class   : 
      * method : Add Meeting Layout
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   

      public function meetingLayout($id=null)
      {
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
        // $selectedsections =$meeting->sections;
          $selectedlockfeature = $meeting->lockfeature;

          //fetch library data
          $librariesTable = TableRegistry::get('libraries');
          $librarieData = $librariesTable->find()->where(['meeting_id'=>$meeting->id])->order(['id' => 'DESC']);

          if ($this->request->is(['patch', 'post', 'put'])) {
              $data = $this->request->data;
              // $sections=implode(',',$data['sections']);
                      //$data['sections']=$sections;
                      $lockfeature=implode(',',$data['lockfeature']);
                      $data['lockfeature']=$lockfeature;

                      if($data['bgselect'] == 'color')
                      {
                        $data['color']  =$data['colorval'];
                        $data['background']='';
                      }
                      if($data['bgselect'] == 'image')
                      {
                         // $data['background']= $data['background']['name'];
                           $data['color']='';
                      }
              $meeting = $this->Meetings->patchEntity($meeting, $data);
              if ($this->Meetings->save($meeting)) {
                  $id = $meeting->id;
                  $this->Flash->success(__('The meeting has been saved.'));
                 // return $this->redirect(['action' => 'addSuccess',$id]);
                  if (isset($this->request->data['start'])) { 
                    
                      return $this->redirect(['action' => 'joinMeeting',$meeting->meeting_id]);
                  }
              }
              else {
                  $this->Flash->error(__('The meeting could not be saved. Please, try again.'));
              }
          }
          $this->set(compact('meeting','selectedlockfeature','librarieData'));
          $this->set('_serialize', ['meeting']);
      }

     // ============================================= 
      /**
      * class   : 
      * method : Add meetingPolls
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function meetingPolls($id=null)
      {
        $meeting = $this->Meetings->get($id, [
            'contain' => []
        ]);
       
       $data1=$this->request->data();

        $poll = TableRegistry::get('Polls');
        $poll->recursive=-1;
        $polldata = $poll->find('all', array('conditions'=>array('meeting_id'=>$id),
            'group' => ['poll_id']));
         foreach($polldata as $data){ 
             $pollans = $poll->find('all', array('conditions'=>array('poll_id'=>$data->poll_id,'meeting_id' =>$data->meeting_id)));
 
             foreach ($pollans as $key => $value) {
  
            }
         } 

        $meetingpoll = $poll->newEntity();
      
       
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $data = $this->request->data;

          
                $meetingpoll = $poll->patchEntity($meeting, $data);
                $meetingpoll1=array();
                
                   foreach ($meetingpoll['polls'] as $entity) {
                        
                       array_push($meetingpoll1,array('poll_id'=>$entity['pollid'],'meeting_id'=>$entity['meeting_id'],'description'=>$entity['description'],'answer'=>$entity['answer']));
                 
                   }
                   
         $meetingDATA = $poll->newEntities($meetingpoll1);
         foreach ($meetingDATA as $value) {
                $poll->save($value);
          }

        }
        $this->set(compact('meeting','polldata','pollkey',' poll'));
        $this->set('_serialize', ['meeting']);
      }

      // ============================================= 
      /**
      * class   : 
      * method : Add findans
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function findans()
      {   
        $this->viewBuilder()->layout(false); 
        $poll_id=$_POST['poll_id'];
        $meeting_id=$_POST['meeting_id'];
      
        $poll = TableRegistry::get('Polls');
                    $poll->recursive=-1;
        $pollans = $poll->find('all', array('conditions'=>array('poll_id'=>$poll_id,'meeting_id' =>$meeting_id)));
        $anskey=1;

        foreach ($pollans as $key => $value) {
         
              $pollans1='<div id="ansdiv'.$value->poll_id.$anskey.'" class="form-group" style="">
                      <label>Answer </label>
                      <div class="row">
                        <div class="col-sm-10">
                              <div class="input text"><input type="text" id="polls-anskey-answer" data-bvalidator="required" value="'.$value->answer.'" class="form-control" name="polls['.$value->poll_id.'][answer]"></div><input type="hidden" value="'.$value->poll_id.'" name="polls['.$value->poll_id.'][meeting_id]">
                             <input type="hidden" value="'.$value->poll_id.'" name="polls['.$value->poll_id.'][pollid]">
                          </div>
                          <div class="col-sm-2">
                            <label class="bottom_text"></label>
                             <input type="button" value="Remove Answer" name="'.$value->poll_id.$anskey.'" id="removeans" class="btn btn-danger">
                          </div>
                          
                      </div>                    
                  </div>';
          $anskey++;          
        }
        echo $pollans1;
        $this->set(compact('pollans'));
        $this->set('_serialize', ['meetingPolls']);

        //print_r($pollans);
      }

      // ============================================= 
        /**
        * class   : 
        * method : timezoneList
        * @param  : 
        * @output : 
        * @Description : 
        **/
        // ==============================================   
      function timezoneList()
      {
          $timezoneIdentifiers = DateTimeZone::listIdentifiers();
          $utcTime = new DateTime('now', new DateTimeZone('UTC'));

          $tempTimezones = array();
          foreach ($timezoneIdentifiers as $timezoneIdentifier) {
              $currentTimezone = new DateTimeZone($timezoneIdentifier);

              $tempTimezones[] = array(
                  'offset' => (int)$currentTimezone->getOffset($utcTime),
                  'identifier' => $timezoneIdentifier
              );
          }

          // Sort the array by offset,identifier ascending
          usort($tempTimezones, function($a, $b) {
              return ($a['offset'] == $b['offset'])
                  ? strcmp($a['identifier'], $b['identifier'])
                  : $a['offset'] - $b['offset'];
          });

          $timezoneList = array();
          foreach ($tempTimezones as $tz) {
              $sign = ($tz['offset'] > 0) ? '+' : '-';
              $offset = gmdate('H:i', abs($tz['offset']));
              $timezoneList[$tz['identifier']] = '(UTC ' . $sign . $offset . ') ' .
                  $tz['identifier'];
          }

          return $timezoneList;
      }
     // ============================================= 
      /**
      * class   : 
      * method : scheduleMeeting
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function scheduleMeeting($type)
      {         
          $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
          $today=$date->format('Y-m-d');
          $attendeesTable = TableRegistry::get('attendees');
                    
          $Pastmeeting = $attendeesTable->find();
          $Pastmeeting->select(['meeting_name' => 'Meetings.headerText',
                'created_at'=>'attendees.created_at',
                'durationOfMeeting' =>'attendees.durationOfMeeting',
                'TotalAttendee'=>$Pastmeeting->func()->count('attendees.created_at'),
                'id'=>'attendees.id',
                'meeting_id'=>'Meetings.id',
                'timezone'=>'Meetings.timezone',
                'attendeesMeeting_id'=>'attendees.meeting_id',
               ])
            ->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_date <='=> $today,'Meetings.is_meeting_room_active'=>'0'])
            ->group(['attendees.meeting_id','attendees.meeting_date'])
            ->join([
                'Meetings' => [
                    'table' => 'meetings',
                    'type' => 'inner',
                    'conditions' => 'Meetings.name = attendees.meeting_id'
               ]
            ])->order(['attendees.created_at' => 'DESC']);

          $upComingmeeting = $this->Meetings->find()
          ->where(['created_by' => $this->Auth->user('id'),'deleted =' => '0','repeatTime !='=>' ']);

          if($type == 'upcoming')
          {
              $this->set(compact('upComingmeeting','type', $this->               paginate($upComingmeeting)));
          }
          if($type == 'past')
          {
           $this->set(compact('Pastmeeting','type', $this->paginate($Pastmeeting)));
          }

              $bbb = new BigBlueButton;
              $data=array();
        
         $this->set('_serialize', ['scheduleMeeting']);

      }
      // DeleteAttendees From PastMeeting
      public function deleteattendees($meeting_id,$meeting_date)
      {
        $meeting_id=base64_decode($meeting_id);
         $meeting_date=base64_decode($meeting_date);
         $meeting_date=date('Y-m-d',strtotime($meeting_date));
        
          $attendeesTable = TableRegistry::get('attendees');
          if($attendeesTable->deleteAll(array(['meeting_id =' => $meeting_id,'meeting_date'=>$meeting_date])))
          {
            return $this->redirect(['controller'=>'Meetings','action' => 'schedule_meetingcopy',"past"]);
          }

           
        //exit();
      }
      // ============================================= 
      /**
      * class   : 
      * method : scheduleMeetingCopy
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function scheduleMeetingcopy($type)
      {         
          $date = new DateTime('now', new DateTimeZone('Asia/Kolkata'));
          $today=$date->format('Y-m-d');
          $attendeesTable = TableRegistry::get('attendees');
                    
          $Pastmeeting = $attendeesTable->find();
          $Pastmeeting->select(['meeting_name' => 'Meetings.headerText',
                'created_at'=>'attendees.created_at',
                'durationOfMeeting' =>'attendees.durationOfMeeting',
                'TotalAttendee'=>$Pastmeeting->func()->count('attendees.created_at'),
                'id'=>'attendees.id',
                'meeting_id'=>'Meetings.name',
                'timezone'=>'Meetings.timezone',
                'meeting_date'=>'attendees.meeting_date',
                'attendeesMeeting_id'=>'attendees.meeting_id',
               ])
            ->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_date <='=> $today,'Meetings.is_meeting_room_active'=>'0'])
            ->group(['attendees.meeting_id','attendees.meeting_date'])
            ->join([
                'Meetings' => [
                    'table' => 'meetings',
                    'type' => 'inner',
                    'conditions' => 'Meetings.name = attendees.meeting_id'
               ]
            ])->order(['attendees.created_at' => 'DESC']);

           // exit;
          $upComingmeeting = $this->Meetings->find()
          ->where(['created_by' => $this->Auth->user('id'),'deleted =' => '0','repeatTime !='=>' ']);

          if($type == 'upcoming')
          {
              $this->set(compact('upComingmeeting','type', $this->               paginate($upComingmeeting)));
          }
          if($type == 'past')
          {
           $this->set(compact('Pastmeeting','type', $this->paginate($Pastmeeting)));
          }

              $bbb = new BigBlueButton;
              $data=array();
            
           
         $this->set('_serialize', ['scheduleMeeting']);
      }
     // ============================================= 
      /**
      * class   : 
      * method : PastmeetingDetails Reports
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      public function PastmeetingDetails($id,$meeting_date)
      {   
       
            //Recording Report
            $Recording_array=array();
            $bbb = new BigBlueButton;
            $meeting_id=json_decode(json_encode($id));
            $Recording= new GetRecordingsParameters($meeting_id);
            $Recordingdata= $bbb->getRecordingsWithXmlResponseArray($Recording);
            $Recordingdata=xml2array($Recordingdata);
            
            if($Recordingdata['messageKey']['0'] != 'noRecordings')
            { 
              $Recording_array['Id']=$id;
              $Recording_array['meeting_date']=$meeting_date;
              $Recording_array['recordId']=$Recordingdata['0']['recordId'];
              $Recording_array['url']=$Recordingdata['0']['playbackFormatUrl'];
              $Recording_array['size']=$Recordingdata['0']['playbackFormatLength'];
            } 

          $meeting_date=base64_decode($meeting_date);
          $meeting_date=date('Y-m-d',strtotime($meeting_date));

          $attendeesTable = TableRegistry::get('attendees');
          
          //meeting datails data          
          $meeting = $attendeesTable->find();
          $meeting->select([
                'meeting_name' => 'Meetings.headerText',
                'created_at'=>'attendees.created_at',
                'durationOfMeeting' =>'attendees.durationOfMeeting',
                'TotalAttendee'=>$meeting->func()->count('attendees.created_at'),
                'id'=>'attendees.id',
                'timezone'=>'Meetings.timezone',

               ])
            ->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_id'=> $id,'attendees.meeting_date'=>$meeting_date])
            ->group('attendees.meeting_id')
            ->join([
                'Meetings' => [
                    'table' => 'meetings',
                    'type' => 'inner',
                    'conditions' => 'Meetings.name = attendees.meeting_id'
                ]
                
            ]); 
            //Attendees chart data
            $attendees = $attendeesTable->find();
            $attendees->select([
                'created_at'=>'attendees.created_at',
                'TotalAttendee'=>$attendees->func()->count('attendees.id'),
               ])
            ->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_id'=> $id,'attendees.meeting_date'=>$meeting_date])
            ->group(['unix_timestamp(attendees.created_at) DIV 300'])
            ->join([
                'Meetings' => [
                    'table' => 'meetings',
                    'type' => 'inner',
                    'conditions' => 'Meetings.name = attendees.meeting_id'
                ]
               
            ]);
           //set data points for Attendees chart data 
            $data_points = array();

            foreach ($attendees as $key => $value) {
            
            $time=date('h:m:i',strtotime($value['created_at']));
            $point = array("label" => $time , "y" => $value['TotalAttendee']);
            array_push($data_points, $point);
            }

          //attendees tab data
            $attendeestab = $attendeesTable->find();
            $attendeestab->select([
                'id' => 'attendees.id',
                'email' => 'attendees.email',
                'created_at'=>'attendees.created_at',
                'durationOfMeeting' =>'attendees.durationOfMeeting',
                'exit_time'=>'attendees.exit_time',
                'username'=>'attendees.username',
               // 'timezone'=>'Meetings.timezone',

               ])
            ->where(['Meetings.created_by'=>$this->Auth->user('id'),'attendees.meeting_id'=> $id,'attendees.meeting_date'=>$meeting_date,'role !='=>'admin'])
           
            ->join([
                'Meetings' => [
                    'table' => 'meetings',
                    'type' => 'inner',
                    'conditions' => 'Meetings.name = attendees.meeting_id'
                ]
                
            ]);
            //chat history
            $chat_folder = "/var/bigbluebutton/published/presentation";
            $meetings = glob($chat_folder . '/*' , GLOB_ONLYDIR);
            $chat_array = array();
            foreach ($meetings as $key => $value) {
              $meeting_id_ary = explode('/', $value);
              $meeting_id = end($meeting_id_ary);
               

               if(file_exists($chat_folder."/".$meeting_id."/"."metadata.xml"))
               {
                  /*header('application/xml');

                  $xml = simplexml_load_file($chat_folder."/".$meeting_id."/"."metadata.xml"); 
                  $json_string = json_encode($xml);
                  $result_array = json_decode($json_string, TRUE);
               echo "<pre>";print_r($result_array); exit;
                  if($result_array['meta']['meetingName'] == $id) 
                  {

                      if(file_exists($chat_folder."/".$meeting_id."/"."slides_new.xml")){
                      header('application/xml');
                      $xml = simplexml_load_file($chat_folder."/".$meeting_id."/"."slides_new.xml");
                      $json_string = json_encode($xml);
                      $result_array = json_decode($json_string, TRUE);
                      $result_array=xml2array($result_array);
                   
                      foreach ($result_array['chattimeline'] as $key => $value) {
                     
                          if($value['name'] != '')
                          {
                            $name=$value['name']; 
                            $message=$value['message'];
                          }
                          else
                          {
                             $name=$value['@attributes']['name']; 
                             $message=$value['@attributes']['message'];
                          }
                           $point = array("name" => $name , "message" => $message);
                           array_push($chat_array, $point);
                        }
                         
                      }
                  }*/
                }
            } 
          //chat history completed

           $this->set(compact('meeting','attendees','data_points','attendeestab','chat_array','Recording_array'));
            //  echo "<pre>";print_r($meeting); exit;

      }        
       public function DeleteRecording($record_id,$id,$meeting_date)
        {
            /*$bbb = new BigBlueButton;

              $Recording= new DeleteRecordingsParameters($record_id,'123456');
              $Recordingdata= $bbb->deleteRecordingsWithXmlResponseArray($Recording);*/
                        
              return $this->redirect(['action' => 'PastmeetingDetails',$id,$meeting_date]);

        }       


      // ============================================= 
      /**
      * class   : 
      * method : sendMail
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      function sendMail($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name,$password)
      {
          try {
              //SEND MAIL TO USER
              $email = new Email('default');
              $email->from(['webinar@gmail.com' => 'Webinar'])
                    ->to($to)
                    ->template('email_invitation')
                    ->emailFormat('html')
                    ->subject($sub)
                    ->viewVars(['meeting_id'=>$meeting_id,'email_message'=>$email_message,'meeting_time'=>$meeting_time,'meeting_date'=>$meeting_date,'meeting_name'=>$meeting_name,'password'=>$password])
                    ->send(); 
          } catch (Exception $e) {
              echo 'Exception : ',  $e->getMessage(), "\n";
          }
      }
      // ============================================= 
      /**
      * class   : 
      * method : sendMailNote
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================   
      function sendMailNote($to,$sub,$meeting_id,$email_message,$meeting_time,$meeting_date,$meeting_name)
      {
          try {
              //SEND MAIL TO USER
              $email = new Email('default');
              $email->from(['webinar@gmail.com' => 'Webinar'])
                    ->to($to)
                    ->template('email_note')
                    ->emailFormat('html')
                    ->subject($sub)
                    ->viewVars(['meeting_id'=>$meeting_id,'email_message'=>$email_message,'meeting_time'=>$meeting_time,'meeting_date'=>$meeting_date,'meeting_name'=>$meeting_name])
                    ->send(); 
          } catch (Exception $e) {
              echo 'Exception : ',  $e->getMessage(), "\n";
          }
      }
     // ============================================= 
      /**
      * class   : 
      * method : attendencelist
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================
      function attendencelist()
      {   
        $id=$_POST['id'];
        $query = TableRegistry::get('Meetings');
        $meetings = $query->find();
        $meetings->select('attendees');
        $meetings->where(['id' => $id]);
        
        $meetings->count();
        foreach ($meetings as $row){
          $meeting=$row->attendees;
        }
        echo $meeting;
        exit;

      }
      // ============================================= 
      /**
      * class   : 
      * method : contentLibrary
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================       

      public function contentLibrary($id = null)
      {
        
        $librariesTable = TableRegistry::get('libraries');
        $librarie = $librariesTable->newEntity();
        $librarieData = $librariesTable->find()->where(['meeting_id'=>$id])->order(['id' => 'DESC']);

        $uploadData = '';
        if ($this->request->is('post')) {
            if(!empty($this->request->data['file']['name'])){
                $fileName = $this->request->data['file']['name'];
                $uploadPath =  WWW_ROOT.'files/library/';
                $uploadFile = $uploadPath.$fileName;
                $type=$this->request->data['file']['type'];
                $size=$this->request->data['file']['size'];
                $this->request->data['file']['tmp_name'];
              
              /*  $validExtensions  = array('image/jpeg','image/gif','image/png','application/pdf','application/pptx'); //array of valid extensions
                if(!in_array($type, $validExtensions))
                {
                    echo 'invalid file type';
                   // $this->Flash->error(__('invalid file type.'));
                }
                if($size > '26214400' || $size == '0')
                {
                   echo 'upload less than or equal 25 Mb file';
                }*/
               

                if(move_uploaded_file($this->request->data['file']['tmp_name'],$uploadFile)){
                    $uploadData = $librariesTable->newEntity();
                    $uploadData->file = $fileName;
                    $uploadData->size = $size;
                    $uploadData->created_at = new Time(date('Y-m-d H:i:s'));
                    $uploadData->meeting_id = $this->request->data['meeting_id'];

                    if ($librariesTable->save($uploadData)) {
                      echo "File has been uploaded successfully.";
                       // $this->Flash->success(__('File has been uploaded successfully.'));
                    }else{
                      echo "Unable to upload file, please try again.";
                       // $this->Flash->error(__('Unable to upload file, please try again.'));
                    }
                }else{
                  echo 'Unable to upload file, please try again.';
                   // $this->Flash->error(__('Unable to upload file, please try again.'));
                }
            }else{
              echo 'Please choose a file to upload';
                //$this->Flash->error(__('Please choose a file to upload.'));
            }
            exit;
          }
          $meeting = $this->Meetings->get($id, [
              'contain' => []
          ]);
          $this->set(compact('librarieData', $this->paginate($librarieData)));
            $this->set(compact('meeting'));
          $this->set('_serialize', ['contentLibrary']);
      }

      // ============================================= 
      /**
      * method : DeleteLibrary
      * @param  : 
      * @output : 
      * @Description : 
      **/
      // ==============================================    
     public function DeleteLibrary($id)
      {
          $librariesTable = TableRegistry::get('libraries');
          $libraries = $librariesTable->get($id);
          if ($librariesTable->delete($libraries)) {
              $this->Flash->success(__('The libraries file has been deleted.'));
          } 
          else
           {
              $this->Flash->error(__('The libraries file could not be deleted. Please, try again.'));
           } 
          return $this->redirect(['controller'=>'meetings','action' => 'contentLibrary']);
      }

      // ============================================= 
      /**
      * method : cronjob
      * @Description : create cronjob which run every 5 minute
         and check admin or presenter is true in meetinginfo request
         if presenter true update create_time of admin in copy attendee
         table

         if presenter false them wait for 30 minute 
         if presenter not come within 30 minute 
         all attendee kicked out from running minute
      **/
      // ==============================================    
      public function cronjob()
      {
          $this->viewBuilder()->layout(false);
          $copy_attendees = TableRegistry::get('copy_attendees');
          
          //count Running Meeting
          $roledata = $copy_attendees->find()
          ->where(['role =' => 'admin'])
          ->group('created_by')->count();
         
          //Fetch running Meeting Data
           $roledata1 = $copy_attendees->find()
           ->group('created_by');
          
          //if count greater that 0
          if($roledata > '0')
          {

            foreach ($roledata1 as $key => $value) {
              $bbb = new BigBlueButton;
              $meeting_id=$value->meeting_id;
              $password='swM6dfAKVF@$';
             
             // getMeetingInfo of running Meeting
              $meeting_info = new GetMeetingInfoParameters($meeting_id,$password);
              $getMeetingInfoUrl = $bbb->getMeetingInfo($meeting_info);

              $getMeetingInfo=xml2array($getMeetingInfoUrl);
              $vals= array_values($getMeetingInfo);
              $infoArray=$vals['3']['attendees'];

              $finaltime=$vals['3']['createDate'];
              $timezoneArray=explode(' ', $finaltime);
              $time= $timezoneArray['0'].' '.$timezoneArray['1'].' '.$timezoneArray['2'].' '.$timezoneArray['3'].' '.$timezoneArray['5'];
              
              date_default_timezone_set($value->timezone);

              //CHECK ATTENDEES EXIT IN MEETING OR NOT
              $meeting_date= date('Y-m-d',strtotime($time));
              $meetingID=$vals['3']['meetingID'];
              $attendees = TableRegistry::get('attendees');
              $attendeesdata = $attendees->find()
              ->where(['meeting_id =' => $meetingID,'meeting_date'=>$meeting_date,'role !='=>'admin']);
           
           
             $attendeesName=array();
              foreach ($infoArray as $value) {
                    $attendeesName[]=$value['userID']; 
                foreach ($value as  $value1) {
                  $attendeesName[]=$value1->userID; 
                 }
               } 
               
              foreach ($attendeesdata as $key => $value) {
                
                if(!in_array($value->id, $attendeesName))
                { 
                    echo $value->id;
                    $query = $attendees->query();
                    $query->update()
                    ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
                    ->where(['id'=>$value->id,'exit_time'=>'0000-00-00 00:00:00'])
                    ->execute();
                }
              }

           
             
              $checkAdmin = '0';
            
                foreach ($infoArray as $key => $value) {
                     
                  if($value['isPresenter'] == 'true')
                  {
                      //Update Admin create_time
                      $query = $copy_attendees->query();
                      $created_at=new Time(date('Y-m-d H:i:s'));
                        $query->update()
                        ->set(['created_at' => $created_at])
                        ->where(['meeting_id' => $meeting_id,'role'=>'admin'])
                        ->execute();

                        $checkAdmin = '1';
                  } 

                  foreach ($value as $key => $value1) {
                  
                    if($value1->isPresenter == 'true')
                    { 
                      //Update Admin create_time
                    $query = $copy_attendees->query();
                     $created_at=new Time(date('Y-m-d H:i:s'));
                      $query->update()
                      ->set(['created_at' => $created_at])
                      ->where(['meeting_id' => $meeting_id,'role'=>'admin'])
                      ->execute();
                      $checkAdmin = '1';
                    } 
                  }
                }
            
                if($checkAdmin != '1')
                {
                
                  $copy_attendees = TableRegistry::get('copy_attendees');
                  $data=$copy_attendees->find()->where(['meeting_id' => $meeting_id,'role'=>'admin'])->first();
                
                 

                  date_default_timezone_set($data->timezone);

                  $time= date('Y-m-d H:i:s',strtotime ($data->created_at));

                  $currentTime=date('Y-m-d H:i:s');
                  $enterTime =date('Y-m-d H:i:s',strtotime ('+30 minute' ,strtotime ($time)) );

                  //if current time is grater than admin create_at time then

                    if($currentTime > $enterTime)
                    {
                      $bbb = new BigBlueButton;
                      $password='swM6dfAKVF@$';
                      //END MEETING
                      $end_meeting = new endMeetingParameters($meeting_id,$password);
                      $EndMeetingResponse = $bbb-> getEndMeetingURL($end_meeting);
                      $response=$bbb->endMeeting($end_meeting);
                      $arr=xml2array($response);
                      $vals= array_values($arr);

                      //Update attendees remaining attendees and admin exit time 
                      $attendees = TableRegistry::get('attendees');
                     
                       $query = $attendees->query();
                        $query->update()
                        ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
                        ->where(['meeting_id =' => $meetingID,'meeting_date'=>$meeting_date,'role'=>'admin'])
                        ->execute();
                        //update users time in attendee table
                          $query1 = $attendees->query();
                          $query1->update()
                        ->set(['exit_time' => new Time(date('Y-m-d H:i:s'))])
                        ->where(['meeting_id =' => $meetingID,'meeting_date'=>$meeting_date,'exit_time'=>'0000-00-00 00:00:00','role !='=>'admin'])
                        ->execute();                       

               // Counting Duration of Meeting, 
              
                 $attendeesdata=$attendees->find()->where(['meeting_id' => $meetingID,'meeting_date'=>$meeting_date,'role'=>'admin'])->first();

                  $date1=date('Y-m-d H:i:s',strtotime ($attendeesdata->created_at));
                  $date2=date('Y-m-d H:i:s',strtotime ($attendeesdata->exit_time));
                  $duration=time_diff($date2,$date1);
                  $meeting_date=date('Y-m-d',strtotime ($data->created_at));
                
                  $count=$attendees->find()->where(['meeting_id' => $meetingID,'meeting_date'=>$meeting_date,'role'=>'admin'])->count();

                 
                  if($count > 0)
                  {
                    $id=$attendeesdata->id;
                    $attendeesdata = $attendees->get($id);

                    $attendeesdata->durationOfMeeting=$duration;
                    $attendees->save($attendeesdata);
                   }

                      // set Meeting status InActive 0
                      $MeetingsTable = TableRegistry::get('meetings');
                      $query = $MeetingsTable->query();
                      $name=$meeting_id;
                      $query->update()
                      ->set(['is_meeting_room_active' => '0'])
                      ->where(['name' => $name])
                      ->execute();
                  //delete that meetings all entries from table
                      $copy_attendees->deleteAll(array('meeting_id =' => $meeting_id));
                      echo $message='Meetings was ended successfully.';

                  /*    //insert in recording table

                       $recording = TableRegistry::get('recording');
                        
                        $query = $recording->query();
                        $query->insert(['recording_id', 'exit_time'])
                            ->values([
                                'recording_id' => 'First post',
                                'exit_time' => 'Some body text'
                            ])
                            ->execute();
*/
                    }
                }

              }
           }
            exit;
      }
    

    // ============================================= 
    /** menthod : downloadRecording
    * @param  : 
    * @Description : method for download recording
    */// ==============================================
   // public function downloadRecording($meeting_id,$meeting_name)
    public function downloadRecording()
    {
        /*$presentation_folder = "/var/bigbluebutton/published/presentation";
        $recorded_files = "/home/olympic/www/webroot/files/recordings";

             
        $value = $presentation_folder.'/'.$meeting_id;
       */
        $presentation_folder = "/var/bigbluebutton/published/presentation";
        $recorded_files = "/home/olympic/www/webroot/files/recordings";
        $meetings = glob($presentation_folder . '/*' , GLOB_ONLYDIR);
         foreach ($meetings as $key => $value) {
          
            $meeting_id_ary = explode('/', $value);
            $meeting_id = end($meeting_id_ary);

        $meetings = glob($presentation_folder . '/*' , GLOB_ONLYDIR);
             /*FOR POLL*/
            if (!file_exists($recorded_files."/".$meeting_id."/poll")) {
                $image_xml = simplexml_load_file($value."/"."shapes.svg");
                $image_ary = json_decode(json_encode((array)$image_xml), TRUE);
               // echo "<pre>";print_r($image_ary); echo "</pre>";exit;
            }
         
            if(!file_exists($recorded_files."/".$meeting_id."/"."download.mp4")){
                shell_exec("mkdir $recorded_files/$meeting_id");
             $presentation_images_video = $this->generateVideoFromImages($value,$recorded_files."/".$meeting_id);   
                
                if (file_exists($value."/video")) {
                    $this->overLapVideoOnImagePresentation($value."/video",$presentation_images_video,$recorded_files."/".$meeting_id);    
                }else{
                    /*CONCAT AUDIO WITH SLIDE SHOW VIDEO*/
                     exec("/usr/local/bin/ffmpeg -i ".$presentation_images_video." -i ".$value."/audio/audio.ogg ".$recorded_files."/".$meeting_id."/download.mp4");
                }
            }
     
       }// return $this->redirect(['controller'=>'dashboard','action' => 'GetRecordingcopy']);
        echo "<pre>";print_r("DONE"); echo "</pre>";exit;
    } 
    // ============================================= 
   /** menthod : generateVideoFromImages
   * @param  : 
   * @Description : GET ALL iMAGES FROM FOLDER AND GENERATE VIDEO FROM IT REGARDING IT'S DURATION
   */// ==============================================           
   public function generateVideoFromImages($meeting_path='',$destination_path='')
   {
        if (!empty($meeting_path)) {
            $image_xml = simplexml_load_file($meeting_path."/"."shapes.svg");
         $image_ary = json_decode(json_encode((array)$image_xml), TRUE);

            if(!isset($image_ary['image'])) return FALSE;
          
            shell_exec("mkdir $destination_path/raw-presentation");
            
            /*IF ONLY AUDIO AVAILABLE FOR RECORDING THEN WE SHOULD CREATE VIDEO OF PRESENTATION UPTO AUDIO FILE STEAM LENGHT*/
           /* if ( (file_exists($meeting_path.'/audio')) && (!file_exists($meeting_path.'/video')) ) {
                $file_information = exec("/usr/local/bin/ffmpeg -i".$meeting_path."/audio/audio.ogg");

                $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
                if (preg_match($regex_duration, $file_information, $regs)) {
                    $hours = $regs [1] ? $regs [1] : null;
                    $mins = $regs [2] ? $regs [2] : null;
                    $secs = $regs [3] ? $regs [3] : null;
                    $ms = $regs [4] ? $regs [4] : null;
                }

            }*/
          
            $img_path=array();
           // echo $image_xml->image->attributes('xlink', true)->href;
           // exit;
             foreach ($image_xml->image as $item) { 
                      $attributes = $item->attributes('xlink', true);
                      $img_path[]=$attributes->href;
                      }
                   
              $img_path=json_decode(json_encode((array)$img_path), TRUE);
                
            $cnt =1;
        
            $time_ary = array();
            foreach ($image_ary['image'] as $key => $value) {
               $attributes = $item->attributes('xlink', true);
                 if (isset($value['@attributes']['text'])) {
                    $in_time = $value['@attributes']['in'];
                    $out_time = $value['@attributes']['out'];
                    $text_file = $value['@attributes']['text'];
                    $image_id = $value['@attributes']['id'];
                    $image_idarray=explode('image',$image_id);
                    $id=$image_idarray['1'];

                    if(strpos($in_time, ' ')!==FALSE){
                        $intime = explode(' ', $in_time);
                        $outtime = explode(' ', $out_time);
                        
                        foreach ($intime as $key => $value) {
                           echo "cnt=".$cnt;
                            $value = round($value);
                            $time_ary[$value][$cnt]['image_id'] = $image_id;
                            $time_ary[$value][$cnt]['in_time'] =  round($value);
                            $time_ary[$value][$cnt]['out_time'] =  round($outtime[$key]);
                            $time_ary[$value][$cnt]['text_file'] =  $text_file;
                            $time_ary[$value][$cnt]['destination_path'] =  $destination_path;
                            $time_ary[$value][$cnt]['link'] =   $img_path[$id]['0'];
                          
                            $cnt++;
                        }
                    }else{
                       
                        $in_time = round($in_time);
                        $time_ary[$in_time][$cnt]['image_id'] = $image_id;
                        $time_ary[$in_time][$cnt]['in_time'] = round($in_time);
                        $time_ary[$in_time][$cnt]['out_time'] = round($out_time);
                        $time_ary[$in_time][$cnt]['text_file'] = $text_file;
                        $time_ary[$in_time][$cnt]['destination_path'] =  $destination_path;
                        $time_ary[$in_time][$cnt]['link'] = $img_path[$id]['0'];
                        
                        $cnt++;
                    }
                 }
            
          }
         
                   ksort($time_ary);

            // TIME ARRAY 
            $img_cnt=1;
            foreach ($time_ary as $key => $value) {
                foreach ($value as $k => $val) {
                    $text_file = $val['link'];
                    $text_file_array = explode('/',$text_file);
                    $image_name = explode('.',end($text_file_array));
                  
                    $lastEle=end($time_ary );
                                     
                  
                    if($value != $lastEle)
                    {
                     $time = $val['out_time']-$val['in_time'];
                    }

                    else
                    {
                       $time=1;
                    }
                     
          
                    // CONVERT IMAGE TO VIDEO WITH IT'S RESPECTIVE IN OUT TIME
                    if (!empty($time)) {
                        if ( (!file_exists("$val[destination_path]/raw-presentation/$img_cnt-$image_name[0].mp4")) ) {
                         $image_path = $meeting_path."/".$text_file_array[0]."/".$text_file_array[1]."/".$image_name[0].".png";
                     
                         exec("/usr/local/bin/ffmpeg -loop 1 -i $image_path -c:v libx264 -t $time -pix_fmt yuv420p -vf scale=640:480 $destination_path/raw-presentation/$img_cnt-$image_name[0].mp4");
                        }

                        // CONVERT VIDEO TO TXT FILE FOR CONCAT
                        if ( (!file_exists("$val[destination_path]/raw-presentation/$img_cnt-$image_name[0].ts")) ) {
                           exec("/usr/local/bin/ffmpeg -i $val[destination_path]/raw-presentation/$img_cnt-$image_name[0].mp4 -c copy -bsf:v h264_mp4toannexb -f mpegts $destination_path/raw-presentation/$img_cnt-$image_name[0].ts");
                        }
                    
                        $img_cnt++;
                    }

                } 
            }

    
            $raw_image_txt_files = "$destination_path/raw-presentation";
            $raw_images = glob($raw_image_txt_files . '/*');
            
            /*COMBINE ALL IMAGE VIDEO AS VIDEO*/
            $img_video_ts_files = array();
            foreach ($raw_images as $key => $value) {
                if(strpos($value, '.ts')!==FALSE){
                    $img_video_ts_files[]=$value;
                }
            }

            if ((!empty($img_video_ts_files)) && (!file_exists($destination_path.'/presentation_video/presentation_image.mp4')) ) {
                shell_exec("mkdir $destination_path/presentation_video");
             
                $all_files = implode('|',$img_video_ts_files) ;
                exec('/usr/local/bin/ffmpeg -i "concat:'.$all_files.'" -c copy -bsf:a aac_adtstoasc '.$destination_path.'/presentation_video/presentation_image.mp4');
            }
            return $destination_path.'/presentation_video/presentation_image.mp4';
       }
    }
    // ============================================= 
    /** menthod : overLapVideoOnImagePresentation
    * @param  : 
    * @Description : video overlap on video
    */// ==============================================
    public function overLapVideoOnImagePresentation($overlap_video='',$inner_video='',$destination_path='')
    {
        if (file_exists($overlap_video.'/webcams.webm')) {
            $overlap_video = $overlap_video.'/webcams.webm';
            if (!file_exists($destination_path."/".'webcame-video/output.mp4')) {
                shell_exec("mkdir $destination_path/webcame-video");

                /*RESIZE VIDEO AND CONVERT IT'S FORMAT*/
                exec("/usr/local/bin/ffmpeg -fflags +genpts -i ".$overlap_video." -vf colorchannelmixer=aa=0.5[bottom] -r 0.5 ".$destination_path."/webcame-video/output.mp4");
               
              //  exec("/usr/local/bin/ffmpeg -fflags +genpts -i ".$overlap_video." -vf scale=300:280:force_original_aspect_ratio -r 24 ".$destination_path."/webcame-video/output.mp4");
            }
            /*OVERLAP WEBCAM ON SLIDE VIDEO*/
           
            exec('/usr/local/bin/ffmpeg -i '.$inner_video.' -vf colorchannelmixer=aa=0.5[bottom] -r 0.5 "movie='.$destination_path.'/webcame-video/output.mp4[inner]; [in][inner] overlay=325:200 [out]" '.$destination_path.'/completed.mp4');
            
            /*EXTRACT AUDIO FROM WEBCAM VIDEO*/
            exec("/usr/local/bin/ffmpeg -i ".$destination_path."/webcame-video/output.mp4 -vn -acodec copy ".$destination_path."/webcame-video/webcam-audio.aac");
            
            /*CONCAT AUDIO WITH SLIDE SHOW VIDEO*/
            exec("/usr/local/bin/ffmpeg -i ".$destination_path."/completed.mp4 -i ".$destination_path."/webcame-video/webcam-audio.aac ".$destination_path."/download.mp4");
            
        }
    }  

    public function  deleteRecordingCron()
    {
        $presentation_folder = "/var/bigbluebutton/published/presentation";
        $recorded_files = "/home/olympic/www/webroot/files/recordings";
        $meetings = glob($presentation_folder . '/*' , GLOB_ONLYDIR);
         foreach ($meetings as $key => $value) {
          
            $meeting_id_ary = explode('/', $value);
            $record_id = end($meeting_id_ary);
            $bbb = new BigBlueButton;
            $create_time=date("Y-m-d H:i:s", filectime($presentation_folder.'/'.$record_id));
            $finalTime =date('Y-m-d H:i:s',strtotime ('+72 hours' ,strtotime ($create_time)) );
            $today=date('Y-m-d H:i:s');
            if($finaltime < $today)
            {
              $Recording= new DeleteRecordingsParameters($record_id);
              $Recordingdata= $bbb->deleteRecordingsWithXmlResponseArray($Recording);
              echo "sudo bbb-record --delete ".$presentation_folder.'/'.$record_id;
              exec("bbb-record --delete ".$record_id);
              
            }
         }
        exit;   
    }
}  

  