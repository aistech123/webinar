<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * CakePHP Profile
 * @author abhijit
 */
class Userprofile extends AppModel {

    public $useDbConfig = 'ogi';
    public $useTable = 'profiles';
    public $primaryKey = 'user_id';
    public $belongsTo = array(
        'User' => array(
            'class_name' => 'User',
            'foreignKey' => 'user_id'
        )
    );

}
